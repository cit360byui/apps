package com.thetrio.ewfitness;

import androidx.annotation.NonNull;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.thetrio.ewfitness.Model.User;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class FirebaseTest {

    @Test
    public void firebaseTest() {
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference ref = db.getReference().child("test");
        ref.removeValue();
        //assertEquals(0, ref.);

        //Create a test user
        final User test1 = new User("test1", 1993, 0, 15, "m", 5, 10, 291, (float) 1.2, -500);

        //Push to database to create unique id
        DatabaseReference u1 = ref.push();

        //Give unique id user info
        u1.setValue(test1);

        //grab the key of the unique ID
        final String u1ID = u1.getKey();

        //Add single value even listener to test grabbing data from firebase
        ref.child(u1ID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot shot) {
                try {
                    User u = shot.getValue(User.class);
                    assertEquals(test1.getName(), u.getName());
                    assertEquals(test1.getBirthdate(), u.getBirthdate());
                    assertEquals(test1.getSex(), u.getSex());
                    assertEquals(test1.getHeight(), u.getHeight(), 0.001);
                    assertEquals(test1.getWeight(), u.getWeight(), 0.001);
                    assertEquals(test1.getActLevel(), u.getActLevel(), 0.001);
                    assertEquals(test1.getGoal(), u.getGoal());
                    assertEquals(test1.getBMR(), u.getBMR());
                    assertEquals(test1.getDailyCalories(), u.getDailyCalories());
                    assertEquals(test1.getMealCalories(), u.getMealCalories());
                    //Set the user key
                    u.setKey(u1ID);

                    //print user to ensure correct information
                    System.out.println(u);
                } catch (DatabaseException e) {
                    System.out.println(e);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println("The read has failed: " + databaseError.getCode());
            }
        });
    }
}


/* HEre are some examples of code I used to attempt to create test cases of logging in, creating account, and signing out in firebase. I ended up not
being able to complete due to tests not containing an app activity for my code to use. Maybe I will find a solution in the future.
    public static Activity getCurrentActivity() {
        final Activity[] currentActivity = {null};
        InstrumentationRegistry.getInstrumentation().runOnMainSync(new Runnable() {
            public void run() {
                Collection resumedActivities = ActivityLifecycleMonitorRegistry.getInstance()
                        .getActivitiesInStage(RESUMED);
                if (resumedActivities.iterator().hasNext()) {
                    currentActivity[0] = (Activity) resumedActivities.iterator().next();
                }
            }
        });
        return currentActivity[0];
    }
*/

/*    @Test
    public void firebaseAccount() {
        int counter = 0;
        String userName = "test" + counter + "@example.com";
        String password = "T3st3xample";
        //Activity act = getActivity();
        Account.createAccount(this, userName, password);
    }*/

/*
    @Before
    public void setup() throws Exception {
        super.setUp();

        setActivityInitialTouchMode(true);
        myActivity = getActivity();
    }
*/

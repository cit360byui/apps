package com.thetrio.ewfitness.view;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.thetrio.ewfitness.Model.Account;
import com.thetrio.ewfitness.R;

public class CreateAccount extends Fragment {
    private EditText username;
    private EditText password;
    private EditText passCheck;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstState) {
        View view = null;
        try {
            super.onCreateView(inflater, container, savedInstState);

            // Load the xml file that corresponds to this Java file.
            view = inflater.inflate(R.layout.create_account, container, false);

            username = view.findViewById(R.id.editUsername);
            password = view.findViewById(R.id.editPass);
            passCheck = view.findViewById(R.id.editPassCheck);

            Button createAcc = view.findViewById(R.id.button2);
            createAcc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Activity act = getActivity();
                    String userN = username.getText().toString();
                    String pass = password.getText().toString();
                    String passC = passCheck.getText().toString();

                    if(userN.isEmpty() || pass.isEmpty() || passC.isEmpty()) {
                        AlertDialog alert = new AlertDialog.Builder(act).create();
                        alert.setTitle("Alert!");
                        alert.setMessage("Must fill in all fields!");
                        alert.show();
                    } else if(!pass.equals(passC)) {
                        AlertDialog alert = new AlertDialog.Builder(act).create();
                        alert.setTitle("Alert!");
                        alert.setMessage("Passwords do not match");
                        alert.show();
                    } else {
                        try {
                            Account.createAccount(act, userN, pass);

                            Fragment frag = new NSD();
                            FragmentTransaction trans =
                                    getFragmentManager().beginTransaction();
                            trans.replace(R.id.fragContainer, frag);
                            trans.addToBackStack(null);
                            trans.commit();
                        } catch (Exception ex) {
                            //Log.e(MainActivity.TAG, ex.getMessage());
                            AlertDialog alert = new AlertDialog.Builder(act).create();
                            alert.setTitle("Alert!");
                            alert.setMessage(ex.getMessage());
                            alert.show();
                        }
                    }
                }
            });

        } catch (Exception ex) {
            //Log.e(MainActivity.TAG, ex.toString());
        }

        return view;
    }
}
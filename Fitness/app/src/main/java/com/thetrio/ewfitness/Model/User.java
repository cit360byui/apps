package com.thetrio.ewfitness.Model;

import java.text.ParseException;
import java.util.Objects;
import java.util.Calendar;

public class User {
    private float actLevel;
    private long birthdate;
    private int bmr;
    private int dailyCalories;
    private int goal;
    private float height;
    private int mealCalories;
    private String name;
    private String sex;
    private String key;
    private float weight;

    public User() {

    }

    public User(String name, int year, int month, int day, String sex, float feet, float inches, float weight, float actLevel, int goal) {
        this.name = name;
        this.setBirthdate(year, month, day);
        this.sex = sex;
        this.height = (feet * 12) + inches;
        this.weight = weight;
        this.actLevel = actLevel;
        this.goal = goal;
        this.bmr = CalorieCalc.getBMR(this);
        this.dailyCalories = CalorieCalc.getDailyCalories(this);
        this.mealCalories = CalorieCalc.getMealCalories(this);
    }

    public int getAge() {
        long current = Calendar.getInstance().getTimeInMillis();
        double age = Math.floor((current - this.birthdate) / (1000 * 60 * 60 * 24 * 365.25));
        return (int) age;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String userID) {
        this.key = userID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(int y, int m, int d) {
        Calendar c = Calendar.getInstance();
        c.set(y, m, d);
        long bd = c.getTimeInMillis();
        this.birthdate = bd;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float inches) {
        this.height = inches;
    }

    public void setHeight(float inches, float feet) {
        this.height = feet * 12 + inches;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getActLevel() {
        return actLevel;
    }

    public void setActLevel(float actLevel) {
        this.actLevel = actLevel;
    }

    public int getGoal() {
        return goal;
    }

    public void setGoal(int goal) {
        this.goal = goal;
    }

    public int getBMR() {
        return bmr;
    }

    public void setBMR(int bmr) {
        this.bmr = bmr;
    }

    public int getDailyCalories() {
        return dailyCalories;
    }

    public void setDailyCalories(int dailyCalories) {
        this.dailyCalories = dailyCalories;
    }

    public int getMealCalories() {
        return mealCalories;
    }

    public void setMealCalories(int mealCalories) {
        this.mealCalories = mealCalories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return sex == user.sex &&
                Float.compare(user.height, height) == 0 &&
                Float.compare(user.weight, weight) == 0 &&
                Float.compare(user.actLevel, actLevel) == 0 &&
                Float.compare(user.goal, goal) == 0 &&
                bmr == user.bmr &&
                dailyCalories == user.dailyCalories &&
                mealCalories == user.mealCalories &&
                Objects.equals(key, user.key) &&
                Objects.equals(name, user.name) &&
                Objects.equals(birthdate, user.birthdate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, name, birthdate, sex, height, weight, actLevel, goal, bmr, dailyCalories, mealCalories);
    }

    @Override
    public String toString() {
        return "User{" +
                "userID='" + key + '\'' +
                ", name='" + name + '\'' +
                ", birthdate=" + birthdate +
                ", sex=" + sex +
                ", height=" + height +
                ", weight=" + weight +
                ", actLevel=" + actLevel +
                ", goal=" + goal +
                ", totalCalories=" + bmr +
                ", dailyCalories=" + dailyCalories +
                ", mealCalories=" + mealCalories +
                '}';
    }
}

package com.thetrio.ewfitness.Model;


public class CalorieCalc {

    public static int getBMR(User u) {
        float bmr;
        if (u.getSex() == "m") {
            //Male Formula
            bmr = (float)((4.536 * u.getWeight()) + (15.88 * u.getHeight()) - (5 * u.getAge()) + 5) * u.getActLevel();

        } else {
            //Female Formula
            bmr = (float)((4.536 * u.getWeight()) + (15.88 * u.getHeight()) - (5 * u.getAge()) - 161) * u.getActLevel();

        }

        return (int)Math.floor(bmr);
    }

    public static int getDailyCalories(User u) {
        return u.getBMR() + u.getGoal();
    }

    public static int getMealCalories(User u) {
        return (int)Math.floor(u.getDailyCalories() / 3);
    }
}

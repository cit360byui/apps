package com.thetrio.ewfitness.Model;

import android.app.Activity;
import android.app.AlertDialog;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


public class Account {
    private FirebaseAuth mAuth;

    public static void createAccount(final Activity act, String email, String pass) {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.createUserWithEmailAndPassword(email, pass)
                .addOnCompleteListener(act, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        AlertDialog alert = new AlertDialog.Builder(act).create();
                        alert.setTitle("Alert!");
                        String mess;
                        if (task.isSuccessful()) {
                            mess = "success";
                        } else {
                            mess = "failure" + task.getException();
                        }
                        alert.setMessage(mess);
                        alert.show();
                    }
                });
    }

    public void signIn(Activity act, String email, String pass) {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.signInWithEmailAndPassword(email, pass)
                .addOnCompleteListener(act, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            System.out.println("Success");
                        } else {
                            System.out.println("Failure");
                        }
                    }
                });
    }

    public void signOut() {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.signOut();
    }
}

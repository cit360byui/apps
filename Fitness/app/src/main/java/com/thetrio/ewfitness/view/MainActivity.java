package com.thetrio.ewfitness.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
/*import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;*/


/*import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.thetrio.ewfitness.Model.*;*/
import com.thetrio.ewfitness.R;

/*import java.io.OutputStream;
import java.io.PrintStream;*/

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstState) {
        try {
            super.onCreate(savedInstState);
            setContentView(R.layout.activity_main);
            if (savedInstState == null) {
                // Create the main fragment and place it
                // as the first fragment in this activity.
                Fragment frag = new StartActivity();
                FragmentTransaction trans =
                        getSupportFragmentManager().beginTransaction();
                trans.add(R.id.fragContainer, frag);
                trans.commit();
            }

        } catch (Exception ex) {
            //Log.e(TAG, ex.toString());
        }
    }

    public void openFirstPage(View FirstPage) {
        Fragment frag = new FirstPage();
        FragmentTransaction trans =
                getSupportFragmentManager().beginTransaction();
        trans.replace(R.id.fragContainer, frag);
        trans.commit();
    }
    public void openHome(View Home) {
        Fragment frag = new Home();
        FragmentTransaction trans =
                getSupportFragmentManager().beginTransaction();
        trans.replace(R.id.fragContainer, frag);
        trans.commit();
    }
    public void openCreate(View CreateAccount) {
        Fragment frag = new CreateAccount();
        FragmentTransaction trans =
                getSupportFragmentManager().beginTransaction();
        trans.replace(R.id.fragContainer, frag);
        trans.commit();
    }
/*    public void openNSD(View NSD) {
        Fragment frag = new NSD();
        FragmentTransaction trans =
                getSupportFragmentManager().beginTransaction();
        trans.replace(R.id.fragContainer, frag);
        trans.commit();
    }*/
    public void openHWAG(View HWAG) {
        Fragment frag = new HWAG();
        FragmentTransaction trans =
                getSupportFragmentManager().beginTransaction();
        trans.replace(R.id.fragContainer, frag);
        trans.commit();
    }
}


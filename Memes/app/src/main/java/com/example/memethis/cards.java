package com.example.memethis;

public class cards {
    private String liked;
    private String name;
    private String imageURL;
    public cards (String liked, String name, String imageURL) {
        this.liked = liked;
        this.name = name;
        this.imageURL = imageURL;
    }

    public String getLiked(){
        return liked;
    }
    public void setLiked(String liked) {
        this.liked = liked;
    }

    public String getName(){
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getImageURL(){
        return  imageURL;
    }
    public void setImageURL(String ImageURL) {
        this.imageURL = imageURL;
    }
}

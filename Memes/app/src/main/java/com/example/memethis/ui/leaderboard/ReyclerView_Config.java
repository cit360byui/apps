package com.example.memethis.ui.leaderboard;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.memethis.Memes;
import com.example.memethis.R;

import org.w3c.dom.Text;

import java.util.List;
// We haven't gotten this to work yet, but this will show the list of memes in ranking order using a recycler view
//The order will be based on how many "likes" a meme gets
public class ReyclerView_Config {
    private Context mContext;
    private MemeAdapter memeAdapter;
    public void setConfig(RecyclerView recyclerView, Context context, List<Memes> memes, List<String> strings) {
        mContext = context;
        memeAdapter = new MemeAdapter(memes, strings);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(memeAdapter);
    }


    class MemeItemView extends RecyclerView.ViewHolder {
        private TextView mMemeName;
        private ImageView mImageUrl;
        private TextView mRankId;

        private String id;

        public MemeItemView(ViewGroup parent) {
            super(LayoutInflater.from(mContext).
            inflate(R.layout.fragment_leaderboard, parent, false));

            mMemeName = (TextView) itemView.findViewById(R.id.memeName);
            mImageUrl = (ImageView) itemView.findViewById(R.id.imageUrl);
            mRankId = (TextView) itemView.findViewById(R.id.rankId);

        }

        public void bind(Memes meme, String id) {
            mMemeName.setText(meme.getName());
            mImageUrl.setImageURI(Uri.parse(meme.getImageUrl()));
            mRankId.setText(meme.getLiked());
            this.id = id;
        }
    }
    class MemeAdapter extends RecyclerView.Adapter<MemeItemView>{
        private List<Memes> memesList;
        private List<String> stringList;

        public MemeAdapter(List<Memes> memesList, List<String> stringList) {
            this.memesList = memesList;
            this.stringList = stringList;
        }

        @NonNull
        @Override
        public MemeItemView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MemeItemView(parent);
        }

        @Override
        public void onBindViewHolder(@NonNull MemeItemView holder, int position) {
            holder.bind(memesList.get(position), stringList.get(position));
        }

        @Override
        public int getItemCount() {
            return memesList.size();
        }
    }
}

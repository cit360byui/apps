package com.example.memethis;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FirebaseDatabaseHelper {
    private FirebaseDatabase mDatabase;
    private DatabaseReference mReferenceMemes;
    private List<Memes> memes = new ArrayList<>();

    public FirebaseDatabaseHelper() {

    }

    public interface DataStatus{
        void DataIsLoaded();
    }

    public FirebaseDatabaseHelper(FirebaseDatabase mDatabase) {
        mDatabase = FirebaseDatabase.getInstance();
        mReferenceMemes = mDatabase.getReference("memes");
    }

    public void readMemes(DataStatus dataStatus) {
        mReferenceMemes.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                memes.clear();
                List<String> keys = new ArrayList<>();
                for(DataSnapshot keyNode : dataSnapshot.getChildren()) {
                    keys.add(keyNode.getKey());
                    Memes meme = keyNode.getValue(Memes.class);
                    memes.add(meme);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

        }

        });
    }
}

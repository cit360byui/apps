package com.example.memethis;

public class Memes {
    private String imageUrl;
    private int liked;
    private int unliked;
    private String name;


    public Memes() {

    }

    public Memes(String imageUrl, int liked, int unliked, String name) {
        this.imageUrl = imageUrl;
        this.liked = liked;
        this.unliked = unliked;
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getLiked() {

        return liked;
    }

    public void setLiked(int liked) {

        this.liked = liked;
    }

    public int getUnliked() {

        return unliked;
    }

    public void setUnliked(int unliked) {

        this.unliked = unliked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

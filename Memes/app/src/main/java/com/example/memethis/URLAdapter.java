package com.example.memethis;

import android.content.Context;
import android.media.Image;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class URLAdapter extends BaseAdapter {
    private final ArrayList<String> allURLs;
    private final Context context;

    public URLAdapter(Context context, int item, int imageView, ArrayList<String> al) {
        this.context = context;
        allURLs = new ArrayList<>();
        allURLs.add("https://ih0.redbubble.net/image.520057004.8377/flat,750x,075,f-pad,750x1000,f8f8f8.jpg");
        allURLs.add("https://i.imgur.com/tGbaZCY.jpg");

    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public int getCount() {
        return allURLs.size();
    }

    @Override
    public Object getItem(int position) {
        return allURLs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imgView = null;
        if (convertView == null) {
            imgView = new ImageView(context);
        }
        else {
            imgView = (ImageView)convertView;
        }
        String url = allURLs.get(position);
        Picasso.get().load(url).into(imgView);
        return imgView;
    }
}

package org.famfortitude;

import android.location.Location;

import org.famfortitude.drawer.R;


public class DummyData {

	public static String[] names = new String[]{
			"Profile1",
			"Profile2",
			"Profile3",
			"Profile4",
			"Profile5",
			"Profile6",
			"Profile7",
			"Profile8",
			"Profile9",
			"Profile10",
			"Profile11",
			"Profile12",
			"Profile13",
			"Profile14",
			"Profile15",
	};

	public static String[] title = new String[]{
			"This is where the bio of Profile1 would go.",
			"This is where the bio of Profile2 would go.",
			"This is where the bio of Profile3 would go.",
			"This is where the bio of Profile4 would go.",
			"This is where the bio of Profile5 would go.",
			"This is where the bio of Profile6 would go.",
			"This is where the bio of Profile7 would go.",
			"This is where the bio of Profile8 would go.",
			"This is where the bio of Profile9 would go.",
			"This is where the bio of Profile10 would go.",
			"This is where the bio of Profile11 would go.",
			"This is where the bio of Profile12 would go.",
			"This is where the bio of Profile13 would go.",
			"This is where the bio of Profile14 would go.",
			"This is where the bio of Profile15 would go.",
	};

	public static String[] profileInfo = new String[]{
			names[0] + ", Location: Idaho, Age: 30, Occupation: Dentist",
			names[1] + ", Location: Idaho, Age: 30, Occupation: Dentist",
			names[2] + ", Location: Idaho, Age: 30, Occupation: Dentist",
			names[3] + ", Location: Idaho, Age: 30, Occupation: Dentist",
			names[4] + ", Location: Idaho, Age: 30, Occupation: Dentist",
			names[5] + ", Location: Idaho, Age: 30, Occupation: Dentist",
			names[6] + ", Location: Idaho, Age: 30, Occupation: Dentist",
			names[7] + ", Location: Idaho, Age: 30, Occupation: Dentist",
			names[8] + ", Location: Idaho, Age: 30, Occupation: Dentist",
			names[9] + ", Location: Idaho, Age: 30, Occupation: Dentist",
			names[10] + ", Location: Idaho, Age: 30, Occupation: Dentist",
			names[11] + ", Location: Idaho, Age: 30, Occupation: Dentist",
			names[12] + ", Location: Idaho, Age: 30, Occupation: Dentist",
			names[13] + ", Location: Idaho, Age: 30, Occupation: Dentist",
			names[14] + ", Location: Idaho, Age: 30, Occupation: Dentist",
	};

	public static int[] picturePath = new int[]{
			R.drawable.profile1,
			R.drawable.profile2,
			R.drawable.profile3,
			R.drawable.profile4,
			R.drawable.profile5,
			R.drawable.profile6,
			R.drawable.profile7,
			R.drawable.profile8,
			R.drawable.profile9,
			R.drawable.profile10,
			R.drawable.profile11,
			R.drawable.profile12,
			R.drawable.profile13,
			R.drawable.profile14,
			R.drawable.profile15,
	};
}

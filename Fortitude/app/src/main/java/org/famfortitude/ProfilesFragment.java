package org.famfortitude;


import android.os.Bundle;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.famfortitude.drawer.R;

import java.util.ArrayList;


public class ProfilesFragment extends Fragment {

	private ArrayList<String> mNames = new ArrayList<>();
	private ArrayList<String> mInfo = new ArrayList<>();
	private ArrayList<Integer> mImages = new ArrayList<>();

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
			@Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.profiles, container, false);

		RecyclerView recyclerView = view.findViewById(R.id.profileRV);

		ListAdapter listAdapter = new ListAdapter(getActivity()) {
		};
		recyclerView.setAdapter(listAdapter);
		RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(
				getActivity());
		recyclerView.setLayoutManager(layoutManager);

		return view;
	}

	@Override
	public void onViewCreated(@NonNull View view,
			@Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		getActivity().setTitle("Profiles");
	}

	private void createList() {
		mNames.add("Profile1");
		mNames.add("Profile2");
		mNames.add("Profile3");
		mNames.add("Profile4");
		mNames.add("Profile5");
		mNames.add("Profile6");
		mNames.add("Profile7");
		mNames.add("Profile8");
		mNames.add("Profile9");
		mNames.add("Profile10");
		mNames.add("Profile11");
		mNames.add("Profile12");
		mNames.add("Profile13");
		mNames.add("Profile14");
		mNames.add("Profile15");
		mInfo.add(mNames.get(
				0) + ", Location: Idaho, Age: 30, Occupation: Dentist");
		mInfo.add(mNames.get(
				1) + ", Location: Idaho, Age: 30, Occupation: Dentist");
		mInfo.add(mNames.get(
				2) + ", Location: Idaho, Age: 30, Occupation: Dentist");
		mInfo.add(mNames.get(
				3) + ", Location: Idaho, Age: 30, Occupation: Dentist");
		mInfo.add(mNames.get(
				4) + ", Location: Idaho, Age: 30, Occupation: Dentist");
		mInfo.add(mNames.get(
				5) + ", Location: Idaho, Age: 30, Occupation: Dentist");
		mInfo.add(mNames.get(
				6) + ", Location: Idaho, Age: 30, Occupation: Dentist");
		mInfo.add(mNames.get(
				7) + ", Location: Idaho, Age: 30, Occupation: Dentist");
		mInfo.add(mNames.get(
				8) + ", Location: Idaho, Age: 30, Occupation: Dentist");
		mInfo.add(mNames.get(
				9) + ", Location: Idaho, Age: 30, Occupation: Dentist");
		mInfo.add(mNames.get(
				10) + ", Location: Idaho, Age: 30, Occupation: Dentist");
		mInfo.add(mNames.get(
				11) + ", Location: Idaho, Age: 30, Occupation: Dentist");
		mInfo.add(mNames.get(
				12) + ", Location: Idaho, Age: 30, Occupation: Dentist");
		mInfo.add(mNames.get(
				13) + ", Location: Idaho, Age: 30, Occupation: Dentist");
		mInfo.add(mNames.get(
				14) + ", Location: Idaho, Age: 30, Occupation: Dentist");
		mImages.add(R.drawable.profile1);
		mImages.add(R.drawable.profile2);
		mImages.add(R.drawable.profile3);
		mImages.add(R.drawable.profile4);
		mImages.add(R.drawable.profile5);
		mImages.add(R.drawable.profile6);
		mImages.add(R.drawable.profile7);
		mImages.add(R.drawable.profile8);
		mImages.add(R.drawable.profile9);
		mImages.add(R.drawable.profile10);
		mImages.add(R.drawable.profile11);
		mImages.add(R.drawable.profile12);
		mImages.add(R.drawable.profile13);
		mImages.add(R.drawable.profile14);
		mImages.add(R.drawable.profile15);
	}
}

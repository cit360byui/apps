package org.famfortitude;

import android.app.Dialog;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.famfortitude.drawer.R;

import java.util.List;


public class ListAdapter extends RecyclerView.Adapter {

	Context mContext;
	Dialog mDialog;
	List<DummyData> mData;

	public ListAdapter(Context mContext) {
		this.mContext = mContext;
	}

	@NonNull
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(
			@NonNull ViewGroup viewGroup, int i) {
		View view =
				LayoutInflater.from(mContext).inflate(R.layout.profile_item,
				viewGroup, false);
		final ListViewHolder vHolder = new ListViewHolder(view);

		mDialog = new Dialog(mContext);
		mDialog.setContentView(R.layout.single_profile_fragment);
		TextView dialogStory = mDialog.findViewById(R.id.story);
		TextView dialogInfo = mDialog.findViewById(R.id.pers_info);
		ImageView dialogImage = mDialog.findViewById(R.id.profile_image);
		dialogStory.setText(DummyData.names[vHolder.getAdapterPosition() + 1]);
		dialogInfo.setText(
				DummyData.profileInfo[vHolder.getAdapterPosition() + 1]);
		dialogImage.setImageResource(
				DummyData.picturePath[vHolder.getAdapterPosition() + 1]);
		mDialog.setTitle(DummyData.names[vHolder.getAdapterPosition() + 1]);


		vHolder.mItemProfile.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Toast.makeText(mContext, "Test Click" + String.valueOf(
						vHolder.getAdapterPosition() + 1),
						Toast.LENGTH_SHORT).show();
				mDialog.show();
			}
		});
		return new ListViewHolder(view);
	}

	@Override
	public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder,
			int i) {
		((ListViewHolder)viewHolder).bindView(i);
	}

	@Override
	public int getItemCount() {

		return DummyData.names.length;
	}

	private class ListViewHolder extends RecyclerView.ViewHolder {

		private LinearLayout mItemProfile;
		private TextView mItemText;
		private ImageView mItemImage;
		private TextView mName;

		public ListViewHolder(View itemView) {
			super(itemView);
			mItemProfile = itemView.findViewById(R.id.profile);
			mItemText = itemView.findViewById(R.id.itemText);
			mItemImage = itemView.findViewById(R.id.itemImage);
			mName = itemView.findViewById(R.id.itemName);
		}

		public void bindView(int position) {
			mItemText.setText(DummyData.title[position]);
			mItemImage.setImageResource(DummyData.picturePath[position]);
			mName.setText(DummyData.names[position]);

		}

	}
}

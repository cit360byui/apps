package org.famfortitude;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.famfortitude.drawer.R;


public class SingleProfileFragment extends Fragment {

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
			@Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.single_profile_fragment,
				container, false);

		RecyclerView recyclerView = view.findViewById(R.id.profileRV);

		ListAdapter listAdapter = new ListAdapter(getActivity()) {
		};
		recyclerView.setAdapter(listAdapter);
		RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(
				getActivity());
		recyclerView.setLayoutManager(layoutManager);

		return view;
	}

	@Override
	public void onViewCreated(@NonNull View view,
			@Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		getActivity().setTitle("Profiles");
	}
}


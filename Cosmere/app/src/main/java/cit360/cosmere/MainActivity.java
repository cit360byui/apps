/*
This app is meant to be a quick reference app for information about the
Cosmere, a fictional universe developed by Brandon Sanderson in his book series.
This app is based upon a main page with buttons for each of the planetary
systems in this universe. Each file in the view folder is for the different
fragments pertaining to each system. Each fragment in the layouts folder is
for the main fragments, and the fragements for each system. All files are
named for their fragment and system. 
*/
package cit360.cosmere;

import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.google.android.material.navigation.NavigationView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.MenuItem;


import cit360.cosmere.View.AboutFrag;
import cit360.cosmere.View.CosmereFrag;
import cit360.cosmere.View.MainFrag;
import cit360.cosmere.View.NalthisFrag;
import cit360.cosmere.View.RosharFrag;
import cit360.cosmere.View.ScadrialFrag;
import cit360.cosmere.View.SelFrag;
import cit360.cosmere.View.SunFrag;
import cit360.cosmere.View.TaldainFrag;
import cit360.cosmere.View.ThrenodyFrag;
import cit360.cosmere.View.YolenFrag;


public class MainActivity extends AppCompatActivity
		implements NavigationView.OnNavigationItemSelectedListener {

	public static final String TAG = "Comsere";
	private DrawerLayout drawer;


	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// code for the tool bar and implemeneting the nav drawer
		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		drawer = findViewById(R.id.drawer_layout);
		NavigationView navigationView = findViewById(R.id.nav_view);
		navigationView.setNavigationItemSelectedListener(this);

		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer,
				toolbar,
				R.string.navigation_drawer_open,
				R.string.navigation_drawer_close);

		drawer.addDrawerListener(toggle);
		toggle.syncState();

		getSupportFragmentManager().beginTransaction().replace(
				R.id.fragContainer,
				new MainFrag()).commit();
		navigationView.setCheckedItem(R.id.cosmere);

		// code to connect the frag_main fragment to the main activity.
		try {

			if (savedInstanceState == null) {
				//create the main fragment, and place it as the first fragment
				// in activity
				Fragment frag = new MainFrag();
				FragmentTransaction trans =
						getSupportFragmentManager().beginTransaction();
				trans.add(R.id.fragContainer, frag);
				trans.commit();
			}
		}
		catch (Exception ex) {
			Log.e(TAG + " 1", ex.toString());
		}
	}

	// method to handle the navigation drawer buttons.
	@Override
	public boolean onNavigationItemSelected(@NonNull MenuItem item) {
		switch (item.getItemId()) {
			case R.id.home:
				getSupportFragmentManager().beginTransaction().replace(
						R.id.fragContainer,
						new MainFrag()).commit();
				break;
			case R.id.cosmere:
				getSupportFragmentManager().beginTransaction().replace(
						R.id.fragContainer,
						new CosmereFrag()).addToBackStack("frag_main").commit();
				break;
			case R.id.fots:
				getSupportFragmentManager().beginTransaction().replace(
						R.id.fragContainer,
						new SunFrag()).addToBackStack("frag_main").commit();
				break;
			case R.id.nalthis:
				getSupportFragmentManager().beginTransaction().replace(
						R.id.fragContainer,
						new NalthisFrag()).addToBackStack("frag_main").commit();
				break;
			case R.id.roshar:
				getSupportFragmentManager().beginTransaction().replace(
						R.id.fragContainer,
						new RosharFrag()).addToBackStack("frag_main").commit();
				break;
			case R.id.scadrial:
				getSupportFragmentManager().beginTransaction().replace(
						R.id.fragContainer,
						new ScadrialFrag()).addToBackStack(
						"frag_main").commit();
				break;
			case R.id.sel:
				getSupportFragmentManager().beginTransaction().replace(
						R.id.fragContainer,
						new SelFrag()).addToBackStack("frag_main").commit();
				break;
			case R.id.taldain:
				getSupportFragmentManager().beginTransaction().replace(
						R.id.fragContainer,
						new TaldainFrag()).addToBackStack("frag_main").commit();
				break;
			case R.id.threnody:
				getSupportFragmentManager().beginTransaction().replace(
						R.id.fragContainer,
						new ThrenodyFrag()).addToBackStack(
						"frag_main").commit();
				break;
			case R.id.yolen:
				getSupportFragmentManager().beginTransaction().replace(
						R.id.fragContainer,
						new YolenFrag()).addToBackStack("frag_main").commit();
				break;
			case R.id.about:
				getSupportFragmentManager().beginTransaction().replace(
						R.id.fragContainer,
						new AboutFrag()).addToBackStack("frag_main").commit();
				break;
			case R.id.coppermind:
				Intent intentC = new Intent("android.intent.action.VIEW",
						Uri.parse(
								"https://coppermind" +
										".net/wiki/Coppermind:Welcome"));
				startActivity(intentC);
				break;
			case R.id.seventeenth:
				Intent intentS = new Intent("android.intent.action.VIEW",
						Uri.parse("https://www" +
								".17thshard.com/forum/"));
				startActivity(intentS);
				break;
		}

		drawer.closeDrawer(GravityCompat.START);
		return true;
	}

	@Override
	public void onBackPressed() {
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		}
		else {
			super.onBackPressed();
		}
	}
}

package cit360.cosmere.View;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cit360.cosmere.R;

public class AboutFrag extends Fragment {

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstState) {
        return inflater.inflate(R.layout.frag_about, container, false);
    }
}

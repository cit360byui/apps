package cit360.cosmere.View;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import cit360.cosmere.MainActivity;
import cit360.cosmere.R;


public class MainFrag extends Fragment {
	private View view;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container,
			Bundle savedInstState) {
		view = null;
		try {
			super.onCreateView(inflater, container, savedInstState);
			view = inflater.inflate(R.layout.frag_main, container, false);

			//get buttons and set handlers
			ImageButton cosmere = view.findViewById(R.id.cosmere);
			//cosmere.setBackgroundColor(Color.TRANSPARENT);
			cosmere.setOnClickListener(new HandleCosmere());

			Button roshar = view.findViewById(R.id.roshar);
			//roshar.setBackgroundColor(Color.TRANSPARENT);
			roshar.setOnClickListener(new HandleRoshar());

			Button nalthis = view.findViewById(R.id.nalthis);
			//nalthis.setBackgroundColor(Color.TRANSPARENT);
			nalthis.setOnClickListener(new HandleNalthis());

			Button yolen = view.findViewById(R.id.yolen);
			//yolen.setBackgroundColor(Color.TRANSPARENT);
			yolen.setOnClickListener(new HandleYolen());

			Button sun = view.findViewById(R.id.sun);
			sun.setOnClickListener(new HandleSun());
			//sun.setBackgroundColor(Color.TRANSPARENT);

			Button taldain = view.findViewById(R.id.taldain);
			taldain.setOnClickListener(new HandleTaldain());
			//taldain.setBackgroundColor(Color.TRANSPARENT);

			Button scadrial = view.findViewById(R.id.scadrial);
			scadrial.setOnClickListener(new HandleScadrial());
			//scadrial.setBackgroundColor(Color.TRANSPARENT);

			Button sel = view.findViewById(R.id.sel);
			sel.setOnClickListener(new HandleSel());
			//sel.setBackgroundColor(Color.TRANSPARENT);

			Button threnody = view.findViewById(R.id.threnody);
			threnody.setOnClickListener(new HandleThrenody());
			//threnody.setBackgroundColor(Color.TRANSPARENT);

			Button bsWebsite = view.findViewById(R.id.bsWebsite);
			bsWebsite.setOnClickListener(new HandlebsWebsite());
			//bsWebsite.setBackgroundColor(Color.TRANSPARENT);

		}
		catch (Exception ex) {
			Log.e(MainActivity.TAG, ex.toString());
		}
		return view;
	}

	//test handler to test buttons and button positions
	//code to test opening links in users browser of choice
	private final class HandleButtonsTest implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			AppCompatActivity act = (AppCompatActivity)getActivity();

			Toast.makeText(act, "Button pressed", Toast.LENGTH_LONG).show();
			try {
				Intent test = new Intent(Intent.ACTION_VIEW);
				test.setData(Uri.parse("http://www.byui.edu"));
				startActivity(test);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	// button handlers for the planet buttons in the screen
	private final class HandleRoshar implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			AppCompatActivity act = (AppCompatActivity)getActivity();

			RosharFrag roshar = new RosharFrag();
			FragmentTransaction trans =
					act.getSupportFragmentManager().beginTransaction();
			trans.setCustomAnimations(R.anim.enter_from_right,
					R.anim.exit_to_right,
					R.anim.enter_from_right, R.anim.exit_to_right);
			trans.replace(R.id.fragContainer, roshar);
			trans.addToBackStack("frag_main");
			trans.commit();
		}
	}

	private final class HandleNalthis implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			AppCompatActivity act = (AppCompatActivity)getActivity();

			NalthisFrag nalthis = new NalthisFrag();
			FragmentTransaction trans =
					act.getSupportFragmentManager().beginTransaction();
			trans.setCustomAnimations(R.anim.enter_from_right,
					R.anim.exit_to_right,
					R.anim.enter_from_right, R.anim.exit_to_right);
			trans.replace(R.id.fragContainer, nalthis);
			trans.addToBackStack("frag_main");
			trans.commit();
		}
	}

	private final class HandleYolen implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			AppCompatActivity act = (AppCompatActivity)getActivity();

			YolenFrag yolen = new YolenFrag();
			FragmentTransaction trans =
					act.getSupportFragmentManager().beginTransaction();
			trans.setCustomAnimations(R.anim.enter_from_right,
					R.anim.exit_to_right,
					R.anim.enter_from_right, R.anim.exit_to_right);
			trans.replace(R.id.fragContainer, yolen);
			trans.addToBackStack("frag_main");
			trans.commit();
		}
	}

	private final class HandleSun implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			AppCompatActivity act = (AppCompatActivity)getActivity();

			SunFrag sun = new SunFrag();
			FragmentTransaction trans =
					act.getSupportFragmentManager().beginTransaction();
			trans.setCustomAnimations(R.anim.enter_from_right,
					R.anim.exit_to_right,
					R.anim.enter_from_right, R.anim.exit_to_right);
			trans.replace(R.id.fragContainer, sun);
			trans.addToBackStack("frag_main");
			trans.commit();
		}
	}

	private final class HandleTaldain implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			AppCompatActivity act = (AppCompatActivity)getActivity();

			TaldainFrag taldain = new TaldainFrag();
			FragmentTransaction trans =
					act.getSupportFragmentManager().beginTransaction();
			trans.setCustomAnimations(R.anim.enter_from_right,
					R.anim.exit_to_right,
					R.anim.enter_from_right, R.anim.exit_to_right);
			trans.replace(R.id.fragContainer, taldain);
			trans.addToBackStack("frag_main");
			trans.commit();
		}
	}

	private final class HandleScadrial implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			AppCompatActivity act = (AppCompatActivity)getActivity();

			ScadrialFrag scadrial = new ScadrialFrag();
			FragmentTransaction trans =
					act.getSupportFragmentManager().beginTransaction();
			trans.setCustomAnimations(R.anim.enter_from_right,
					R.anim.exit_to_right,
					R.anim.enter_from_right, R.anim.exit_to_right);
			trans.replace(R.id.fragContainer, scadrial);
			trans.addToBackStack("frag_main");
			trans.commit();
		}
	}

	private final class HandleSel implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			AppCompatActivity act = (AppCompatActivity)getActivity();

			SelFrag sel = new SelFrag();
			FragmentTransaction trans =
					act.getSupportFragmentManager().beginTransaction();
			trans.setCustomAnimations(R.anim.enter_from_right,
					R.anim.exit_to_right,
					R.anim.enter_from_right, R.anim.exit_to_right);
			trans.replace(R.id.fragContainer, sel);
			trans.addToBackStack("frag_main");
			trans.commit();
		}
	}

	private final class HandleThrenody implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			AppCompatActivity act = (AppCompatActivity)getActivity();

			ThrenodyFrag threnody = new ThrenodyFrag();
			FragmentTransaction trans =
					act.getSupportFragmentManager().beginTransaction();
			trans.setCustomAnimations(R.anim.enter_from_right,
					R.anim.exit_to_right,
					R.anim.enter_from_right, R.anim.exit_to_right);
			trans.replace(R.id.fragContainer, threnody);
			trans.addToBackStack("frag_main");
			trans.commit();
		}
	}

	private final class HandleCosmere implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			AppCompatActivity act = (AppCompatActivity)getActivity();

			CosmereFrag cosmere = new CosmereFrag();
			FragmentTransaction trans =
					act.getSupportFragmentManager().beginTransaction();
			trans.setCustomAnimations(R.anim.enter_from_top,
					R.anim.exit_to_top,
					R.anim.enter_from_top, R.anim.exit_to_top);
			trans.replace(R.id.fragContainer, cosmere);
			trans.addToBackStack("frag_main");
			trans.commit();
		}
	}

	private final class HandlebsWebsite implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			try {
				Intent test = new Intent(Intent.ACTION_VIEW);
				test.setData(Uri.parse("https://brandonsanderson.com/"));
				startActivity(test);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}


}

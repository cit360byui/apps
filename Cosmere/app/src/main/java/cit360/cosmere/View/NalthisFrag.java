package cit360.cosmere.View;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import cit360.cosmere.MainActivity;
import cit360.cosmere.R;


public class NalthisFrag extends Fragment {
	public View view;
	private ScaleGestureDetector mScaleGestureDetector;
	private float mScaleFactor = 1.0f;
	private ImageView mImageView;


	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
			ViewGroup container, Bundle savedinstState) {

		try {
			super.onCreateView(inflater, container, savedinstState);
			view = inflater.inflate(R.layout.frag_nalthis, container, false);

			Button more = view.findViewById(R.id.nalthislink);
			more.setOnClickListener(new LinkHandler());

			Activity act = getActivity();

			mImageView = view.findViewById(R.id.imageView);
			mScaleGestureDetector = new ScaleGestureDetector(act,
					new ScaleListener());
		}
		catch (Exception ex) {
			Log.e(MainActivity.TAG, ex.toString());
		}
		return view;


	}


	public boolean onTouchEvent(MotionEvent motionEvent) {
		mScaleGestureDetector.onTouchEvent(motionEvent);
		return true;
	}

	private class ScaleListener
			extends ScaleGestureDetector.SimpleOnScaleGestureListener {
		@Override
		public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
			mScaleFactor *= scaleGestureDetector.getScaleFactor();
			mScaleFactor = Math.max(0.1f,
					Math.min(mScaleFactor, 10.0f));
			mImageView.setScaleX(mScaleFactor);
			mImageView.setScaleY(mScaleFactor);
			return true;
		}
	}

	private final class LinkHandler implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			try {
				Intent test = new Intent(Intent.ACTION_VIEW);
				test.setData(Uri.parse("https://coppermind.net/wiki/Nalthis"));
				startActivity(test);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}

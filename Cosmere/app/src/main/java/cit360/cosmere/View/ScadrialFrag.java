package cit360.cosmere.View;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import cit360.cosmere.MainActivity;
import cit360.cosmere.R;


public class ScadrialFrag extends Fragment {
	public View view;

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
			ViewGroup container, Bundle savedinstState) {

		try {
			super.onCreateView(inflater, container, savedinstState);
			view = inflater.inflate(R.layout.frag_scadrial, container, false);

			Button more = view.findViewById(R.id.scadriallink);
			more.setOnClickListener(new LinkHandler());
		}
		catch (Exception ex) {
			Log.e(MainActivity.TAG, ex.toString());
		}
		return view;
	}

	private final class LinkHandler implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			try {
				Intent test = new Intent(Intent.ACTION_VIEW);
				test.setData(Uri.parse(
						"https://coppermind.net/wiki/Scadrian_system"));
				startActivity(test);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}

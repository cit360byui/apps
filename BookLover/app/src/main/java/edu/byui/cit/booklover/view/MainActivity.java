package edu.byui.cit.booklover.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;

import edu.byui.cit.booklover.R;
import edu.byui.cit.booklover.view.MainFrag;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "BookLover";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            if (savedInstanceState ==null) {
                Fragment frag = new MainFrag();
                FragmentTransaction trans =
                        getSupportFragmentManager().beginTransaction();
                trans.add(R.id.fragContainer, frag);
                trans.commit();
            }
        }
        catch (Exception ex) {
            Log.e(TAG, ex.toString());
        }
    }
}

package edu.byui.cit.booklover.view;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;

import edu.byui.cit.booklover.model.AppDatabase;
import edu.byui.cit.booklover.model.Book;
import edu.byui.cit.booklover.model.BookDAO;
import edu.byui.cit.booklover.R;

import java.util.List;


final class BookAdapter
        extends RecyclerView.Adapter<BookAdapter.ViewHolder> {
    // A reference to the main activity
    private final MainActivity activity;

    // A reference to the project DAO to read and write to the Project table.
    private final BookDAO bDao;

    // A list of all projects stored in the Project table. This is an in
    // memory copy of the projects that are stored in the Project table
    // of the Room database.
    private final List<Book> dataset;


    BookAdapter(MainActivity activity) {
        // Tell the RecyclerView that the project keys are stable.
        setHasStableIds(true);

        this.activity = activity;

        // Todo: Get a reference to the project DAO.
        Context appContext = activity.getApplicationContext();
        AppDatabase db = AppDatabase.getInstance(appContext);
        bDao = db.getBookDAO();

        // Todo: Get a list of all the projects in the Project table.
        dataset = bDao.getAll();
    }

    @Override
    public int getItemCount() {
        // Todo: Return the number of elements that are
        // stored in the in memory list of projects.
        return dataset.size();
    }

    @Override
    public long getItemId(int index) {
        // Todo: Return the key of the project that is
        // stored in the list of projects at index.
        Book book = dataset.get(index);
        return book.getKey();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_book, parent, false); //Not sure if this is the correct frag!
        return new ViewHolder(view);
    }

    // This onBindViewHolder method will be called each time that the
    // WorkTime app displays a project in a row of the RecyclerView.
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int index) {
        try {
            holder.bind(index);
        } catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }
    }


    // Each ViewHolder object corresponds to one row in the RecyclerView.
    // Each ViewHolder object will hold two TextViews that display a
    // book to the user. The TextViews are defined in item_book.xml.
    final class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, View.OnLongClickListener, OnMenuItemClickListener {

        // References to the TextViews in this row.
        private final TextView txtTitle, txtAuthor; //these are from item_book

        // A reference to the project object that
        // is displayed (held) in this ViewHolder.
        private Book heldBook;


        ViewHolder(View view) {
            super(view);

            // Todo: Get a reference to each of the two
            // TextViews that are in the corresponding row.

            txtTitle = view.findViewById(R.id.txtTitle);
            txtAuthor = view.findViewById(R.id.txtAuthor);

            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        // Bind this ViewHolder to the project that is
        // stored at index in the list of all books.
        void bind(int index) {
            heldBook = dataset.get(index);

            // Todo: Display in the txtTitle TextView
            // the data that is in heldBook.

            txtTitle.setText(heldBook.getTitle());
            txtAuthor.setText(heldBook.getAuthor());

        }

        @Override
        public void onClick(View v) {
            Frag_reviewBook nextFrag = new Frag_reviewBook(heldBook);

            //Creating bundle to pass to Frag_reviewBook
            Bundle args = new Bundle();
            args.putSerializable("clickedBook", heldBook);
            nextFrag.setArguments(args);


            FragmentManager mgr = activity.getSupportFragmentManager();
            FragmentTransaction trans = mgr.beginTransaction();
            trans.replace(R.id.fragContainer, nextFrag, "findThisFragment");
            trans.addToBackStack(null);
            trans.commit();
        }


        // This onLongClick method will be called when the
        // user long presses one row in the RecyclerView.
        @Override
        public boolean onLongClick(View view) {
            try {
                // Create a popup menu and show it to the user.
                PopupMenu menu = new PopupMenu(activity, view);
                menu.getMenuInflater().inflate(R.menu.popup, menu.getMenu());
                menu.setOnMenuItemClickListener(this);
                menu.show();
            } catch (Exception ex) {
                Log.e(MainActivity.TAG, ex.toString());
            }
            return true;
        }

        // This onMenuItemClick method will be called when
        // the user presses an item on the popup menu.
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            boolean handled = false;
            try {
                switch (item.getItemId()) {
                    case R.id.itmDelete:
                        // Todo: Delete the project that the user long
                        // pressed (heldBook) from the Project table.

                        // Get the index of the project that
                        // the user long pressed (heldBook)
                        // from the in memory list of projects.
                        heldBook.getKey();
                        bDao.delete(heldBook);

                        // Todo: remove heldBook from
                        // the in memory list of projects.
                        dataset.remove(getAdapterPosition());

                        // Notify the RecyclerView that
                        // heldBook has been deleted.
                        notifyItemRemoved(getAdapterPosition());
                        handled = true;
                        break;
                }
            } catch (Exception ex) {
                Log.e(MainActivity.TAG, ex.toString());
            }
            return handled;
        }
    }

}

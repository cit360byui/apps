package edu.byui.cit.booklover.view;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.lang.String;

import edu.byui.cit.booklover.R;
import edu.byui.cit.booklover.model.AppDatabase;
import edu.byui.cit.booklover.model.Book;
import edu.byui.cit.booklover.model.BookDAO;

public class Book_Add_Frag extends Fragment {
    EditText userTitle;
    EditText userAuthor;
    EditText userDescription;
    EditText userGenre;
    CheckBox userPrintBook;
    CheckBox userEBook;
    CheckBox userAudioBook;

    @Override
    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = null;

        try {
            super.onCreateView(inflater, container, savedInstanceState);

            view = inflater.inflate(R.layout.add_book_frag, container, false);


            userTitle = view.findViewById(R.id.bookTitle);
            userAuthor = view.findViewById(R.id.author);
            userDescription = view.findViewById(R.id.description);
            userGenre = view.findViewById(R.id.storedGenre);
            userPrintBook = view.findViewById(R.id.rbPrintBook);
            userEBook = view.findViewById(R.id.rbEBook);
            userAudioBook = view.findViewById(R.id.rbAudioBook);


//            String inputTags = userGenre;
//            String currentTag = "";
//            List<String> tags = new ArrayList<>();
//
//            for(int i = 0; i < inputTags.length(); i++) {
//                if(inputTags.charAt(i) == ',') {
//                    tags.add(currentTag);
//                    currentTag = "";
//                } else{
//                    currentTag += inputTags.charAt(i);
//                }
//            }


            Button addButton = view.findViewById(R.id.addbutton);
            addButton.setOnClickListener(new AddBook());


        } catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }
        return view;
    }

    private final class AddBook implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Log.d("click", "onClick: ");
            Context appContext =  getContext();
            AppDatabase db = AppDatabase.getInstance(appContext);

            //Grab bookdoa
            BookDAO bdao = db.getBookDAO();

            //Cast user input variables into strings
            String userTitleString = userTitle.getText().toString();
            String userAuthString = userAuthor.getText().toString();
            String userDescString = userDescription.getText().toString();
            String userGenreString = userGenre.getText().toString();
            String userPrintString = userPrintBook.getText().toString();
            String userEBookString = userEBook.getText().toString();
            String userAudioString = userAudioBook.getText().toString();

            //Create new project
            Book newBook = new Book(userTitleString, userAuthString, userDescString, userGenreString, userPrintString, userEBookString, userAudioString);

            //Insert into database
            bdao.insert(newBook);

            //Return to MainFrag
            Activity act = getActivity();
            act.onBackPressed();
        }
    }

    private final class CancelBook implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            //Return to MainFrag
            Activity act = getActivity();
            act.onBackPressed();
        }
    }







}

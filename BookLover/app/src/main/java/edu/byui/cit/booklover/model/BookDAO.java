package edu.byui.cit.booklover.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;


@Dao
public abstract class BookDAO {
    @Query("SELECT COUNT(*) FROM Book")
    public abstract long count();

    @Query("SELECT `key`, title, author, review, genre, print, ebook, audio FROM Book")
    public abstract List<Book> getAll();

    @Query("SELECT * FROM Book WHERE `key` = :key")
    public abstract Book getByKey(long key);

    public long insert(Book book) {
        long id = insertH(book);
        book.setKey(id);
        return id;
    }

    @Insert
    abstract long insertH(Book book);

    @Update
    public abstract void update(Book book);

    @Delete
    public abstract void delete(Book book);

    @Query("DELETE FROM Book")
    public abstract void deleteAll();
}
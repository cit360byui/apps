package edu.byui.cit.booklover.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import edu.byui.cit.booklover.R;


public final class MainFrag extends Fragment {

    @Override
    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = null;

        try {
            super.onCreateView(inflater, container, savedInstanceState);

            view = inflater.inflate(R.layout.frag_main, container, false);


            MainActivity activity = (MainActivity) getActivity();

            //linking All button to book_list_frag
            Button allBtn = view.findViewById(R.id.txtAll);
            allBtn.setOnClickListener(new AllButton());

            //linking button to book_list_frag for ebooks
            Button eBookBtn = view.findViewById(R.id.txteBook);
            eBookBtn.setOnClickListener(new ebookButton());

            //linking button to book_list_frag for print books
            Button printBtn = view.findViewById(R.id.txtPrint);
            printBtn.setOnClickListener(new printButton());

            //linking button to book_list_frag for audio books
            Button audioBtn = view.findViewById(R.id.txtAudio);
            audioBtn.setOnClickListener(new audioBookButton());

            //linking floating action button to addBookFrag
            Button homeFAB = view.findViewById(R.id.homeFAB);
            homeFAB.setOnClickListener(new addBookButton());


        } catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }
        return view;
    }

    private final class AllButton implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            BookListFrag nextFrag =  new BookListFrag();
            FragmentManager mgr =  getActivity().getSupportFragmentManager();
            FragmentTransaction trans = mgr.beginTransaction();
            trans.replace(R.id.fragContainer, nextFrag, "findThisFragment");
            trans.addToBackStack(null);
            trans.commit();
        }
    }

    private final class ebookButton implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            BookListFrag nextFrag =  new BookListFrag();
            FragmentManager mgr =  getActivity().getSupportFragmentManager();
            FragmentTransaction trans = mgr.beginTransaction();
            trans.replace(R.id.fragContainer, nextFrag, "findThisFragment");
            trans.addToBackStack(null);
            trans.commit();
        }
    }

    private final class printButton implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            BookListFrag nextFrag =  new BookListFrag();
            FragmentManager mgr =  getActivity().getSupportFragmentManager();
            FragmentTransaction trans = mgr.beginTransaction();
            trans.replace(R.id.fragContainer, nextFrag, "findThisFragment");
            trans.addToBackStack(null);
            trans.commit();
        }
    }

    private final class audioBookButton implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            BookListFrag nextFrag =  new BookListFrag();
            FragmentManager mgr =  getActivity().getSupportFragmentManager();
            FragmentTransaction trans = mgr.beginTransaction();
            trans.replace(R.id.fragContainer, nextFrag, "findThisFragment");
            trans.addToBackStack(null);
            trans.commit();
        }
    }

    private final class addBookButton implements View.OnClickListener {
        @Override
        public  void onClick(View view) {
            Book_Add_Frag nextFrag = new Book_Add_Frag();
            FragmentManager mgr = getActivity().getSupportFragmentManager();
            FragmentTransaction trans = mgr.beginTransaction();
            trans.replace(R.id.fragContainer, nextFrag, "findThisFragment");
            trans.addToBackStack(null);
            trans.commit();
        }
    }


}

package edu.byui.cit.booklover.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.google.android.material.floatingactionbutton.FloatingActionButton;

import edu.byui.cit.booklover.R;

public final class BookListFrag extends Fragment {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter bookAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = null;

        try {
            super.onCreateView(inflater, container, savedInstanceState);

            view = inflater.inflate(R.layout.book_list_frag, container, false);

            recyclerView = view.findViewById(R.id.recycler_view);

            MainActivity activity = (MainActivity)getActivity(); //Does this need to be BookListFrag instead?

            //Linking Add Book Button to Book_Add_Frag.
            // Must be under MainActivity activity = (MainActivity)getActivity();
            Button homeFAB = view.findViewById(R.id.homeFAB);
            homeFAB.setOnClickListener(new addBookFrag());

            // use a linear layout manager
            layoutManager = new LinearLayoutManager(activity);
            recyclerView.setLayoutManager(layoutManager);

            bookAdapter = new BookAdapter(activity);
            recyclerView.setAdapter(bookAdapter);
            

        } catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }
        return view;
    }

    //OnClick for Add Book Button
    private final class addBookFrag implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Book_Add_Frag nextFrag = new Book_Add_Frag();
            FragmentManager mgr = getActivity().getSupportFragmentManager();
            FragmentTransaction trans = mgr.beginTransaction();
            trans.replace(R.id.fragContainer, nextFrag, "findThisFragment");
            trans.addToBackStack(null);
            trans.commit();
        }
    }
}

package edu.byui.cit.booklover.view;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import edu.byui.cit.booklover.R;
import edu.byui.cit.booklover.model.AppDatabase;
import edu.byui.cit.booklover.model.Book;
import edu.byui.cit.booklover.model.BookDAO;

public class Frag_reviewBook extends Fragment {
    private EditText titleEdit;
    private EditText authorEdit;
    private EditText genreEdit;
    private EditText reviewEdit;


    private Book heldBook;

    Frag_reviewBook(Book heldBook) {
        this.heldBook = heldBook;
    }


    @Override
    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = null;
        try {
            super.onCreateView(inflater, container, savedInstanceState);

            view = inflater.inflate(R.layout.review_book_frag, container, false);

            MainActivity activity = (MainActivity) getActivity();


//  MAY OR MAY NOT USE THE BACK BUTTON
//            Button backbttn = view.findViewById(R.id.backbttn);//change the getview() to a variable
//            backbttn.setOnClickListener(new HandleBack());// On click listener

            //Accepting Bundle and unpacking it to fill EditText

             titleEdit = view.findViewById(R.id.storedTitle);
             authorEdit = view.findViewById(R.id.storedAuthor);
             genreEdit = view.findViewById(R.id.storedGenre);
             reviewEdit = view.findViewById(R.id.storedReview);

           //  rating = view.findViewById(R.id.storedRating);


            Bundle args = getArguments();
            Book clickedBook = (Book)args.getSerializable("clickedBook");
            titleEdit.setText(clickedBook.getTitle());
            authorEdit.setText(clickedBook.getAuthor());
            genreEdit.setText(clickedBook.getGenre());
            reviewEdit.setText(clickedBook.getReview());


//            //User updated data to store in database
//            String updatedTitle = storedTitle.getText().toString();
//            Log.d("updateTitle", "" + updatedTitle);
//
//            String updatedAuthor = storedAuthor.getText().toString();
//            Log.d("updateAuthor", "" + updatedAuthor);
//
//            String updatedReview = storedReview.getText().toString();
//            Log.d("updateReview", "" + updatedReview);

            Button updateBttn = view.findViewById(R.id.updateBttn);
            updateBttn.setOnClickListener(new HandleEdit());// On click listener

        } catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }
        return view;
    }



    //OnClick for Add Book Button
    private final class HandleEdit implements View.OnClickListener {
            @Override
        public void onClick(View view) {
                Context appContext =  getContext();
                AppDatabase db = AppDatabase.getInstance(appContext);
                Log.d("update", "withinclickListener");
                //Grab bookdoa
                BookDAO bdao = db.getBookDAO();

                //Cast user input variables into strings
                String userTitleString = titleEdit.getText().toString();
                Log.d("titleEdit", ""+ userTitleString);
                String userAuthString = authorEdit.getText().toString();
                String userGenreString = genreEdit.getText().toString();
                String userDescString = reviewEdit.getText().toString();


                //Save the current book as Held book
                heldBook.setTitle(userTitleString);
                heldBook.setAuthor(userAuthString);
                heldBook.setGenre(userGenreString);
                heldBook.setReview(userDescString);


                bdao.update(heldBook);

                Toast saved = Toast.makeText(getContext(), "Book Updated", Toast.LENGTH_SHORT);

                saved.show();
                //Return to MainFrag
                Activity act = getActivity();
                act.onBackPressed();
        }
    }


    private final class HandleBack implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Activity act = getActivity();
            act.onBackPressed();
        }

    }
}

package edu.byui.cit.booklover;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import edu.byui.cit.booklover.view.MainActivity;

public final class SortByFrag extends Fragment {
    @Override
    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = null;

        try {
            super.onCreateView(inflater, container, savedInstanceState);

            view = inflater.inflate(R.layout.sort_by, container, false);


            MainActivity activity = (MainActivity) getActivity();

        } catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }
        return view;
    }
    private final class UpdateList implements View.OnClickListener {
        @Override
        public  void onClick(View view) { }
    }
}

package edu.byui.cit.booklover.model;


import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.Objects;

@Entity
public class Book implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private long key;

    private String title;
    private String author;
    private String review;
    private String genre;
    private String print;
    private String ebook;
    private String audio;

    @Ignore
    Book() { }

    @Ignore
    public Book(long key) {this.key = key;}

    @Ignore
    public Book( String title, String author, String review, String genre, String print, String ebook, String audio) {
        this.title = title;
        this.author = author;
        this.review = review;
        this.genre = genre;
        this.print = print;
        this.ebook = ebook;
        this.audio = audio;
    }

    public Book( long key, String title, String author, String review, String genre, String print, String ebook, String audio) {
        this.key = key;
        this.title = title;
        this.author = author;
        this.review = review;
        this.genre = genre;
        this.print = print;
        this.ebook = ebook;
        this.audio = audio;
    }

    public long getKey() { return key; }
    void setKey ( long key ) {  this.key = key; }

    public String getTitle() { return title; }
    public void setTitle(String title) {this.title = title;}

    public String getAuthor() { return author; }
    public void setAuthor(String author) {this.author = author;}

    public String getReview() { return review; }
    public void setReview(String review) {this.review = review;}

    public String getGenre() { return genre; }
    public void setGenre(String genre) {this.genre = genre;}

    public String getPrint() { return print; }
    public void setPrint(String print) { this.print = print; }

    public String getEbook() { return ebook; }
    public void setEbook(String ebook) {this.ebook = ebook;}

    public String getAudio() { return audio; }
    public void setAudio(String audio) {this.audio = audio;}

    @Override
    public boolean equals(Object obj) {
        boolean eq = (this == obj);
        if (!eq && obj != null && getClass() == obj.getClass()) {
            Book other = (Book) obj;
            eq = this.key == other.key &&
                    this.title.equals(other.title) &&
                    this.author.equals(other.author) &&
                    this.review.equals(other.review); //&&
//                    this.genre.equals(other.genre) &&
//                    this.print.equals(other.print) &&
//                    this.ebook.equals(other.ebook) &&
//                    this.audio.equals(other.audio);
        }
        return eq;
    }

    @Override
    public int hashCode() { return Objects.hash(key, title, author, review, genre, print, ebook, audio); }

    @Override
    public String toString() { return key + " " +  "Title:" + " " + title; }

}

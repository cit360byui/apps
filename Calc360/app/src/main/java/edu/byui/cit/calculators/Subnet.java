package edu.byui.cit.calculators;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.NumberFormat;

import edu.byui.cit.calc360.CalcFragment;
import edu.byui.cit.calc360.R;
import edu.byui.cit.widget.ButtonWrapper;
import edu.byui.cit.widget.ClickListener;
import edu.byui.cit.widget.TextChangeListener;
import edu.byui.cit.widget.WidgetWrapper;
import edu.byui.cit.widget.EditInteger;
import edu.byui.cit.widget.EditWrapper;
import edu.byui.cit.widget.TextWrapper;

import static edu.byui.cit.model.Computing.Subnet.*;


public class Subnet extends CalcFragment {
	private final HandleIPAddr handleIPAddr;
	private final HandleCIDR handleCIDR;
	private final HandleSubnet handleSubnet;
	private final HandleClass classA, classB, classC;
	private final NumberFormat fmtrInt;

	private EditInteger txtIP1, txtIP2, txtIP3, txtIP4,
			txtSub1, txtSub2, txtSub3, txtSub4, txtCIDR;
	private EditWrapper[] ipInputs, subInputs;
	private WidgetWrapper[] subDepends, toClear;
	private TextWrapper availableHosts, numberOfSubnets, networkAddress,
			broadcastAddress, wildcardMask;


	public Subnet() {
		// Call the constructor in the parent class.
		super();

		fmtrInt = NumberFormat.getInstance();
		handleIPAddr = new HandleIPAddr();
		classA = new HandleClass(24);
		classB = new HandleClass(16);
		classC = new HandleClass(8);
		handleCIDR = new HandleCIDR();
		handleSubnet = new HandleSubnet();
	}


	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this calculator.
		View view = inflater.inflate(R.layout.subnet, container, false);

		txtIP1 = new EditInteger(view, R.id.txtIP1, handleIPAddr);
		txtIP2 = new EditInteger(view, R.id.txtIP2, handleIPAddr);
		txtIP3 = new EditInteger(view, R.id.txtIP3, handleIPAddr);
		txtIP4 = new EditInteger(view, R.id.txtIP4, handleIPAddr);

		new ButtonWrapper(view, R.id.btnClassA, classA);
		new ButtonWrapper(view, R.id.btnClassB, classB);
		new ButtonWrapper(view, R.id.btnClassC, classC);
		txtCIDR = new EditInteger(view, R.id.txtCIDR, handleCIDR);

		txtSub1 = new EditInteger(view, R.id.txtSub1, handleSubnet);
		txtSub2 = new EditInteger(view, R.id.txtSub2, handleSubnet);
		txtSub3 = new EditInteger(view, R.id.txtSub3, handleSubnet);
		txtSub4 = new EditInteger(view, R.id.txtSub4, handleSubnet);

		numberOfSubnets = new TextWrapper(view, R.id.numberOfSubnets);
		availableHosts = new TextWrapper(view, R.id.availableHosts);
		networkAddress = new TextWrapper(view, R.id.networkAddress);
		broadcastAddress = new TextWrapper(view, R.id.broadcastAddress);
		wildcardMask = new TextWrapper(view, R.id.wildcardMask);

		ipInputs = new EditWrapper[]{ txtIP1, txtIP2, txtIP3, txtIP4 };
		subInputs = new EditWrapper[]{ txtSub1, txtSub2, txtSub3, txtSub4 };
		subDepends = new WidgetWrapper[]{ availableHosts, numberOfSubnets,
				networkAddress, broadcastAddress, wildcardMask, };
		toClear = new WidgetWrapper[]{ txtIP1, txtIP2, txtIP3, txtIP4,
				txtCIDR, txtSub1, txtSub2, txtSub3, txtSub4,
				availableHosts, numberOfSubnets, networkAddress,
				broadcastAddress, wildcardMask };

		new ButtonWrapper(view, R.id.btnClear, this);
		return view;
	}


	private final class HandleIPAddr implements TextChangeListener {
		@Override
		public void textChanged(EditWrapper source) {
			if (EditWrapper.allNotEmpty(ipInputs) &&
					EditWrapper.allNotEmpty(subInputs)) {
				String ip = txtIP1.getInt() + "." + txtIP2.getInt() +
						"." + txtIP3.getInt() + "." + txtIP4.getInt();
				String subnet = txtSub1.getInt() + "." + txtSub2.getInt() +
						"." + txtSub3.getInt() + "." + txtSub4.getInt();
				String netAddr = networkAddress(ip, subnet);
				String broadAddr = broadcastAddress(ip, subnet);
				networkAddress.setText(netAddr);
				broadcastAddress.setText(broadAddr);
			}
			else {
				networkAddress.clear();
				broadcastAddress.clear();
			}

			// Move focus if necessary.
			if (source == txtIP1) {
				if (txtIP1.getText().length() == 3) {
					txtIP2.requestFocus();
				}
			}
			else if (source == txtIP2) {
				if (txtIP2.getText().length() == 3) {
					txtIP3.requestFocus();
				}
			}
			else if (source == txtIP3) {
				if (txtIP3.getText().length() == 3) {
					txtIP4.requestFocus();
				}
			}
			else if (source == txtIP4) {
				if (txtIP4.getText().length() == 3) {
					txtSub1.requestFocus();
				}
			}
		}
	}


	private final class HandleClass implements ClickListener {
		private final String cidr;

		HandleClass(int cidr) {
			this.cidr = fmtrInt.format(cidr);
		}

		@Override
		public void clicked(WidgetWrapper source) {
			txtCIDR.setText(cidr);
			handleCIDR.textChanged(txtCIDR);
		}
	}


	private final class HandleCIDR implements TextChangeListener {
		@Override
		public void textChanged(EditWrapper source) {
			if (txtCIDR.notEmpty()) {
				int cidr = txtCIDR.getInt();
				String subnetMask = subFromCIDR(cidr);
				String[] parts = subnetMask.split("\\.");
				for (int i = 0; i < parts.length; ++i) {
					subInputs[i].setText(parts[i]);
				}
				computeSubnetDepends(source);
			}
			else {
				clearAll(subInputs);
				clearAll(subDepends);
			}
		}
	}


	private final class HandleSubnet implements TextChangeListener {
		@Override
		public void textChanged(EditWrapper source) {
			if (EditWrapper.allNotEmpty(subInputs)) {
				String subnet = txtSub1.getInt() + "." + txtSub2.getInt() +
						"." + txtSub3.getInt() + "." + txtSub4.getInt();
				int cidr = cidrFromSub(subnet);
				txtCIDR.setText(fmtrInt.format(cidr));
				computeSubnetDepends(source);
			}
			else {
				txtCIDR.clear();
				clearAll(subDepends);
			}

			// Move focus if necessary
			if (source == txtSub1) {
				if (txtSub1.getText().length() == 3) {
					txtSub2.requestFocus();
				}
			}
			else if (source == txtSub2) {
				if (txtSub2.getText().length() == 3) {
					txtSub3.requestFocus();
				}
			}
			else if (source == txtSub3) {
				if (txtSub3.getText().length() == 3) {
					txtSub4.requestFocus();
				}
			}
		}
	}

	private void computeSubnetDepends(EditWrapper source) {
		String subnet = txtSub1.getInt() + "." + txtSub2.getInt() +
				"." + txtSub3.getInt() + "." + txtSub4.getInt();
		double hosts = hostsFromSub(subnet);
		double nets = netsFromSub(subnet);
		String wildcard = wildcardFromSubnet(subnet);
		numberOfSubnets.setText(fmtrInt.format(nets));
		availableHosts.setText(fmtrInt.format(hosts));
		wildcardMask.setText(wildcard);
		handleIPAddr.textChanged(source);
	}


	@Override
	public void clicked(WidgetWrapper source) {
		clearAll(toClear);
	}
}

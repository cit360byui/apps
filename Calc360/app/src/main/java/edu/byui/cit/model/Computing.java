package edu.byui.cit.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public final class Computing {
	private Computing() {
	}


	public static final class PasswordAttack {
		private PasswordAttack() {
		}

		public static double getKeySpace(int domain, int length) {
			return Math.pow(domain, length);
		}

		public static double getTimeSeconds(double keyspace, int keysSeconds,
				int machines) {
			return keyspace / (keysSeconds * machines);
		}

		public static double getTimeHours(double keyspace, int keysSeconds,
				int machines) {
			return getTimeSeconds(keyspace, keysSeconds, machines) / 3600;
		}

		public static double getTimeDays(double keyspace, int keysSeconds,
				int machines) {
			return getTimeHours(keyspace, keysSeconds, machines) / 24;
		}

		public static double getTimeYears(double keyspace, int keysSeconds,
				int machines) {
			return getTimeDays(keyspace, keysSeconds, machines) / 365;
		}
	}


	public static final class Subnet {
		private Subnet() {
		}

		public static String broadcastAddress(String ip, String subnet) {
			checkSubnet(subnet);
			long longIP = longFromDot(ip);
			long longSubnet = longFromDot(subnet);
			int count = Long.bitCount(longSubnet);
			int shift = 32 - count;
			long ones = (1 << shift) - 1;
			long longBroad = ((longIP >>> shift) << shift) | ones;
			return dotFromLong(longBroad);
		}

		public static String networkAddress(String ip, String subnet) {
			checkSubnet(subnet);
			long longIP = longFromDot(ip);
			long longSubnet = longFromDot(subnet);
			int count = Long.bitCount(longSubnet);
			int shift = 32 - count;
			long longNetAddr = (longIP >>> shift) << shift;
			return dotFromLong(longNetAddr);
		}

		/**
		 * Calculates amount of networks from a binary
		 * string returns -1 if the subnet is invalid.
		 */
		public static int netsFromSub(String subnet) {
			checkSubnet(subnet);
			long longSubnet = longFromDot(subnet);
			int count = Long.bitCount(longSubnet);
			return 1 << (count % 8);
		}

		/**
		 * Calculates amount of hosts from a binary string
		 * returns -1 if the subnet is invalid.
		 */
		public static int hostsFromSub(String subnet) {
			checkSubnet(subnet);
			long longSubnet = longFromDot(subnet);
			int count = Long.bitCount(longSubnet);
			return (int)((1L << (32 - count)) - 2);
		}

		public static String subFromCIDR(int cidr) {
			long longSub = ((1L << cidr) - 1) << (32 - cidr);
			return dotFromLong(longSub);
		}

		public static int cidrFromSub(String subnet) {
			checkSubnet(subnet);
			long longSub = longFromDot(subnet);
			return Long.bitCount(longSub);
		}

		public static String wildcardFromSubnet(String subnet) {
			checkSubnet(subnet);
			long longSub = longFromDot(subnet);
			long longWild = ~longSub & 0xffffffffL;
			return dotFromLong(longWild);
		}

		/**
		 * Checks to see if the subnet is valid by passing it a binary
		 * string
		 */
		public static void checkSubnet(String subnet) {
			String binary = binaryFromDot(subnet);
			Pattern regex = Pattern.compile("\\A[1]+[0]+\\z");
			Matcher matcher = regex.matcher(binary);
			if (!matcher.find() || binary.length() != 32) {
				throw new IllegalArgumentException("invalid subnet: " + subnet);
			}
		}

//		public static String dotFromBinary(String binary) {
//			String dot = "invalid";
//			if (binary.length() == 32) {
//				int oct1 = Integer.parseInt(binary.substring(0, 8), 2);
//				int oct2 = Integer.parseInt(binary.substring(8, 16), 2);
//				int oct3 = Integer.parseInt(binary.substring(16, 24), 2);
//				int oct4 = Integer.parseInt(binary.substring(24), 2);
//				dot = oct1 + "." + oct2 + "." + oct3 + "." + oct4;
//			}
//			return dot;
//		}

		/** Converts dot separated octets into a string of 0's and 1's. */
		public static String binaryFromDot(String dot) {
			StringBuilder binary = new StringBuilder(32);
			String[] octets = dot.split("\\.");
			for (String octet : octets) {
				octet = Long.toBinaryString(Integer.parseInt(octet));
				int len = octet.length();
				if (len < 8) {
					octet = "00000000".substring(len) + octet;
				}
				binary.append(octet);
			}
			return binary.toString();
		}

		public static String dotFromLong(long n) {
			return ((n & 0xff000000L) >> 24) + "." +
					((n & 0xff0000) >> 16) + "." +
					((n & 0xff00) >> 8) + "." +
					(n & 0xff);
		}

		public static long longFromDot(String dot) {
			long n = 0;
			String[] octets = dot.split("\\.");
			for (String octet : octets) {
				int oct = Integer.parseInt(octet);
				n = (n << 8) | oct;
			}
			return n;
		}
	}
}

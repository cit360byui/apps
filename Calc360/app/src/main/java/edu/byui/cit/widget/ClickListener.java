package edu.byui.cit.widget;

public interface ClickListener {
	void clicked(WidgetWrapper source);
}

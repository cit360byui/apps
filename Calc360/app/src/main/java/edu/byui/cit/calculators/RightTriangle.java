package edu.byui.cit.calculators;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.NumberFormat;

import edu.byui.cit.calc360.Calc360;
import edu.byui.cit.calc360.R;
import edu.byui.cit.calc360.SolveSeries;
import edu.byui.cit.widget.ItemSelectedListener;
import edu.byui.cit.widget.SpinWrapper;
import edu.byui.cit.widget.WidgetWrapper;
import edu.byui.cit.widget.EditAngle;
import edu.byui.cit.widget.EditDecimal;
import edu.byui.cit.widget.EditWrapper;
import edu.byui.cit.widget.TextWrapper;
import edu.byui.cit.widget.SpinUnit;
import edu.byui.cit.units.Angle;
import edu.byui.cit.units.Unit;

import static edu.byui.cit.model.Geometry.RightTriangle.*;


public class RightTriangle extends SolveSeries {
	private final NumberFormat fmtrDec, fmtrAngle;
	private EditDecimal decA, decB, decHyp;
	private EditAngle decAlpha, decBeta;
	private EditAngle[] angles;
	private TextWrapper decPerim, decArea;
	private SpinUnit spinner;

	public RightTriangle() {
		super();
		fmtrDec = NumberFormat.getInstance();
		fmtrAngle = NumberFormat.getInstance();
		fmtrAngle.setMaximumFractionDigits(5);
	}


	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this calculator.
		View view = inflater.inflate(R.layout.right_triangle, container,
				false);

		Activity act = getActivity();
		spinner = new SpinUnit(act, view, R.id.spinner,
				Angle.getInstance(), R.array.triUnits,
				Calc360.KEY_ANGLE_UNITS, new AngleUnits());

		decA = new EditDecimal(view, R.id.decSideA, this);
		decB = new EditDecimal(view, R.id.decSideB, this);
		decHyp = new EditDecimal(view, R.id.decSideC, this);
		decAlpha = new EditAngle(view, R.id.decAlpha, this);
		decBeta = new EditAngle(view, R.id.decBeta, this);
		EditAngle decGamma = new EditAngle(view, R.id.decGamma, this);
		decPerim = new TextWrapper(view, R.id.decPerim);
		decArea = new TextWrapper(view, R.id.decArea);
		angles = new EditAngle[]{ decAlpha, decBeta, decGamma };

		// Call getRad to initialize the double radians value inside decGamma.
		decGamma.getRad(Angle.getInstance().get(Angle.deg));

		EditWrapper[] inputs = { decA, decB, decHyp, decAlpha, decBeta };
		WidgetWrapper[] toClear = {
				decA, decB, decHyp, decAlpha, decBeta, decPerim, decArea
		};

		Solver[] solvers = new Solver[]{
				// side1 && side2
				new Solver(new EditWrapper[]{ decA, decB },
						new WidgetWrapper[]{ decHyp, decAlpha, decBeta, decPerim, decArea }) {
					@Override
					public void solve() {
						solveSS();
					}
				},

				// side && hypotenuse
				new Solver(new EditWrapper[]{ decA, decHyp },
						new WidgetWrapper[]{ decB, decAlpha, decBeta, decPerim, decArea }) {
					@Override
					public void solve() {
						solveSH(decA, decAlpha, decB, decBeta);
					}
				},
				new Solver(new EditWrapper[]{ decB, decHyp },
						new WidgetWrapper[]{ decA, decAlpha, decBeta, decPerim, decArea }) {
					@Override
					public void solve() {
						solveSH(decB, decBeta, decA, decAlpha);
					}
				},

				// side && opposite angle
				new Solver(new EditWrapper[]{ decA, decAlpha },
						new WidgetWrapper[]{ decB, decHyp, decBeta, decPerim, decArea }) {
					@Override
					public void solve() {
						solveSO(decA, decAlpha, decB, decBeta);
					}
				},
				new Solver(new EditWrapper[]{ decB, decBeta },
						new WidgetWrapper[]{ decA, decHyp, decAlpha, decPerim, decArea }) {
					@Override
					public void solve() {
						solveSO(decB, decBeta, decA, decAlpha);
					}
				},

				// side && adjacent angle
				new Solver(new EditWrapper[]{ decA, decBeta },
						new WidgetWrapper[]{ decB, decHyp, decAlpha, decPerim, decArea }) {
					@Override
					public void solve() {
						solveSA(decA, decBeta, decB, decAlpha);
					}
				},
				new Solver(new EditWrapper[]{ decB, decAlpha },
						new WidgetWrapper[]{ decA, decHyp, decBeta, decPerim, decArea }) {
					@Override
					public void solve() {
						solveSA(decB, decAlpha, decA, decBeta);
					}
				},

				// hypotenuse && angle
				new Solver(new EditWrapper[]{ decHyp, decAlpha },
						new WidgetWrapper[]{ decA, decB, decBeta, decPerim, decArea }) {
					@Override
					public void solve() {
						solveHA(decAlpha, decA, decB, decBeta);
					}
				},
				new Solver(new EditWrapper[]{ decHyp, decBeta },
						new WidgetWrapper[]{ decA, decB, decAlpha, decPerim, decArea }) {
					@Override
					public void solve() {
						solveHA(decBeta, decB, decA, decAlpha);
					}
				},

				// one angle only
				new Solver(new EditWrapper[]{ decAlpha },
						new WidgetWrapper[]{ decBeta }) {
					@Override
					public void solve() {
						solveA(decAlpha, decBeta);
					}
				},
				new Solver(new EditWrapper[]{ decBeta },
						new WidgetWrapper[]{ decAlpha }) {
					@Override
					public void solve() {
						solveA(decBeta, decAlpha);
					}
				}
		};

		initialize(view, inputs, solvers, R.id.btnClear, toClear);
		return view;
	}


	private void solveSS() {
		double a = decA.getDec();
		double b = decB.getDec();
		double hyp = hypot(a, b);
		double alpha = angleOH(a, hyp);
		double beta = angleA(alpha);
		decHyp.setText(fmtrDec.format(hyp));
		Unit user = spinner.getSelectedItem();
		decAlpha.setText(fmtrAngle, user, alpha);
		decBeta.setText(fmtrAngle, user, beta);
		perimAndArea(a, b, hyp);
	}

	private void solveSH(EditDecimal decSide1, EditAngle decAngle1,
			EditDecimal decSide2, EditAngle decAngle2) {
		double side1 = decSide1.getDec();
		double hyp = decHyp.getDec();
		double side2 = sideSH(side1, hyp);
		double angle1 = angleOH(side1, hyp);
		double angle2 = angleA(angle1);
		decSide2.setText(fmtrDec.format(side2));
		Unit user = spinner.getSelectedItem();
		decAngle1.setText(fmtrAngle, user, angle1);
		decAngle2.setText(fmtrAngle, user, angle2);
		perimAndArea(side1, side2, hyp);
	}

	private void solveSO(EditDecimal decSide1, EditAngle decAngle1,
			EditDecimal decSide2, EditAngle decAngle2) {
		Unit user = spinner.getSelectedItem();
		double side1 = decSide1.getDec();
		double angle1 = decAngle1.getRad(user);
		double angle2 = angleA(angle1);
		double side2 = sideSO(side1, angle2);
		double hyp = hypot(side1, side2);
		decAngle2.setText(fmtrAngle, user, angle2);
		decSide2.setText(fmtrDec.format(side2));
		decHyp.setText(fmtrDec.format(hyp));
		perimAndArea(side1, side2, hyp);
	}

	private void solveSA(EditDecimal decSide1, EditAngle decAngle2,
			EditDecimal decSide2, EditAngle decAngle1) {
		Unit user = spinner.getSelectedItem();
		double side1 = decSide1.getDec();
		double angle2 = decAngle2.getRad(user);
		double angle1 = angleA(angle2);
		double side2 = sideSO(side1, angle2);
		double hyp = hypot(side1, side2);
		decHyp.setText(fmtrDec.format(hyp));
		decSide2.setText(fmtrDec.format(side2));
		decAngle1.setText(fmtrAngle, user, angle1);
		perimAndArea(side1, side2, hyp);
	}

	private void solveHA(EditAngle decAngle1, EditDecimal decSide1,
			EditDecimal decSide2, EditAngle decAngle2) {
		Unit user = spinner.getSelectedItem();
		double hyp = decHyp.getDec();
		double angle1 = decAngle1.getRad(user);
		double angle2 = angleA(angle1);
		double side1 = sideHO(hyp, angle1);
		double side2 = sideHO(hyp, angle2);
		decAngle2.setText(fmtrAngle, user, angle2);
		decSide1.setText(fmtrDec.format(side1));
		decSide2.setText(fmtrDec.format(side2));
		perimAndArea(side1, side2, hyp);
	}

	private void perimAndArea(double side1, double side2, double hyp) {
		double perim = perimeter(side1, side2, hyp);
		double area = area(side1, side2);
		decPerim.setText(fmtrDec.format(perim));
		decArea.setText(fmtrDec.format(area));
	}

	private void solveA(EditAngle decAngle1, EditAngle decAngle2) {
		Unit user = spinner.getSelectedItem();
		double angle1 = decAngle1.getRad(user);
		double angle2 = angleA(angle1);
		decAngle2.setText(fmtrAngle, user, angle2);
	}


	@Override
	protected void restorePrefs(SharedPreferences prefs) {
		spinner.restore(prefs, Angle.deg);
	}


	@Override
	protected void savePrefs(SharedPreferences.Editor editor) {
		spinner.save(editor);
	}


	private final class AngleUnits implements ItemSelectedListener {
		@Override
		public void itemSelected(SpinWrapper source, int pos, long id) {
			Unit user = spinner.getSelectedItem();
			for (EditAngle edit : angles) {
				if (edit.notEmpty()) {
					edit.setText(fmtrAngle, user);
				}
			}
		}
	}
}

package edu.byui.cit.widget;

import android.content.SharedPreferences;
import android.view.View;

import java.text.NumberFormat;


public final class EditPercent extends EditWrapper {
	public EditPercent(View parent, int resID) {
		super(parent, resID, null, null);
	}

	public EditPercent(View parent, int resID, String prefsKey) {
		super(parent, resID, prefsKey, null);
	}

	public EditPercent(View parent, int resID, TextChangeListener listener) {
		super(parent, resID, null, listener);
	}

	public EditPercent(View parent, int resID,
			String prefsKey, TextChangeListener listener) {
		super(parent, resID, prefsKey, listener);
	}


	@Override
	public void save(SharedPreferences.Editor editor) {
		if (isEmpty()) {
			editor.remove(prefsKey);
		}
		else {
			// TODO
		}
	}

	@Override
	public void restore(SharedPreferences prefs, NumberFormat fmtr) {
		// TODO
	}


	public double getPerc() throws NumberFormatException {
		return getPerc(getText());
	}

	public double getPerc(double deflt) throws NumberFormatException {
		return getPerc(getText(), deflt);
	}


	private static double getPerc(String str, double deflt)
			throws NumberFormatException {
		return str.length() == 0 ? deflt : getPerc(str);
	}

	private static double getPerc(String str) throws NumberFormatException {
		Number val;
		try {
			val = perFmtr.parse(str);
		}
		catch (Exception ex) {
			try {
				val = decFmtr.parse(str);
			}
			catch (Exception ex2) {
				val = Double.parseDouble(str);
			}
		}
		return val.doubleValue();
	}
}

package edu.byui.cit.calculators;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import edu.byui.cit.calc360.CalcFragment;
import edu.byui.cit.calc360.R;


public class TimeDiff extends CalcFragment implements View.OnClickListener {
	// create variables for text boxes and buttons
	private EditText hourEdOne;
	private EditText minuteEdOne;
	private EditText secondEdOne;

	private EditText hourEdTwo;
	private EditText minuteEdTwo;
	private EditText secondEdTwo;

	private TextView hourEdThree;
	private TextView minuteEdThree;
	private TextView secondEdThree;

	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstState) {
		View view = inflater.inflate(R.layout.time_diff, container, false);

		// assign IDs to corresponding variables
		hourEdOne = view.findViewById(R.id.editHourOne);
		minuteEdOne = view.findViewById(R.id.editMinOne);
		secondEdOne = view.findViewById(R.id.editSecOne);
		hourEdTwo = view.findViewById(R.id.editHourTwo);
		minuteEdTwo = view.findViewById(R.id.editMinTwo);
		secondEdTwo = view.findViewById(R.id.editSecTwo);
		hourEdThree = view.findViewById(R.id.editHourThree);
		minuteEdThree = view.findViewById(R.id.editMinThree);
		secondEdThree = view.findViewById(R.id.editSecThree);

		Button add = view.findViewById(R.id.btnAdd);
		Button sub = view.findViewById(R.id.btnSub);
		Button clear = view.findViewById(R.id.btnClear);

		// buttons will call onclick
		add.setOnClickListener(this);
		sub.setOnClickListener(this);
		clear.setOnClickListener(this);

		return view;
	}


	public void onClick(View view) {

		// if else statement to test if all text boxes are filled in
		if (!hourEdOne.getText().toString().isEmpty() && !minuteEdOne.getText().toString().isEmpty() && !secondEdOne.getText().toString().isEmpty()
				&& !hourEdTwo.getText().toString().isEmpty() && !minuteEdTwo.getText().toString().isEmpty() && !secondEdTwo.getText().toString().isEmpty()) {

			// convert strings into int
			int hoursOne = Integer.parseInt(hourEdOne.getText().toString());
			int minutesOne = Integer.parseInt(
					minuteEdOne.getText().toString());
			int secondsOne = Integer.parseInt(
					secondEdOne.getText().toString());

			int hoursTwo = Integer.parseInt(hourEdTwo.getText().toString());
			int minutesTwo = Integer.parseInt(
					minuteEdTwo.getText().toString());
			int secondsTwo = Integer.parseInt(
					secondEdTwo.getText().toString());

			// convert hours and minutes into seconds, then add all three
			// values together

			int hoursInSecondsOne = hoursOne * 60 * 60;
			int minutesInSecondsOne = minutesOne * 60;
			int timeOneTotal =
					hoursInSecondsOne + minutesInSecondsOne + secondsOne;

			int hoursInSecondsTwo = hoursTwo * 60 * 60;
			int minutesInSecondsTwo = minutesTwo * 60;
			int timeTwoTotal =
					hoursInSecondsTwo + minutesInSecondsTwo + secondsTwo;

			int totalSeconds = 0;

			// switch case to determine math based on which button was pressed
			switch (view.getId()) {
				case R.id.btnAdd:
					totalSeconds = timeOneTotal + timeTwoTotal;
					break;
				case R.id.btnSub:
					totalSeconds = timeOneTotal - timeTwoTotal;
					break;
				case R.id.btnClear:
					// clear button will fill all fields with zeroes
					hourEdOne.setText(R.string.zeroFill);
					minuteEdOne.setText(R.string.zeroFill);
					secondEdOne.setText(R.string.zeroFill);
					hourEdTwo.setText(R.string.zeroFill);
					minuteEdTwo.setText(R.string.zeroFill);
					secondEdTwo.setText(R.string.zeroFill);
			}

			//convert lump total of seconds back into hours, minutes and
			// seconds
			int hourTotal = totalSeconds / 3600;
			totalSeconds = totalSeconds - (hourTotal * 3600);
			int minTotal = totalSeconds / 60;
			totalSeconds = totalSeconds - (minTotal * 60);
			int secTotal = totalSeconds;

			// while loop to set time to actual hours
			while (hourTotal >= 24) {
				hourTotal -= 24;
			}
			// convert totals to strings
			String hourDisplay = Integer.toString(hourTotal);
			String minDisplay = Integer.toString(minTotal);
			String secDisplay = Integer.toString(secTotal);

			// send totals to corresponding text fields
			hourEdThree.setText(hourDisplay);
			minuteEdThree.setText(minDisplay);
			secondEdThree.setText(secDisplay);
		}
	}
}

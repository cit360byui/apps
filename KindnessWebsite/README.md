The Kindness Website


Fonts:
	IMPORT BOTH TOGETHER: @import url('https://fonts.googleapis.com/css?family=Comfortaa|Roboto');
	Heading: https://fonts.google.com/specimen/Comfortaa
			Import: @import url('https://fonts.googleapis.com/css?family=Comfortaa'); --specify at the top of the css stylesheet
			Specify in css: font-family: 'Comfortaa', cursive;
	Paragraph: https://fonts.google.com/specimen/Roboto
			Import: @import url('https://fonts.googleapis.com/css?family=Roboto'); --specify at the top of the css stylesheet
			Specify in css: font-family: 'Roboto', sans-serif;
			
			
Colors (Hex):
RED
	Main:
	CD2F30
		Secondary:
		7A0206
		A4191B
		E25755
		FF8C89
YELLOW
	Main:
	CB9C42
		Secondary:
		795619
		A3792D
		E0B662
		FFDD92
GREEN
	Main:
	009B42
		Secondary:
		005C1D
		007B30
		32AB5A
		61C87E
BLUE
	Main:
	2C3986
		Secondary:
		0B154F
		1B2869
		455094
		6973AD
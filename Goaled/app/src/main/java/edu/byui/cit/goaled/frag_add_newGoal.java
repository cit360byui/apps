package edu.byui.cit.goaled;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.Clock;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import edu.byui.cit.model.AppDatabase;
import edu.byui.cit.model.Goal;
import edu.byui.cit.model.GoalDAO;
import edu.byui.cit.widget.ButtonWrapper;
import edu.byui.cit.widget.CITFragment;
import edu.byui.cit.widget.ClickListener;
import edu.byui.cit.widget.DateChangeListener;
import edu.byui.cit.widget.DateWrapper;
import edu.byui.cit.widget.EditString;
import edu.byui.cit.widget.EditWrapper;
import edu.byui.cit.widget.SpinString;
import edu.byui.cit.widget.TextChangeListener;
import edu.byui.cit.widget.TextWrapper;
import edu.byui.cit.widget.WidgetWrapper;


public class frag_add_newGoal extends CITFragment {
	private SpinString spinner;
	private EditString goalNameBox;
	private Date endDate;
	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstState) {
		View view = inflater.inflate(R.layout.frag_add_newgoal, container, false);

        //this should get the date from the other fragment and put it on the date button
		MainActivity main =(MainActivity)getActivity();
		this.endDate = main.getDate();
		Button dateButton = view.findViewById(R.id.dateButton);

		String dateFormat ="EEE, MMM d, yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
		dateButton.setText(simpleDateFormat.format(endDate));


		this.goalNameBox = new EditString(view, R.id.goalName);
		this.spinner = new SpinString(view, R.id.spinner);

		//set the time spinner to 6:00 PM by default
		TimePicker pickerTime = getActivity().findViewById(R.id.timePicker);
		Calendar now = Calendar.getInstance();

		//these two setters are causing the app to not work
//		pickerTime.setCurrentHour(6);
		//pickerTime.setCurrentMinute(0);


		//set up text boxes to be ready
		Calendar calendar = new Calendar() {
			@Override
			protected void computeTime() {

			}

			@Override
			protected void computeFields() {

			}

			@Override
			public void add(int field, int amount) {

			}

			@Override
			public void roll(int field, boolean up) {

			}

			@Override
			public int getMinimum(int field) {
				return 0;
			}

			@Override
			public int getMaximum(int field) {
				return 0;
			}

			@Override
			public int getGreatestMinimum(int field) {
				return 0;
			}

			@Override
			public int getLeastMaximum(int field) {
				return 0;
			}
		};
//		//set the date to two months from today
		calendar.set(Calendar.MONTH, (calendar.get(Calendar.MONTH) + 2));
		// TODO: This line of code is causing an issue with linking the calendar
//		new DateWrapper(view, R.id.finishDate, calendar);
		new EditString(view, R.id.goalName);
//		new ButtonWrapper(view, R.id.finishDate, new onDateClickListener());
		//set up submit button to be clicked
		new ButtonWrapper(view, R.id.submitFormButton, new submitNewGoalListener());
		//this line is causing a crash
		new ButtonWrapper(view, R.id.dateButton, new onDateClickListener());

		return view;
	}

	private void addGoal(){
		Goal newGoal = new Goal();
		//set data in goal equal to data in form
		newGoal.setTitle(getView().findViewById(R.id.goalName).toString());
		//an empty date is initialized to be now
		newGoal.setStart(new Date());
		CheckBox check = (CheckBox)getView().findViewById(R.id.checkBoxOngoing);
		if(check.isChecked())
			newGoal.setEnd(null);
		else{
			newGoal.setEnd(endDate);
		}
		newGoal.setReportFreq(Goal.Frequency.valueOf(getView().findViewById(R.id.spinner).toString()));

        TimePicker pickerTime = getActivity().findViewById(R.id.timePicker);
        int hour = pickerTime.getCurrentHour();
        int minute = pickerTime.getCurrentMinute();

        // Tanner commented this out.  See Goal.java:
//        newGoal.setRemindTime(new Time(hour, minute, 0));
	}
	@Override
	protected String getTitle() {
		return "Add New Goal";
	}

	private class submitNewGoalListener implements ClickListener {
		@Override
		public void clicked(WidgetWrapper source) {
		    addGoal();




		    // below is old code, I am not sure what it does
			String goalName = goalNameBox.getText();
			if (!goalName.isEmpty()) {
				//TODO: extract data from the form
				// goalName holds the name of the goal as a string


				// frequencyChoice stores the user's choice as an integer based on position
				int frequencyChoice = spinner.getSelectedItemPosition();



				//extracting time from the form
				TimePicker pickerTime = getActivity().findViewById(R.id.timePicker);
				int hour = pickerTime.getCurrentHour();
				int minute = pickerTime.getCurrentMinute();


				//TODO: here we need to save the goal before moving back to the home screen

				// creating a test object for the database. VERY IMPORTANT!!! Inserting incomplete rows will break the database call
			Random rand = new Random();
			int n =rand.nextInt(200) + 1;
            edu.byui.cit.model.Goal test1 = new Goal();
			test1.setGoalID(n);
			test1.setTitle("milk the cow");
			test1.setDescription("literally go milk the cow");
			test1.setReportType(Goal.Type.text);
			test1.setReportFreq(Goal.Frequency.daily);
			Date startDate = new Date();
			test1.setStart(startDate);
			test1.setEnd(startDate);
			test1.setEnableReminder(true);
			// database call to clear table and then insert proper data. If table not cleared during testing, it won't work
			// remove clear table function when ready for production
			Context ctx = getActivity().getApplicationContext();
			GoalDAO gDao = AppDatabase.getInstance(ctx).getGoalDAO();
			gDao.clearTable();
			gDao.insert(test1);

				//once the goal is saved, go back to the main lander fragment
				//we need to pop the fragment off the fragment stack, taking us back to the main screen
				// -- similar to pressing the back button.
				getActivity().getFragmentManager().popBackStack();

				//output message to user that the goal was saved
				Toast toast = Toast.makeText(getActivity().getApplicationContext(), "Recorded Goal: \"" + goalName + "\", with fequency choice of "
						+ frequencyChoice + ". Remind at: " + hour + ":" + minute + "!", Toast.LENGTH_LONG);
				toast.show();
			}
			else{
				Toast toast = Toast.makeText(getActivity().getApplicationContext(), "STOP: You must specify a goal name!", Toast.LENGTH_SHORT);
				toast.show();
			}
		}
	}

	private class selectCompletionDateListener implements DateChangeListener {
		@Override
		public void afterChanged(DateWrapper source, int year, int month, int dayOfMonth) {
			//TODO: Insert updated date into the text box here
//			DateWrapper thePicker = source;
//
//			Calendar calendar = Calendar.getInstance();
//			int junk = calendar.MONTH;

//			Toast toast = Toast.makeText(getActivity().getApplicationContext(), "working with " + junk, Toast.LENGTH_LONG);
		}
	}


	private class onDateClickListener implements ClickListener {
		@Override
		public void clicked(WidgetWrapper source) {
			((MainActivity) getActivity()).switchFragment(new frag_calendarChoose(), new Bundle());
		}
	}
}
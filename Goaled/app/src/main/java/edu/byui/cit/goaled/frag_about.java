package edu.byui.cit.goaled;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.byui.cit.widget.CITFragment;

public class frag_about extends CITFragment {
    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstState) {
        View view = inflater.inflate(R.layout.about_page, container, false);
        return view;
    }

    @Override
    protected String getTitle() {
        return "About";
    }
}

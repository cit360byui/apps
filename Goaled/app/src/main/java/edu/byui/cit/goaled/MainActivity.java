package edu.byui.cit.goaled;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.support.v7.widget.Toolbar;

import java.util.Calendar;
import java.util.Date;

import edu.byui.cit.model.AppDatabase;
import edu.byui.cit.widget.CITFragment;
/*
	The idea behind this app is to give the user a simple way of creating a
	goal and then to track it. Some features we envisioned are to send the
	user a notification that reminds them to work on that goal and an easy
	way to hold themselves accountable. Another feature we intended to add
	was a graph/chart that the user can use to visually see their progress.

	This is what is in the app:
		Homelander is the main page that is where app starts at.
		Functioning room database,
		the app is split using fragments,
		a functional UI,
		Toasts that show the user what they recorded,
		and user input.

	TODO
		implement basic functionality for minimum viable product
		comment code to make it readable
		Figure out notifications,
		figure out how to add user input to database(not hardcoded values),
		figure out graphing data.
		finish about_page
 */

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "Goaled";

    private Date date = new Date();
    private DrawerLayout myGoalDrawer;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        try {

            setContentView(R.layout.main_activity);

            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            ActionBar actionbar = getSupportActionBar();
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);


            //The following lines pertain to making the Drawer, or navigation menu, appear
            myGoalDrawer = findViewById(R.id.drawerLayout);
            final NavigationView navigationView = findViewById(R.id.navView);
            navigationView.setNavigationItemSelectedListener(
                    new NavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(MenuItem menuItem) {
                            // set item as selected to persist highlight
                            menuItem.setChecked(true);
                            // close drawer when item is tapped
                            myGoalDrawer.closeDrawers();

                            // if (navigationView == findViewById(R.id...) Add code here to update the UI based on the item selected
                            // For example, swap UI fragments here
                            if (menuItem == findViewById(R.id.navAbout)){
                                setContentView(R.layout.about_page);
                                }
                            else if (menuItem == findViewById(R.id.navGoals)){
                                setContentView(R.layout.home_lander);
                                }
                            else {
                                setContentView(R.layout.frag_add_newgoal);
                            }

                            return true;
                        }
                    });

            //Daily notification time, intent, and alarm manager
            Calendar calendar = Calendar.getInstance();
//			Hard coded calendar values:
            //    calendar.set(Calendar.HOUR_OF_DAY, 18);
            //    calendar.set(Calendar.MINUTE, 30);
            calendar.add(Calendar.SECOND, 5);

            Intent intent = new Intent(getApplicationContext(),
                    NotificationReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                    getApplicationContext(), 100, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            //the alarm manager is used to run code while app is closed
            AlarmManager alarmManager = (AlarmManager) getSystemService(
                    ALARM_SERVICE);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
                    calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY,
                    pendingIntent);

            //testing for notifications

            //manually make a notification when app is opened
            sendBroadcast(intent);

            //end testing for notifications

            //populate the main screen area with current goals

            //go ahead and launch the main lander fragment
            // VERY IMPORTANT! This is how we switch to the main fragment
            if (savedInstanceState == null) {
                CITFragment frag = new frag_home_lander();
                FragmentTransaction trans = getFragmentManager()
                        .beginTransaction();
                trans.add(R.id.fragContainer, frag);
                trans.commit();
            }

            //test with filler data

        } catch (Exception ex) {
            Log.e(TAG, ex.toString());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                myGoalDrawer.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //code to handle our fragments
    void switchFragment(CITFragment fragment, Bundle bundle) {
        fragment.setArguments(bundle);
        FragmentTransaction trans = getFragmentManager().beginTransaction();
        trans.replace(R.id.fragContainer, fragment, "thing");
        trans.addToBackStack(null);
        trans.commit();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}


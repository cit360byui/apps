package edu.byui.cit.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.v4.widget.CircularProgressDrawable;

import java.sql.Time;
import java.util.Date;

//Creates SQLite table in the database using a data model class.
@Entity
public class Goal {
	public void setReportFreq() {
	}

	// I, Tanner, commented out the following because I
	// couldn't figure out the issue with compiling.  When
	// I did so, it failed because Date could not be converted
	// to Time.  See frag_add_newGoal.java for another portion
	// I commented out that calls the below methods.

//	public Time getRemindTime() {
//		return remindTime;
//	}
//
//	public void setRemindTime(Time remindTime) {
//		this.remindTime = remindTime;
//	}

	//Type of the report they would be reporting
	public enum Type {
		none,
		bool, //check box
		num,  // a number they would report for their progress
		text // a text string used by the user to report their progress
    }

	//This will define how often they will be reporting their progress
	public enum Frequency {
		none,
		hourly,
		daily,
		weekly,
		monthly,
		quarterly,
		yearly
    }
	//Primary Key= goalID column
	@PrimaryKey(autoGenerate = true)
	private int goalID;

	//title column
	private String title;

	//description column
	private String description;

	//report type column
	@ColumnInfo (name = "report_type")
	private Type reportType;

	//report frequency column
	@ColumnInfo (name = "report_frequency")
	private Frequency reportFreq;

	//start date column
	@ColumnInfo (name = "start_date")
	private java.util.Date start;

	//end date column
	@ColumnInfo (name = "end_date")
	private java.util.Date end;

//	private Time remindTime;

	//enable reminder column
	@ColumnInfo (name = "enable_reminder")
	private Boolean enableReminder;


    public Goal() {

    }

    //constructor
	public Goal(String title, String description, Type reportType, Frequency reportFreq, Date start, Date end, Boolean enableReminder) {
		this.title = title;
		this.description = description;
		this.reportType = reportType;
		this.reportFreq = reportFreq;
		this.start = start;
		this.end = end;
		this.enableReminder = enableReminder;
	}

	Boolean getEnableReminder() { return enableReminder; }

	public void setEnableReminder(Boolean enableReminder) { this.enableReminder = enableReminder; }

	public int getGoalID() {
		return goalID;
	}

	public void setGoalID(int goalID) {
		this.goalID = goalID;
	}

	public String getTitle() { return title; }

	public void setTitle(String title) { this.title = title; }

	public String getDescription() { return description; }

	public void setDescription(String description) { this.description = description; }

	public Frequency getReportFreq() {return reportFreq; }

	public void setReportFreq(Frequency reportFreq) { this.reportFreq = reportFreq; }

	public Type getReportType() {
		return reportType;
	}

	public void setReportType(Type reportType) {
		this.reportType = reportType;
	}

	public java.util.Date getStart() {
		return start;
	}

	public void setStart(java.util.Date start) {
		this.start = new Date(start.getTime());
	}

	public java.util.Date getEnd() {
		return end;
	}

	public void setEnd(java.util.Date end) {
		this.end = new Date(end.getTime());
	}

	@Override
	public boolean equals(Object obj2) {
		boolean equ = false;
		if (obj2 instanceof Goal) {
			Goal other = (Goal)obj2;
			equ = goalID == other.goalID &&
					title == other.title &&
					description == other.description &&
					reportType == other.reportType &&
					reportFreq == other.reportFreq &&
					compareObjects(start, other.start) &&
					compareObjects(end, other.end);
		}
		return equ;
	}

	private static boolean compareObjects(Object obj1, Object obj2) {
		return obj1 == null ? obj2 == null : obj1.equals(obj2);
	}

	@Override
	public String toString() {
		return "Goal: " +
				goalID + ", " +
				title + ", " +
				description + ", " +
 				reportType + ", " +
				reportFreq + ", " +
				start + ", " +
				end;
	}
}

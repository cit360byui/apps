package edu.byui.cit.goaled;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import java.text.SimpleDateFormat;
import java.util.Date;
//****************************************************************************************
//THIS FILE IS NOT LONGER BEING USED - WE HAVE SWITCHED TO FRAGMENTS INSTEAD OF ACTIVITIES
//****************************************************************************************

import edu.byui.cit.model.AppDatabase;
import edu.byui.cit.model.Goal;
import edu.byui.cit.model.GoalDAO;


public class AddCustomGoal extends AppCompatActivity {

    //global integer to hold the current radio button selection.
    private static int buttonSelection = 0;
    private static boolean checkedOngoing = false;

    private EditText txtGoalName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frag_add_newgoal);

        //populate the spinner with the frequency choices
        Spinner spinner = findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.frequencyChoices, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        //set up text boxes to be read
        txtGoalName = findViewById(R.id.goalName);
//        txtCompletiondate = findViewById(R.id.finishDate);

        //set up button to be clicked
        Button submitButton = findViewById(R.id.submitFormButton);
        submitButton.setOnClickListener(new NewGenericGoal());
    }


    public void onCheckBoxClicked(View view) {
        boolean isChecked = ((CheckBox) view).isChecked();

        switch(view.getId()){
            case R.id.checkBoxOngoing:
                checkedOngoing = isChecked;
        }
    }

    private class NewGenericGoal implements View.OnClickListener {
        @Override
        public void onClick(View submitButton){
            //put what happens when the RECORD! button is clicked.
            // collect data from the form.
            String goalName = txtGoalName.toString();

            Context ctx = getApplicationContext();
            GoalDAO dao = AppDatabase.getInstance(ctx).getGoalDAO();
            Goal newGoal = new Goal();

            //set data in goal equal to data in form
            newGoal.setTitle(findViewById(R.id.goalName).toString());
            newGoal.setReportFreq(); //an empty  date initialized to be nowq(Goal.Frequency.valueOf(findViewById(R.id.spinner).toString()));
            newGoal.setStart(new Date());

            //TODO get End Date
            //TODO check that end date is after start date
            //TODO see if it is ongoing
            //TODO set a unique ID



            //i am unsure if this needs to happen in the fragment or here

            //--state of check boxes and radio buttons are stored in global variables already defined.--

            //make a new row in the goal database with the data.
            //TODO: once database objects are ready, insert all of this data into a new row.

            //this is old test code, it is commented out to not mess with the app
            /*
            Goal test1 = new Goal();
            test1.setGoalID(1);

            dao.insert(test1);
            dao.getByID(1);

            System.out.println(test1);
            */

        }
    }
}

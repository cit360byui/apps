package edu.byui.cit.model;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface ProgressDAO {
    @Query("SELECT * FROM progress")
    List<Progress> getAll();

    @Query("SELECT * FROM progress WHERE progressID IN (:progressIDs)")
    List<Progress> getAllByID(int[] progressIDs);

    @Query("SELECT * FROM progress WHERE progressID = :id")
    Goal getByID(int id);

    @Insert
    void insertAll(Progress... progresses);

    @Delete
    void delete(Progress progress);

}

package edu.byui.cit.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.util.TableInfo;

import java.lang.annotation.Annotation;
import java.sql.Date;

@Entity(foreignKeys = {
        @ForeignKey(entity = Goal.class,
                    parentColumns = "goalID",
                    childColumns  = "goalID"
        )
})

// I don't understand these next two snippets:
public class Progress {

    //primary key- progressID column
    @PrimaryKey(autoGenerate = true)
    private int progressID;

    //foreign key- goalID column
    @ColumnInfo(name = "goalID")
    private int goalID;

    //foreign key- type column
    private Goal.Type reportType;

    //foreign key- frequency column
    private Goal.Frequency reportFreq;

    //report date column
    private java.util.Date reportDate;

    //foreign key- end date column)
    @ColumnInfo(name = "end_date")
    private java.util.Date endDate;

    //constructor
    public Progress(int goalID, Goal.Type reportType, Goal.Frequency reportFreq, java.util.Date reportDate, java.util.Date endDate) {
        this.goalID = goalID;
        this.reportType = reportType;
        this.reportFreq = reportFreq;
        this.reportDate = reportDate;
        this.endDate = endDate;
    }

    /****************** Getters and Setters *********************/

    public int getGoalID() {
        return goalID;
    }

    public void setGoalID(int goalID) {
        this.goalID = goalID;
    }

    public int getProgressID() {
        return progressID;
    }

    public void setProgressID(int progressID) {
        this.progressID = progressID;
    }

    public Goal.Type getType() {
        return reportType;
    }

    public void setType(Goal.Type reportType) {
        this.reportType = reportType;
    }

    public Goal.Frequency getFrequency() {
        return reportFreq;
    }

    public void setFrequency(Goal.Frequency reportFreq) {
        this.reportFreq = reportFreq;
    }

    public java.util.Date getEnd() {
        return endDate;
    }

    public void setEnd(java.util.Date endDate) {
        this.endDate = endDate;
    }

    public java.util.Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(java.util.Date reportDate) {
        this.reportDate = reportDate;
    }
}

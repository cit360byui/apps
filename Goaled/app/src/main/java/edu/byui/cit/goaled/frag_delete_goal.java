package edu.byui.cit.goaled;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import edu.byui.cit.widget.CITFragment;

public class frag_delete_goal extends CITFragment {
    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstState) {
        return inflater.inflate(R.layout.delete_goal, container, false);
    }

    @Override
    protected String getTitle() {
        return "Delete Goal";
    }
}

package edu.byui.cit.languageDictionary;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class MainFrag extends Fragment {
	private ImageButton searchButton, addButton;
	private MainActivity act;
	private DatabaseReference mDatabase, cDatabase;
	private Spinner langSpinner;


	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
			ViewGroup container, Bundle savedInstState) {
		View view = null;
		try {
			mDatabase = FirebaseDatabase.getInstance().getReference();
			cDatabase = mDatabase.child("Languages");

			super.onCreateView(inflater, container, savedInstState);
			view = inflater.inflate(R.layout.frag_main, container, false);
			act = (MainActivity)getActivity();

			//Declare the spinners for the main page.
			langSpinner = view.findViewById(R.id.spinner1);
			//Spinner defSpinner = view.findViewById(R.id.spinner2);

			//ArrayAdapter<CharSequence> langAdapter = ArrayAdapter
			// .createFromResource(act, R.array.langSelection,
			//        android.R.layout.simple_spinner_item);
			//ArrayAdapter<CharSequence> defAdapter = ArrayAdapter
			// .createFromResource(act, R.array.langSelection,
			//        android.R.layout.simple_spinner_item);

			//Layout when list appears from spinner
			// langAdapter.setDropDownViewResource(android.R.layout
			// .simple_spinner_dropdown_item);
			//defAdapter.setDropDownViewResource(android.R.layout
			// .simple_spinner_dropdown_item);

			//Set adapter view to the spinner in xml
			//langSpinner.setAdapter(langAdapter);
			//defSpinner.setAdapter(defAdapter);

			//Button that navigates to Display_Frag page.
			searchButton = view.findViewById(R.id.searchBtn);
			searchButton.setOnClickListener(new BtnToDisplayFrag());

			//Button that navigates to Add_Word_Frag page.
			addButton = view.findViewById(R.id.addBtn);
			addButton.setOnClickListener(new BtnToAddWord());

			cDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
				@Override
				public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
					try {
						final List<String> languages = new ArrayList();

						for (DataSnapshot areaSnapshot :
								dataSnapshot.getChildren()) {
							String languageName = areaSnapshot.getKey();
							languages.add(languageName);
						}
						ArrayAdapter<String> arrayAdapter = new ArrayAdapter(
								act, android.R.layout.simple_spinner_item,
								languages);
						arrayAdapter.setDropDownViewResource(
								android.R.layout.simple_spinner_dropdown_item);
						langSpinner.setAdapter(arrayAdapter);
					}
					catch (Exception ex) {
						Log.e(MainActivity.TAG, ex.toString());
					}
				}

				@Override
				public void onCancelled(@NonNull DatabaseError error) {
					Log.w("Database Error", "Failed to read value.",
							error.toException());
				}
			});

		}
		catch (Exception ex) {
			Log.e(MainActivity.TAG, ex.toString());
		}
		return view;


	}

	private final class BtnToDisplayFrag implements View.OnClickListener {
		@Override
		public void onClick(View view) {
			try {
				Fragment frag = new Display_Frag();
				Bundle bundle = new Bundle();
				String lang = langSpinner.getSelectedItem().toString();
				bundle.putString("chosenLanguage", lang);
				frag.setArguments(bundle);
				FragmentTransaction trans =
						act.getSupportFragmentManager().beginTransaction();
				trans.replace(R.id.frag_container, frag);
				trans.addToBackStack(null);
				trans.commit();
			}
			catch (Exception ex) {
				Log.e(MainActivity.TAG, ex.toString());
			}
		}
	}

	private final class BtnToAddWord implements View.OnClickListener {
		@Override
		public void onClick(View view) {
			try {
				Fragment frag = new Add_Word_Frag();
				Bundle bundle = new Bundle();
				String lang = langSpinner.getSelectedItem().toString();
				bundle.putString("chosenLanguage", lang);
				frag.setArguments(bundle);
				FragmentTransaction trans =
						act.getSupportFragmentManager().beginTransaction();
				trans.replace(R.id.frag_container, frag);
				trans.addToBackStack(null);
				trans.commit();
			}
			catch (Exception ex) {
				Log.e(MainActivity.TAG, ex.toString());
			}
		}
	}
}

package edu.byui.cit.languageDictionary;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;


public class MainActivity extends AppCompatActivity {
	public static final String TAG = "LangDict";
	private Fragment fragMain;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			// Create the main fragment and place it
			// as the first fragment in this activity.
			fragMain = new MainFrag();
			FragmentTransaction trans =
					getSupportFragmentManager().beginTransaction();
			trans.add(R.id.frag_container, fragMain);
			trans.commit();
		}
        /*
        public class BtnNextPage implements View.OnClickListener {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                Display_Frag fragment = new Display_Frag();
                fm.beginTransaction().replace(R.id.display_frag_container,
                fragment).commit();

            }
        }
        */
	}
}


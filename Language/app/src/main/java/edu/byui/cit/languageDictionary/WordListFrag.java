package edu.byui.cit.languageDictionary;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static edu.byui.cit.languageDictionary.MainActivity.TAG;


public class WordListFrag extends Fragment {
	private RecyclerView recyclerView;
	private RecyclerView.Adapter mAdapter;
	private RecyclerView.LayoutManager layoutManager;
	private DatabaseReference mDatabase, rDatabase;
	private FirebaseDatabase database;

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		View view = null;
		try {
			super.onCreateView(inflater, container, savedInstanceState);
			view = inflater.inflate(R.layout.frag_wordlist, container, false);
			MainActivity act = (MainActivity)getActivity();

			// Initializes Database Link between Firebase and App
			mDatabase = FirebaseDatabase.getInstance().getReference();
			rDatabase = mDatabase.child("ENtoRU");

			recyclerView = view.findViewById(R.id.wordListRecycler);

			recyclerView.setHasFixedSize(true);

			layoutManager = new LinearLayoutManager(act);
			recyclerView.setLayoutManager(layoutManager);

			// Creates mAdapter, might need to be linked to a new Java Project
			// i.e. projAdapter
			// mAdapter = new database(act);
			recyclerView.setAdapter(mAdapter);

			//mDatabase.getValue("ENtoRU");
			initValues();
		}
		catch (Exception ex) {
			Log.e(TAG, ex.toString());
		}
		return view;
	}

	// This should display all words within the database, however the code is
	// still broken
	// It doesn't throw an error but it will not present any information.
	private void initValues() {
		// Read from the database
		rDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
			@Override
			public void onDataChange(DataSnapshot dataSnapshot) {
				try {
					// This method is called once with the initial value.
					String value = dataSnapshot.getValue(String.class);
					// Build the set value line here. I am unable to find a
					// correct combo here.
					//mAdapter.get(value);

				}
				catch (Exception ex) {
					Log.e(TAG, ex.toString());
				}
			}

			@Override
			public void onCancelled(@NonNull DatabaseError databaseError) {
				// Failed to read value
				Log.w(TAG, "Failed to read value.",
						databaseError.toException());
			}
		});
	}
}

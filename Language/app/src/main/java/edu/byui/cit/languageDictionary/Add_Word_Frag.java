package edu.byui.cit.languageDictionary;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class Add_Word_Frag extends Fragment {
	private DatabaseReference mDatabase, cDatabase;
	private MainActivity act;
	private Button addWord;
	private TextView wordToAdd, meaning;

	public View onCreateView(@NonNull LayoutInflater inflater,
			ViewGroup container, Bundle savedInstState) {
		View view = null;
		try {
			String language = "";
			Bundle bundle = getArguments();
			if (bundle != null) {
				language = bundle.getString("chosenLanguage");
			}
			mDatabase = FirebaseDatabase.getInstance().getReference();
			cDatabase = mDatabase.child(language);

			super.onCreateView(inflater, container, savedInstState);
			view = inflater.inflate(R.layout.frag_add_word, container, false);
			act = (MainActivity)getActivity();

			wordToAdd = view.findViewById(R.id.wordBox);
			meaning = view.findViewById(R.id.meaningBox);

			addWord = view.findViewById(R.id.addBtn);
			addWord.setOnClickListener(new BtnAdd());

		}
		catch (Exception ex) {
			Log.e(MainActivity.TAG, ex.toString());
		}
		return view;
	}

	private final class BtnAdd implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			try {
				// Get word and meaning from user.
				String word =
						wordToAdd.getText().toString().trim().toLowerCase();
				String definiton =
						meaning.getText().toString().trim().toLowerCase();


			}
			catch (Exception ex) {

			}
		}

	}
}
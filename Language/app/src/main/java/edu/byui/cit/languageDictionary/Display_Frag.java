package edu.byui.cit.languageDictionary;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Collections;

public class Display_Frag extends Fragment {
    private Spinner langSpinner;
    private Spinner defSpinner;
    private MainActivity act;
    private Button translate;
    private Button loadDictionary;
    private DatabaseReference mDatabase, cDatabase;
    private TextView translated;
    private TextView originalText;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstState) {
        View view = null;
        try {
            String language = "";
            Bundle bundle = getArguments();
            if (bundle != null) {
                language = bundle.getString("chosenLanguage");
            }
            mDatabase = FirebaseDatabase.getInstance().getReference();
            cDatabase = mDatabase.child(language);

            super.onCreateView(inflater, container, savedInstState);
            view = inflater.inflate(R.layout.frag_display, container, false);
            act = (MainActivity) getActivity();

            langSpinner = view.findViewById(R.id.spinnerlang);
            //defSpinner = view.findViewById(R.id.spinnerdef);

            ArrayAdapter<String> langAdapter = new ArrayAdapter(act, android.R.layout.simple_spinner_item, Collections.singletonList(language));
           // ArrayAdapter<CharSequence> defAdapter = ArrayAdapter.createFromResource(act, R.array.langSelection,
           //         android.R.layout.simple_spinner_item);

            //Layout when list appears from spinner
            langAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //defAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            //Set adapter view to the spinner in xml
            langSpinner.setAdapter(langAdapter);
            //defSpinner.setAdapter(defAdapter);

            //Button that navigates to Display_Frag page.
            translate = view.findViewById(R.id.translateBtn);
            translate.setOnClickListener(new BtnTranslate());

            translated = view.findViewById(R.id.translated);
            originalText = view.findViewById(R.id.original);

            //Button to navigate into the Recycler View WordList
            loadDictionary = view.findViewById(R.id.fullDictLoad);
            loadDictionary.setOnClickListener(new btnLoadDict());

        } catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }

        return view;
    }

    private final class BtnTranslate implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            try {
                // Get word from user.
                String orig = originalText.getText().toString().trim().toLowerCase();

                cDatabase.child(orig).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        try {
                            String value = dataSnapshot.getValue(String.class);
                            translated.setText(value);

                        } catch (Exception ex) {
                            Log.e(MainActivity.TAG, ex.toString());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                        Log.w("Database Error", "Failed to read value.", error.toException());
                    }
                });
            } catch (Exception ex) {
                Log.e(MainActivity.TAG, ex.toString());
            }
        }
    }

    private final class btnLoadDict implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            try {
                Fragment frag = new WordListFrag();
                FragmentTransaction trans =
                        act.getSupportFragmentManager().beginTransaction();
                trans.replace(R.id.frag_container, frag);
                trans.addToBackStack(null);
                trans.commit();
            } catch (Exception ex) {
                Log.e(MainActivity.TAG, ex.toString());
            }
        }
    }
}
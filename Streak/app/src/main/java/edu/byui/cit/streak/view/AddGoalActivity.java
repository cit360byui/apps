package edu.byui.cit.streak.view;


import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import c.R;
import edu.byui.cit.streak.model.AppDatabase;
import edu.byui.cit.streak.model.Goal;
import edu.byui.cit.streak.model.GoalDAO;


public class AddGoalActivity extends AppCompatActivity {

	//    private EditText duration_title, start_date, end_dates,
	//    durationDescription;
	private AppDatabase appDatabase;
	private Goal goal;
	private boolean update;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_duration);

// update
//        appDatabase = AppDatabase.getInstance(AddGoalActivity.this);
		Button button = findViewById(R.id.button1);
//        if ( (goal = (Goal) getIntent().getSerializableExtra("goal"))!=null ){
////            getSupportActionBar().setTitle("Update Goal");
//            update = true;
////            button.setText("Update");
//            // ID's are not matching up yet below
//            duration_title.setText(goal.getDurationTitle());
////            quantity_title.setText(goal.getQuantityTitle());
//            start_date.setText(goal.getStartDate());
//            end_date.setText(goal.getEndDate());
//            //goal_id.setText(goal.getGoalId);
//            durationDescription.setText(goal.getDescription());
		button.setOnClickListener(new HandleAddGoalClick());
	}
//        // Add & Update feature


	class HandleAddGoalClick implements View.OnClickListener {
		//            button.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			EditText startDate = findViewById(R.id.start_date);
			EditText endDate = findViewById(R.id.end_date);
			EditText durationDescription2 = findViewById(
					R.id.durationDescription);
			EditText durationTitle = findViewById(R.id.duration_title);

//                if (update){
//                    goal.setDescription(durationDescription.getText()
//                    .toString());
//                    //Fix the errors commented out below
//                    goal.setDurationTitle(duration_title.getText().toString
//                    ());
////                    goal.setQuantityTitle(quantity_title.getText()
// .toString());
//                    goal.setStartDate(start_date.getText().toString());
//                    goal.setEndDate(end_date.getText().toString());
//                    //goal.setGoalId(goal_id.getText().toString());
//                    appDatabase.getGoalDAO().updateGoal(goal);
//                    setResult(goal,2); //What does this line mean
//                }else {

			AppDatabase db = AppDatabase.getInstance(AddGoalActivity.this);
			GoalDAO goalDAO = db.getGoalDAO();


			String title = durationTitle.getText().toString();
			String description = durationDescription2.getText().toString();
			String start = startDate.getText().toString();
			String end = endDate.getText().toString();

			Goal goal = new Goal(title, description, start, end);

			goalDAO.insertGoal(goal);

//                new InsertTask(AddGoalActivity.this, goal).execute();
			//                }
		}
	}
}
//
////    private void setResult(Goal goal, int flag){
//////    ****** maybe apart of an error in ViewAllActivity *****
////        setResult(flag, new Intent().putExtra("goal", (Serializable) goal));
////        finish();
//
//        class InsertTask extends AsyncTask<Void, Void, Boolean> {
//
//            private WeakReference<AddGoalActivity> activityReference;
//            private Goal goal;
//
//            // only retain a weak reference to the activity
//            InsertTask(AddGoalActivity context, Goal goal) {
//                activityReference = new WeakReference<>(context);
//                this.goal = goal;
//            }
//
//                // doInBackground methods runs on a worker thread
//                @Override
//                protected Boolean doInBackground(Void... objs) {
//                    // retrieve auto incremented goal id
//                    long j = activityReference.get().appDatabase.getGoalDAO
//                    ().insertGoal(goal);
//                    goal.setGoalId((int) j);
//                    Log.e("ID ", "doInBackground: "+j );
//                    return true;
//                }
//
//                    // onPostExecute runs on main thread
//                    @Override
//                    protected void onPostExecute(Boolean bool) {
//                        if (bool){
//            //                activityReference.get().setResult(goal,1);
//                            activityReference.get().finish();
//                        }
//                    }
//                }
//        }

// OLD CODE
//public class AddGoalActivity extends Fragment {
//    private View view;
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater,
//                             ViewGroup container, Bundle savedInstState) {
//        view = null;
//        try {
//            super.onCreateView(inflater, container, savedInstState);
//            view = inflater.inflate(R.layout.add_goal, container, false);
//
//            //update, start
//            Button update = view.findViewById(R.id.updateButton);
//            update.setOnClickListener(new HandleAddProjectClick());
//
//            Button start = view.findViewById(R.id.start);
//            start.setOnClickListener(new HandleAddProjectClick());
//
//            // The delete is here in case we add a delete button
//            //            Button delete = view.findViewById(R.id
//            .deleteButton);
//            //            delete.setOnClickListener(new
//            HandleCancelProjectClick());
//
//        } catch (Exception ex) {
//            Log.e(MainActivity.TAG, ex.toString());
//        }
//        return view;
//    }
//
//    public final class HandleAddProjectClick implements View.OnClickListener {
//
//        @Override
//        public void onClick(View v) {
//          Toast.makeText(act, "That was clicked", Toast.LENGTH_LONG).show();
//
//            AppCompatActivity act = (AppCompatActivity) getActivity();
//            //act.onBackPressed();
//
//            Context appCtx = act.getApplicationContext();
//            AppDatabase db = AppDatabase.getInstance(appCtx);
//            GoalDAO gDAO = db.getGoalDAO();
//
//            EditText pName = view.findViewById(R.id.title);
//            EditText pDescription = view.findViewById(R.id.card_description);
//
//
//            String goalName = pName.getText().toString();
//            String goalDesc = pDescription.getText().toString();
//
//
//            Goal goal1 = new Goal(goalName, goalDesc);
//            gDAO.insert(goal1);
//
//            // Toast.makeText(act, pDAO.getAll().toString(), Toast
//            .LENGTH_LONG).show();
//
//
//            act.onBackPressed();
//        }
//    }
//
//    public final class HandleCancelProjectClick implements View
//    .OnClickListener {
//
//        @Override
//        public void onClick(View v) {
////            Toast.makeText(act, "That was clicked", Toast.LENGTH_LONG)
// .show();
//
//            AppCompatActivity act = (AppCompatActivity) getActivity();
//            act.onBackPressed();
//
//
//        }
//    }
//}
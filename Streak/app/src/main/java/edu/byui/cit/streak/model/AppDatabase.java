package edu.byui.cit.streak.model;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import android.content.Context;


//@Database(entities = { Goal.class }, version = 1, exportSchema = false)
//@TypeConverters({ Converters.class })
//public abstract class AppDatabase extends RoomDatabase {
//    private static AppDatabase StreakDB;
//
//    public static AppDatabase getInstance(Context appCtx) {
//        if (StreakDB == null) {
//            StreakDB = Room.databaseBuilder(
//                    appCtx, AppDatabase.class, "Streak")
//                    .allowMainThreadQueries()
//                    .fallbackToDestructiveMigration()
//                    .build();
//        }
//        return StreakDB;
//    }
//
//    public abstract GoalDAO getGoalDAO();
//
//    public  void cleanUp(){
//        StreakDB = null;
//    }
//
//}


@Database(entities = { Goal.class }, version = 1, exportSchema = false)
@TypeConverters({ DateRoomConverter.class })
public abstract class AppDatabase extends RoomDatabase {

	public abstract GoalDAO getGoalDAO();


	private static AppDatabase streakDB;

	// synchronized is use to avoid concurrent access in multithred environment
	public static /*synchronized*/ AppDatabase getInstance(Context context) {
		if (null == streakDB) {
			streakDB = buildDatabaseInstance(context);
		}
		return streakDB;
	}

	private static AppDatabase buildDatabaseInstance(Context context) {
		return Room.databaseBuilder(context,
				AppDatabase.class,
				Constants.DB_NAME).allowMainThreadQueries().build();
	}

	public void cleanUp() {
		streakDB = null;
	}
}
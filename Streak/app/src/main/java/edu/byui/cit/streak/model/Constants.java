package edu.byui.cit.streak.model;

final public class Constants {
	private Constants() {
	}

	public static final String TABLE_NAME_GOAL = "goal";
	public static final String DB_NAME = "goaldb.db";
}

package edu.byui.cit.streak.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;


//@Dao
//public abstract class GoalDAO {
//    @Query("SELECT COUNT(*) FROM Goal")
//    public abstract long count();
//
//    @Query("SELECT * FROM Goal")
//    public abstract List<Goal> getAll();
//
//    @Query("SELECT * FROM Goal WHERE `key` = :key")
//    public abstract Goal getByKey(long key);
//
//    public long insert(Goal goal) {
//        long id = insertGoal(goal);
//        goal.setKey(id);
//        return id;
//    }
//
//    @Insert
//    public abstract long insertGoal(Goal goal);
//
//    @Update
//    public abstract void updateGoal(Goal goal);
//
//    @Delete
//    public abstract void delete(Goal goal);
//
//    @Query("DELETE FROM Goal")
//    public abstract void deleteAll();
//}

@Dao
public interface GoalDAO {

	//    @Query("SELECT * FROM "+ Constants.TABLE_NAME_GOAL)
	@Query("SELECT * FROM Goal")
	List<Goal> getGoals();

	/*
	 * Insert the object in database
	 * @param Goal, object to be inserted
	 */
	@Insert
	long insertGoal(Goal goal);

	/*
	 * update the object in database
	 * @param Goal, object to be updated
	 */
	@Update
	void updateGoal(Goal repos);

	/*
	 * delete the object from database
	 * @param Goal, object to be deleted
	 */
	@Delete
	void deleteGoal(Goal goal);

	// Goal... is varargs, here Goal is an array
	/*
	 * delete list of objects from database
	 * @param Goal, array of oject to be deleted
	 */
//    @Delete
//    void deleteGoals(Goal... goal);

}

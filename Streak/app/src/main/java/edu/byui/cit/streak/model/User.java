//package edu.byui.cit.streak.model;
//
//import android.arch.persistence.room.Entity;
//import android.arch.persistence.room.Ignore;
//import android.arch.persistence.room.PrimaryKey;
//
//import java.util.ArrayList;
//import java.util.Objects;
//
//@Entity
//public class User {
//    @PrimaryKey(autoGenerate = true)
//    private static long key;
//
//    private String name;
//    private int totalStars;
//
//    private ArrayList Goals;
//
//    User(long key) {
//        User.key = key;
//    }
//    @Ignore
//    public User(int key) {
//        this.key = key;
//    }
//    @Ignore
//    public User(long key, String name, Integer totalStars) {
//        User.key = key;
//        this.name = name;
//        this.totalStars = totalStars;
//    }
//
//    public long getKey() {
//        return key;
//    }
//
//
//    void setKey(long key) {
//        this.key = key;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public int getTotalStars() {
//        return totalStars;
//    }
//
//
//    public void setToatalStars(Integer totalStars) {
//        this.totalStars = totalStars;
//    }
//
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Goal goal = (Goal) o;
//        return key == goal.getKey() &&
//                Objects.equals(name, goal.getName()) &&
//                Objects.equals(totalStars, goal.getTotalStars());
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(key, name, totalStars);
//    }
//
//}
//

package edu.byui.cit.streak.model;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.Objects;


@Entity
public class Goal implements Serializable {

	@PrimaryKey(autoGenerate = true)
	private long key;

	//    private String quantityTitle;
	private String durationTitle;
	private String description;
	private String startDate;
	private String endDate;
	private long goalId;


	Goal() {
	}

	@Ignore
	public Goal(int key) {
		this.key = key;
	}

	@Ignore
	public Goal(String durationTitle, String description, String startDate,
			String endDate) {
//        this.quantityTitle = quantityTitle;
		this.durationTitle = durationTitle;
		this.description = description;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public long getKey() {
		return key;
	}

	void setKey(long key) {
		this.key = key;
	}

	//    public String getQuantityTitle() { return (String) quantityTitle; }
//
//    public void setQuantityTitle(String quantityTitle) {this.quantityTitle
//    = quantityTitle;
//    }
	public String getDurationTitle() {
		return durationTitle;
	}

	public void setDurationTitle(String durationTitle) {
		this.durationTitle = durationTitle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

//    public int getTotalStars() {
//        return totalStars;
//    }
//
//    public void setTotalStars(int totalStars) {
//        this.totalStars = totalStars;
//    }

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	public long getGoalId() {
		return goalId;
	}

	public void setGoalId(long goalId) {
		this.goalId = goalId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Goal goal = (Goal)o;
		return key == goal.key &&
				Objects.equals(durationTitle, goal.durationTitle) &&
//                Objects.equals(quantityTitle, goal.quantityTitle)&&
				Objects.equals(description, goal.description) &&
//                objects.equals(goalId, goal.goalId)&&
				Objects.equals(startDate, goal.startDate) &&
				Objects.equals(endDate, goal.endDate);
	}


	@Override
	public int hashCode() {
		return Objects.hash(key, durationTitle, description, startDate,
				endDate);
	}

	public String toString() {
		return " " + this.durationTitle + " " + " " + this.description + " " + this.key;
	}

}

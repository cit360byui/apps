package edu.byui.cit.streak.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
// maybe used later in the file with the part about the tool bar see below
import android.view.View;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import c.R;
import edu.byui.cit.streak.model.AppDatabase;
import edu.byui.cit.streak.model.Goal;
// in the same package


public class ViewAllActivity extends AppCompatActivity
		implements GoalAdapter.onGoalItemClick {

	private TextView textViewMsg;
	private RecyclerView recyclerView;
	private AppDatabase goalDatabase;
	private List<Goal> goals;
	private GoalAdapter goalAdapter;
	private int pos;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_all_activity);
		initializeVies();
		displayList();
	}

	private void displayList() {
		goalDatabase = AppDatabase.getInstance(ViewAllActivity.this);
		new RetrieveTask(this).execute();
	}

	private static class RetrieveTask
			extends AsyncTask<Void, Void, List<Goal>> {

		private WeakReference<ViewAllActivity> activityReference;

		// only retain a weak reference to the activity
		RetrieveTask(ViewAllActivity context) {
			activityReference = new WeakReference<>(context);
		}

		@Override
		protected List<Goal> doInBackground(Void... voids) {
			if (activityReference.get() != null) {
				return activityReference.get().goalDatabase.getGoalDAO().getGoals();
			}
			else {
				return null;
			}
		}

		@Override
		protected void onPostExecute(List<Goal> goals) {
			if (goals != null && goals.size() > 0) {
				activityReference.get().goals.clear();
				activityReference.get().goals.addAll(goals);
				// hides empty text view
//                activityReference.get().textViewMsg.setVisibility(View.GONE);
//                activityReference.get().goalAdapter.notifyDataSetChanged();
			}
		}
	}

	private void initializeVies() {
		//Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar); // this is
		// the tool bar that I mentioned in the previous comment in the import
		//setSupportActionBar(toolbar);
		//textViewMsg = (TextView) findViewById(R.id.tv__empty);


		FloatingActionButton fab = findViewById(R.id.fab);
		fab.setOnClickListener(listener);

		//
		recyclerView = findViewById(R.id.recycler_view);
		recyclerView.setLayoutManager(
				new LinearLayoutManager(ViewAllActivity.this));

		goals = new ArrayList<>();
		goalAdapter = new GoalAdapter(goals, ViewAllActivity.this);
		recyclerView.setAdapter(goalAdapter);
	}

	private View.OnClickListener listener = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			startActivityForResult(
					new Intent(ViewAllActivity.this, AddGoalActivity.class),
					100);
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 100 && resultCode > 0) {
			if (resultCode == 1) {
				goals.add((Goal)data.getSerializableExtra("goal"));
			}
			else if (resultCode == 2) {
				goals.set(pos, (Goal)data.getSerializableExtra("goal"));
			}
			listVisibility();
		}
	}

	@Override
	public void onGoalClick(final int pos) {
		new AlertDialog.Builder(ViewAllActivity.this)
				.setTitle("Select Options")
				.setItems(new String[]{ "Delete", "Update" },
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialogInterface,
									int i) {
								switch (i) {
									case 0:
										goalDatabase.getGoalDAO().deleteGoal(
												goals.get(pos));
										goals.remove(pos);
										listVisibility();
										break;
									case 1:
//                                Call update frag here!

										break;
								}
							}
						}).show();
	}

	private void listVisibility() {
		int emptyMsgVisibility = View.GONE;
		if (goals.size() == 0) { // no item to display
			if (textViewMsg.getVisibility() == View.GONE) {
				emptyMsgVisibility = View.VISIBLE;
			}
		}
		textViewMsg.setVisibility(emptyMsgVisibility);
		goalAdapter.notifyDataSetChanged();
	}

	@Override
	protected void onDestroy() {
		goalDatabase.cleanUp();
		super.onDestroy();
	}
}


// OLD CODE
//package edu.byui.cit.streak.view;
//
//
//
//import android.support.annotation.NonNull;
//import android.support.design.widget.FloatingActionButton;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentTransaction;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.CardView;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.ViewGroup;
//import android.util.Log;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.Toast;
//
//import c.R;
//
//
//public class MainFrag extends Fragment {
//
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater,
//                             ViewGroup container, Bundle savedInstState) {
//        View view = null;
//        try {
//            super.onCreateView(inflater, container, savedInstState);
//            view = inflater.inflate(R.layout.frag_main, container, false);
//
//            // waiting for alex to do his work
//            MainActivity act = (MainActivity)getActivity();
//            RecyclerView rec = view.findViewById(R.id.recycler_view);
//            rec.setLayoutManager(new LinearLayoutManager(act));
//            rec.setAdapter(new GoalAdapter(act));
//
//
//            FloatingActionButton fab = view.findViewById(R.id.fab);
//            fab.setOnClickListener(new HandleAddGoalClick());
//
//        } catch (Exception ex) {
//            Log.e(MainActivity.TAG, ex.toString());
//        }
//        return view;
//    }
//
//    public final class HandleAddGoalClick implements View.OnClickListener{
//
//        @Override
//        public void onClick(View view) {
//            AppCompatActivity act = (AppCompatActivity) getActivity();
//            Toast.makeText(act, "The Add FAB was clicked", Toast
//            .LENGTH_LONG).show();
//            AddGoalFrag fragAdd = new AddGoalFrag();
//            FragmentTransaction trans =
//                    act.getSupportFragmentManager().beginTransaction();
//            trans.replace(R.id.fragContainer, fragAdd);
//            trans.addToBackStack(null);
//            trans.commit();
//        }
//    }
//}

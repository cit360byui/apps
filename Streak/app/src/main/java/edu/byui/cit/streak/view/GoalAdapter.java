package edu.byui.cit.streak.view;
//import android.content.Context;
//import android.support.annotation.NonNull;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.View.OnLongClickListener;
//import android.view.ViewGroup;
//import android.widget.PopupMenu;
//import android.widget.PopupMenu.OnMenuItemClickListener;
//import android.widget.TextView;
//import java.text.DateFormat;
//import java.util.List;
//
//import c.R;
//import edu.byui.cit.streak.model.AppDatabase;
//import edu.byui.cit.streak.model.Goal;
//import edu.byui.cit.streak.model.GoalDAO;
//
//// This allows us to work with the items in the recycler view
//public final class GoalAdapter
//        extends RecyclerView.Adapter<GoalAdapter.ViewHolder> {
//
//    private final MainActivity activity;
//    private final GoalDAO goalDao;
//    private final List<Goal> dataSet;
//    private final DateFormat fmtrStart;
//
//     GoalAdapter(MainActivity activity) {
//        setHasStableIds(true);
//        this.activity = activity;
//        Context appCtx = activity.getApplicationContext();
//        AppDatabase db = AppDatabase.getInstance(appCtx);
//        goalDao = db.getGoalDAO();
//        dataSet = goalDao.getAll();
//        fmtrStart = DateFormat.getDateInstance(DateFormat.SHORT);
//    }
//
//    @Override
//    public int getItemCount() {
//        return dataSet.size();
//    }
//
//    @Override
//    public long getItemId(int pos) {
//
//        return dataSet.get(pos).getKey();
//    }
//
//    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.frag_card, parent, false);
//        return new ViewHolder(view);
//    }
//
//
//    @Override
//    public void onBindViewHolder(@NonNull ViewHolder holder, int pos) {
//        try {
//            holder.bind(pos);
//        }
//        catch (Exception ex) {
//            Log.e(MainActivity.TAG, ex.toString());
//        }
//    }
//
//    final class ViewHolder extends RecyclerView.ViewHolder
//            implements OnClickListener, OnLongClickListener,
//            OnMenuItemClickListener {
//
//        private final TextView /*txtStart,*/txtTitle, txtDescription;
//
//        private Goal goal;
//
//        ViewHolder(View view) {
//            super(view);
//
//            // These are connected to Frag_card
//            txtTitle= view.findViewById(R.id.card_title);
//            txtDescription = view.findViewById(R.id.card_description);
//
//            // We need to add controls to the progress bar this maybe apart
//            of that solution
//            //txtStart = view.findViewById(R.id.textView3);
//
//            view.setOnClickListener(this);
//            view.setOnLongClickListener(this);
//        }
//
//        void bind(int pos) {
//            goal = dataSet.get(pos);
//            txtTitle.setText(goal.getName());
//            txtDescription.setText(goal.getDescription());
//            // maybe where we bind the progress(e.g. days completed) from
//            the database
//        }
//
//        @Override
//        public boolean onLongClick(View view) {
//            try {
//                PopupMenu menu = new PopupMenu(activity, view);
//                menu.getMenuInflater().inflate(R.menu.popup, menu.getMenu()
//                ); // this will also connect into the cardview as well
//                menu.setOnMenuItemClickListener(this);
//                menu.show();
//            }
//            catch (Exception ex) {
//                Log.e(MainActivity.TAG, ex.toString());
//            }
//            return true;
//        }
//
//        @Override
//        public boolean onMenuItemClick(MenuItem item) {
//            boolean handled = false;
//            try {
//                switch (item.getItemId()) {
//                    case R.id.itmDelete: // this will connect to the
//                    cardview/ main screen
//
//                        goalDao.delete(goal);
//                        int index = dataSet.indexOf(goal);
//                        dataSet.remove(index);
//
//                        notifyItemRemoved(index);
//                        handled = true;
//                        break;
//                }
//            }
//            catch (Exception ex) {
//                Log.e(MainActivity.TAG, ex.toString());
//            }
//            return handled;
//        }
//
//        @Override
//        public void onClick(View v) {
//
//        }
//    }
//}


import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import c.R;
import edu.byui.cit.streak.model.Goal;


public class GoalAdapter extends RecyclerView.Adapter<GoalAdapter.ViewHolder> {

	private final List<Goal> dataSet;
	private Context context;
	private LayoutInflater layoutInflater;
	private onGoalItemClick onGoalItemClick;

	public GoalAdapter(List<Goal> dataSet, Context context) {
		layoutInflater = LayoutInflater.from(context);
		this.dataSet = dataSet;
		this.context = context;
		this.onGoalItemClick = (onGoalItemClick)context;
	}


	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = layoutInflater.inflate(R.layout.card_list_item, parent,
				false);
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		Log.e("bind", "onBindViewHolder: " + dataSet.get(position));
//        holder.textViewTitle.setText(dataSet.get(position).getQuantityTitle
//        ());
		holder.textViewTitle.setText(dataSet.get(position).getDurationTitle());
		holder.textViewContent.setText(dataSet.get(position).getDescription());
		holder.textViewContent.setText(dataSet.get(position).getStartDate());
		holder.textViewContent.setText(dataSet.get(position).getEndDate());
//        holder.textViewContent.setText(goalId.get(position.getGoalId));
	}

	@Override
	public int getItemCount() {
		return dataSet.size();
	}

	public class ViewHolder extends RecyclerView.ViewHolder
			implements View.OnClickListener {

		TextView textViewTitle;
		TextView textViewContent;

		public ViewHolder(View itemView) {
			super(itemView);
			itemView.setOnClickListener(this);
			textViewTitle = itemView.findViewById(R.id.card_title);
			textViewContent = itemView.findViewById(R.id.card_description);

		}

		@Override
		public void onClick(View view) {
			onGoalItemClick.onGoalClick(getAdapterPosition());
		}
	}

	public interface onGoalItemClick {
		void onGoalClick(int pos);
	}
}

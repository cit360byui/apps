package edu.byui.cit.parking.view;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import edu.byui.cit.parking.R;


public class LicenseInputFrag extends Fragment {

	private TextView txtLicenseHead;
	private EditText txtLicense;
	private Button getInput;

	private DatabaseReference mDatabase;

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {

		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.frag_input_license, container,
				false);

		txtLicenseHead = view.findViewById(R.id.txtLicenseHead);
		txtLicense = view.findViewById(R.id.txtLicense);

		Button btnLicenseSubmit = view.findViewById(R.id.getInput);
		btnLicenseSubmit.setOnClickListener(new HandleSendLicense());

		return view;
	}

	private final class HandleSendLicense implements View.OnClickListener {
		@Override
		public void onClick(View view) {

			//figures which part of the database to look for the license plate

			mDatabase = FirebaseDatabase.getInstance().getReference();

			Date currentTime = new Date();
			Date startWinter2019 = new GregorianCalendar(2019,
					Calendar.JANUARY,
					7).getTime();
			Date endWinter2019 = new GregorianCalendar(2019, Calendar.APRIL,
					12).getTime();
			Date startSpring2019 = new GregorianCalendar(2019, Calendar.APRIL,
					22).getTime();
			Date endSpring2019 = new GregorianCalendar(2019, Calendar.JULY,
					23).getTime();
			Date startFall2019 = new GregorianCalendar(2019,
					Calendar.SEPTEMBER,
					16).getTime();
			Date endFall2019 = new GregorianCalendar(2019, Calendar.DECEMBER,
					18).getTime();

			if ((currentTime.after(startWinter2019)) & (currentTime.before(
					endWinter2019))) {
				//searches through Winter2019
			}

			if ((currentTime.after(startSpring2019)) & (currentTime.before(
					endSpring2019))) {
				//searches through Spring2019
			}

			if ((currentTime.after(startFall2019)) & (currentTime.before(
					endFall2019))) {
				//searches through Fall2019
			}

		}
	}


}



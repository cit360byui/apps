package edu.byui.cit.parking.model;

import java.util.Objects;


public class Lot {

	private String name;
	private String color;

	public Lot() {
		this.name = null;
		this.color = null;
	}

	public Lot(String name, String color) {
		this.name = name;
		this.color = color;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Lot)) {
			return false;
		}
		Lot lot = (Lot)o;
		return Objects.equals(getName(), lot.getName()) &&
				Objects.equals(getColor(), lot.getColor());
	}

	@Override
	public String toString() {
		return "Lot{" +
				"name='" + name + '\'' +
				", color='" + color + '\'' +
				'}';
	}
}

package edu.byui.cit.parking.view;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;


import edu.byui.cit.parking.R;


public class MainFrag extends Fragment {
	//    private RecyclerView recyclerView;
//    private RecyclerView.Adapter mAdapter;
//    private RecyclerView.LayoutManager layoutManager;
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
			ViewGroup container, Bundle savedInstState) {
		View view = null;
		try {
			super.onCreateView(inflater, container, savedInstState);
			view = inflater.inflate(R.layout.new_menu_template, container,
					false);

			// Find the floating action button and add a listener to it.
			Button EnterLicensePlate = view.findViewById(
					R.id.enterLicensePlate);
			EnterLicensePlate.setOnClickListener(new HandleEnterLicensePlate());

			Button OpenCamera = view.findViewById(R.id.openCamera);
			OpenCamera.setOnClickListener(new HandleOpenCamera());

//            ImageButton OpenCameraImageButton = view.findViewById(R.id
//            .openCameraImageButton);
//            OpenCameraImageButton.setOnClickListener(new HandleOpenCamera());

			Button OpenParkingMap = view.findViewById(R.id.parkinglotMap);
			OpenParkingMap.setOnClickListener(new HandleParkingMap());


		}
		catch (Exception ex) {
			Log.e(MainActivity.TAG, ex.toString());
		}
		return view;
	}

	private class HandleEnterLicensePlate implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			//Toast.makeText(getActivity(), "This is my toast message!", Toast
			// .LENGTH_SHORT).show();
			try {
				//Open the app license input frag
				Fragment frag = new LicenseInputFrag();
				MainActivity act = (MainActivity)getActivity();
				FragmentTransaction trans =
						act.getSupportFragmentManager().beginTransaction();
				trans.replace(R.id.fragContainer, frag);
				trans.addToBackStack(null);
				trans.commit();
			}
			catch (Exception ex) {
				Log.e(MainActivity.TAG, ex.toString());
			}
		}
	}


	private class HandleParkingMap implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			//Toast.makeText(getActivity(), "This is my toast message!", Toast
			// .LENGTH_SHORT).show();
			try {
				//Open the app license input frag
				Fragment frag = new ParkingLotMapFrag();
				MainActivity act = (MainActivity)getActivity();
				FragmentTransaction trans =
						act.getSupportFragmentManager().beginTransaction();
				trans.replace(R.id.fragContainer, frag);
				trans.addToBackStack(null);
				trans.commit();
			}
			catch (Exception ex) {
				Log.e(MainActivity.TAG, ex.toString());
			}
		}
	}

	private class HandleOpenCamera implements View.OnClickListener {
		final int REQUEST_IMAGE_CAPTURE = 1;

		@Override
		public void onClick(View v) {
			try {
				Intent takePictureIntent = new Intent(
						MediaStore.ACTION_IMAGE_CAPTURE);
				if (takePictureIntent.resolveActivity(
						getActivity().getPackageManager()) != null) {
					startActivityForResult(takePictureIntent,
							REQUEST_IMAGE_CAPTURE);
				}

			}
			catch (Exception ex) {
				//Toast.makeText(getActivity(), "This is my toast message!",
				// Toast.LENGTH_SHORT).show();
				ActivityCompat.requestPermissions(getActivity(),
						new String[]{ Manifest.permission.CAMERA },
						REQUEST_IMAGE_CAPTURE);

				Log.e(MainActivity.TAG, ex.toString());
			}
		}
	}
}

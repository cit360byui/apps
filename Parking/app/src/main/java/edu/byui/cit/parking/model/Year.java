package edu.byui.cit.parking.model;

import java.util.Objects;


public class Year {

	private String semesterYear;

	public Year() {
		this.semesterYear = null;
	}

	public Year(String semesterYear) {
		this.semesterYear = semesterYear;
	}

	public String getSemesterYear() {
		return semesterYear;
	}

	public void setSemesterYear(String semesterYear) {
		this.semesterYear = semesterYear;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Year)) {
			return false;
		}
		Year year = (Year)o;
		return Objects.equals(getSemesterYear(), year.getSemesterYear());
	}

	@Override
	public String toString() {
		return "Year{" +
				"semesterYear='" + semesterYear + '\'' +
				'}';
	}
}

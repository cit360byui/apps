package edu.byui.cit.parking.model;

import java.util.Objects;


public class Car {
	private String license_plate;
	private String color;
	private String make;
	private String model;
	private String state;

	public Car() {
		this.license_plate = null;
		this.color = null;
		this.make = null;
		this.model = null;
		this.state = null;
	}

	public Car(String license_plate, String color, String make, String model,
			String state) {
		this.license_plate = license_plate;
		this.color = color;
		this.make = make;
		this.model = model;
		this.state = state;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Car)) {
			return false;
		}
		Car car = (Car)o;
		return Objects.equals(license_plate, car.license_plate) &&
				Objects.equals(color, car.color) &&
				Objects.equals(make, car.make) &&
				Objects.equals(model, car.model) &&
				Objects.equals(state, car.state);
	}

	@Override
	public String toString() {
		return "Car{" +
				"license_plate='" + license_plate + '\'' +
				", color='" + color + '\'' +
				", make='" + make + '\'' +
				", model='" + model + '\'' +
				", state='" + state + '\'' +
				'}';
	}
}

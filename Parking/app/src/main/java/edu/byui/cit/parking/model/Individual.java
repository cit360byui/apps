package edu.byui.cit.parking.model;

import java.util.Objects;


public class Individual {
	private String byui_id;
	private String first_name;
	private String middle_name;
	private String last_name;
	private String email;
	private String type;
	private String phone_number;

	public Individual() {
		this.byui_id = null;
		this.first_name = null;
		this.middle_name = null;
		this.last_name = null;
		this.email = null;
		this.type = null;
		this.phone_number = null;
	}

	public Individual(String byui_id, String first_name, String middle_name,
			String last_name, String email, String type, String phone_number) {
		this.byui_id = byui_id;
		this.first_name = first_name;
		this.middle_name = middle_name;
		this.last_name = last_name;
		this.email = email;
		this.type = type;
		this.phone_number = phone_number;
	}

	public String getByui_id() {
		return byui_id;
	}

	public void setByui_id(String byui_id) {
		this.byui_id = byui_id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getMiddle_name() {
		return middle_name;
	}

	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Individual)) {
			return false;
		}
		Individual that = (Individual)o;
		return Objects.equals(getByui_id(), that.getByui_id()) &&
				Objects.equals(getFirst_name(), that.getFirst_name()) &&
				Objects.equals(getMiddle_name(), that.getMiddle_name()) &&
				Objects.equals(getLast_name(), that.getLast_name()) &&
				Objects.equals(getEmail(), that.getEmail()) &&
				Objects.equals(getType(), that.getType()) &&
				Objects.equals(getPhone_number(), that.getPhone_number());
	}

	@Override
	public String toString() {
		return "Individual{" +
				"byui_id='" + byui_id + '\'' +
				", first_name='" + first_name + '\'' +
				", middle_name='" + middle_name + '\'' +
				", last_name='" + last_name + '\'' +
				", email='" + email + '\'' +
				", type='" + type + '\'' +
				", phone_number='" + phone_number + '\'' +
				'}';
	}
}

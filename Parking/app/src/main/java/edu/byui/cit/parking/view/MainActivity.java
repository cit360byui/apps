package edu.byui.cit.parking.view;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import edu.byui.cit.parking.R;


public class MainActivity extends AppCompatActivity {
	public static final String TAG = "Parking";
	private Fragment fragLicense;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			//fragLicense = new LicenseInputFrag();
			fragLicense = new MainFrag();
			FragmentTransaction trans =
					getSupportFragmentManager().beginTransaction();
			trans.add(R.id.fragContainer, fragLicense);
			trans.commit();
		}

	}

}

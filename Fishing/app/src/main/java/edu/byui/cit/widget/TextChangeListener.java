package edu.byui.cit.widget;

public interface TextChangeListener {
	void textChanged(EditWrapper source);
}

package edu.byui.cit.widget;

public interface DateChangeListener {
	void afterChanged(DateWrapper source, int year, int month, int dayOfMonth);
}

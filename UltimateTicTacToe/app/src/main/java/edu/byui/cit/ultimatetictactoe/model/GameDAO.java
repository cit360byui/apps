package edu.byui.cit.ultimatetictactoe.model;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public abstract class GameDAO {
    @Query("SELECT COUNT(*) FROM Game")
    public abstract long count();

    @Query("SELECT * FROM Game")
    public abstract List<Game> getAll();

    @Query("SELECT * FROM Game WHERE `key` = :key")
    public abstract Game getByKey(long key);

    @Query("SELECT * FROM Game LIMIT 1")
    public abstract Game getFirstGame();

    public long insert(Game game) {
        long id = insertH(game);
        game.setKey(id);
        return id;
    }

    @Insert
    abstract long insertH(Game game);

    @Update
    public abstract void update(Game game);

    @Delete
    public abstract void delete(Game game);

    @Query("DELETE FROM Game")
    public abstract void deleteAll();
}

package edu.byui.cit.ultimatetictactoe.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import edu.byui.cit.ultimatetictactoe.R;
import edu.byui.cit.ultimatetictactoe.model.AppDatabase;
import edu.byui.cit.ultimatetictactoe.model.OnlineGame;
import edu.byui.cit.ultimatetictactoe.model.OnlineGameDAO;

/*
    User Verification/Game Creation
    2 options, create new game, or enter friend's game
    Go to 2 different view screens, new game asking for information, then show game information
    Or enter a friend's game asking for game identifier
 */
public class OnlineGamesFrag extends Fragment {
    private RecyclerView.Adapter adapter;
    private View view;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_online_games, container, false);

        // Get the locally-stored list of OnlineGames to pass it to the RecyclerView
        AppDatabase appDatabase = AppDatabase.getInstance(getContext());
        OnlineGameDAO onlineGameDAO = appDatabase.getOnlineGameDAO();
        List<OnlineGame> onlineGameList = onlineGameDAO.getAll();

        // Create the Recycler View and connect it to this Frag
        RecyclerView recyclerView = view.findViewById(R.id.onlineList);
        adapter = new OnlineListAdapter(onlineGameList, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        Button newOnlineBtn = view.findViewById(R.id.btnNewOnline);
        Button joinOnlineBtn = view.findViewById(R.id.btnJoinOnline);

        newOnlineBtn.setOnClickListener(new NewGameHandler());
        joinOnlineBtn.setOnClickListener(new JoinGameHandler());

        return view;
    }

    public void removedGame(OnlineGame onlineGame) {
        AppDatabase appDatabase = AppDatabase.getInstance(getContext());
        OnlineGameDAO onlineGameDAO = appDatabase.getOnlineGameDAO();
        onlineGameDAO.delete(onlineGame);
        Toast.makeText(getContext(), "That game no longer exists!", Toast.LENGTH_LONG).show();
        // Get the locally-stored list of OnlineGames to pass it to the RecyclerView
        List<OnlineGame> onlineGameList = onlineGameDAO.getAll();

        // Create the Recycler View and connect it to this Frag
        RecyclerView recyclerView = view.findViewById(R.id.onlineList);
        adapter = new OnlineListAdapter(onlineGameList, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    /*
        Open frag that asks for game identifier
     */
    final class JoinGameHandler implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            MainActivity.openFrag(new JoinGameFrag(), getFragmentManager());
        }
    }

    /*
        Open a new Frag that prompts for OnlineGame info
     */
    final class NewGameHandler implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            MainActivity.openFrag(new CreateGameFrag(), getFragmentManager());
        }
    }
}

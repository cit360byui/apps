package edu.byui.cit.ultimatetictactoe.presenter;

import edu.byui.cit.ultimatetictactoe.model.AppDatabase;
import edu.byui.cit.ultimatetictactoe.model.Game;
import edu.byui.cit.ultimatetictactoe.model.GameFirebaseDAO;
import edu.byui.cit.ultimatetictactoe.model.OnlineGame;
import edu.byui.cit.ultimatetictactoe.model.OnlineGameDAO;
import edu.byui.cit.ultimatetictactoe.view.GameFrag;

/*
    We're going to use most of GameEngine's methods, but will also create some of our own for online
    communications. Therefore, we will use GameEngine as our parent class.

    Online Game Functionality:
    Create OnlineGameEngine
    Loads game from Firebase
    Listen to changes made in Firebase and update OnlineGameEngine
    Update game in Firebase
    Limit user action to their turn only
    Work like GameEngine
    There is an issue where the game isn't always updating the Firebase db (It might be that
     I was running two emulators at the same time?)
 */
public class OnlineGameEngine  extends GameEngine {
    private GameFirebaseDAO gameFirebaseDAO;
    private OnlineGame onlineGame;
    private GameFrag gameFrag;

    public OnlineGameEngine(Game game, GameFrag gameFrag, OnlineGame onlineGame) {
        super(game, gameFrag);
        this.onlineGame = onlineGame;
        this.gameFrag = gameFrag;
        gameFirebaseDAO = new GameFirebaseDAO(onlineGame.getKey());
        startGameEngine();
    }

    private void startGameEngine() {
        gameFirebaseDAO.listenToGame(this);
    }

    /*
        First, update the game locally
        Then, push the new game state and changed position to Firebase
        Only push if the play was successful
        Only allow the user to play if it's their turn
     */
    public boolean play(int position) {
        if (onlineGame.isX() != getGame().getXTurn()) {
            gameFrag.displayMessage("It's not your turn!");
        } else if(onlineGame.isX() == getGame().getXTurn() && super.play(position)) {
            gameFirebaseDAO.update(getGame(), position);
            return true;
        }

        return false;
    }

    /*
        We only want to update if the change came from someone else.
        However, gameFirebaseDAO.listenToGame() will call updateInterface() on any changes made.
        Therefore, we determine if we were the one who made the change by seeing if the cell
        position has already been changed.
        This works because we make changes locally, then push to Firebase.
        Use the super-class's play method(purely local) so it won't attempt to unnecessarily write
        to Firebase again and create an infinite loop.
     */
    public void updateInterface(int position) {
        // Do bounds checking, then if the current player was the one who did the change
        if (position > 0
                && position < getBoard().size()
                && getBoard().get(position) == ' ') {
            super.play(position);
            gameFrag.updateCellView(position, getBoard().get(position));
        }
    }

    public void stopListening() {
        gameFirebaseDAO.stopListeningToGame();
    }

    public void deleteGame() {
        AppDatabase appDatabase = AppDatabase.getInstance(gameFrag.getContext());
        OnlineGameDAO onlineGameDAO = appDatabase.getOnlineGameDAO();
        onlineGameDAO.delete(onlineGame);
    }
}

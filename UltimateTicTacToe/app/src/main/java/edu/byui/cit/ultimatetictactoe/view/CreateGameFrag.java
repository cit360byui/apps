package edu.byui.cit.ultimatetictactoe.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import edu.byui.cit.ultimatetictactoe.R;
import edu.byui.cit.ultimatetictactoe.model.AppDatabase;
import edu.byui.cit.ultimatetictactoe.model.GameFirebaseDAO;
import edu.byui.cit.ultimatetictactoe.model.OnlineGame;
import edu.byui.cit.ultimatetictactoe.model.OnlineGameDAO;

/*
    1. Link all input views
    2. Fully implement CreateGameHandler
 */
public class CreateGameFrag extends Fragment {
    private EditText gameTitleEdit;
    private TextView createWarningTxt;
    private boolean isX = true;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_create_game, container, false);

        createWarningTxt = view.findViewById(R.id.txtCreateWarning);
        gameTitleEdit = view.findViewById(R.id.editTitleNew);
        Button createOnlineBtn = view.findViewById(R.id.btnCreateOnline);
        RadioGroup playAsGroup = view.findViewById(R.id.rGroupTurn);

        createWarningTxt.setVisibility(View.GONE);
        createOnlineBtn.setOnClickListener(new CreateGameHandler());
        playAsGroup.setOnCheckedChangeListener(new TurnChangedHandler());

        return view;
    }

    /*
        There are only two turn options, so set isX to the other possible choice if the user selects
        a different option.

        Thanks to: https://stackoverflow.com/questions/31929935/android-why-is-the-radiobutton-returning-a-null-value
     */
    class TurnChangedHandler implements RadioGroup.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int id) {
            isX = !isX;
        }
    }

    /*
        1. Get all input views' values
        2. Ensure that all data is correctly filled
        3. Push the new game to Firebase
        4. Store OnlineGame in Room db
        5. Change to GameKeyFrag
     */
    class CreateGameHandler implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            String title = gameTitleEdit.getText().toString();

            if (title.length() > 0) {
                // Create new Game on Firebase and store reference in Room db
                String key = GameFirebaseDAO.insert(isX);
                OnlineGame onlineGame = new OnlineGame(key, title, new Date(), isX);
                AppDatabase appDatabase = AppDatabase.getInstance(getContext());
                OnlineGameDAO onlineGameDAO = appDatabase.getOnlineGameDAO();
                onlineGameDAO.insert(onlineGame);
                // Pass data to frag_game_key.xml
                MainActivity.openFragNoBack(new GameKeyFrag(onlineGame), getFragmentManager());
            } else {
                createWarningTxt.setVisibility(View.VISIBLE);
            }
        }
    }
}

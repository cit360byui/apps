package edu.byui.cit.ultimatetictactoe.model;

/*
    The main reasoning for this class is ease of use when pushing and pulling from Firebase. It
    really shouldn't be used outside of GameFirebaseDAO.
 */
class GameFirebase {
    private String jsonGame;
    private int position;
    private  boolean isCreatorX;

    /*
        IMPORTANT:: Keep this for Firebase to work. Firebase needs a default-constructor to store
        its dataSnapshot into an object.
     */
    GameFirebase() {
        jsonGame = "";
        position = -1;
        isCreatorX = true;
    }

    GameFirebase(String jsonGame, int position, boolean isX) {
        this.jsonGame = jsonGame;
        this.position = position;
        isCreatorX = isX;
    }

    // IMPORTANT:: These getters and setters need to be public for Firebase to work
    public String getJsonGame() {
        return jsonGame;
    }

    public int getPosition() {
        return position;
    }

    public boolean getIsCreatorX() {
        return isCreatorX;
    }

    public void setJsonGame(String jsonGame) {
        this.jsonGame = jsonGame;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setIsCreatorX(boolean isCreatorX) {
        this.isCreatorX = isCreatorX;
    }
}

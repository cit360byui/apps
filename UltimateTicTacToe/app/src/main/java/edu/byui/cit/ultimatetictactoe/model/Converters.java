package edu.byui.cit.ultimatetictactoe.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

import androidx.room.TypeConverter;

class Converters {
    @TypeConverter
    public static List<Character> convertToList(String jsonFormat) {
        Type type = new TypeToken<List<Character>>() {}.getType();

        return new Gson().fromJson(jsonFormat, type);
    }

    @TypeConverter
    public static String convertToJson(List<Character> listFormat) {
        Gson gson = new Gson();
        return gson.toJson(listFormat);
    }

    @TypeConverter
    public static char[] convertToCharArray(String jsonFormat) {
        Type type = new TypeToken<char[]>() {}.getType();

        return new Gson().fromJson(jsonFormat, type);
    }

    @TypeConverter
    public static String convertToJson(char[] charArrayFormat) {
        Gson gson = new Gson();
        return gson.toJson(charArrayFormat);
    }

    @TypeConverter
    public static String convertToJson(Date date) {
        Gson gson = new Gson();
        return gson.toJson(date);
    }

    @TypeConverter
    public static Date convertToDate(String jsonFormat) {
        Type type = new TypeToken<Date>(){}.getType();
        return new Gson().fromJson(jsonFormat, type);
    }
}

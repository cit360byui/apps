package edu.byui.cit.ultimatetictactoe.model;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public abstract class OnlineGameDAO {
    @Query("SELECT COUNT(*) FROM OnlineGame")
    public abstract long count();

    @Query("SELECT * FROM OnlineGame")
    public abstract List<OnlineGame> getAll();

    @Query("SELECT * FROM OnlineGame WHERE `key` = :key")
    public abstract OnlineGame getByKey(String key);

    @Insert
    public abstract long insert(OnlineGame onlineGame);

    @Update
    public abstract void update(OnlineGame onlineGame);

    @Delete
    public abstract void delete(OnlineGame onlineGame);

    @Query("DELETE FROM OnlineGame")
    public abstract void deleteAll();
}

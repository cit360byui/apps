package edu.byui.cit.ultimatetictactoe.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class Game {
    @PrimaryKey(autoGenerate = true)
    private long key;
    private List<Character> board = new ArrayList<>();
    private char[] gridWon;
    private char gameStatus;
    private boolean xTurn;
    private int nextGrid;

    @Ignore
    public Game() {
        for (int i = 0; i < 81; i++) {
            board.add(' ');
        }
        gridWon = new char[9];
        for (int i = 0; i < 9; i++) {
            gridWon[i] = 'F';
        }
        xTurn = true;
        nextGrid = 10;
        gameStatus = 'C';
    }

    @Ignore
    public Game(Game game) {
        this.key = game.getKey();
        this.board = game.getBoard();
        this.gridWon = game.getGridWon();
        this.gameStatus = game.getGameStatus();
        this.xTurn = game.getXTurn();
        this.nextGrid = game.getNextGrid();
    }

    public Game(List<Character> board, char[] gridWon, char gameStatus, boolean xTurn, int nextGrid) {
        this.board = board;
        this.gridWon = gridWon;
        this.gameStatus = gameStatus;
        this.xTurn = xTurn;
        this.nextGrid = nextGrid;
    }

    public long getKey() {
        return key;
    }

    void setKey(long key) {
        this.key = key;
    }

    public List<Character> getBoard() {
        return board;
    }

    public void setBoard(List<Character> board) {
        this.board = board;
    }

    public char[] getGridWon() {
        return gridWon;
    }

    public void setGridWon(char[] gridWon) {
        this.gridWon = gridWon;
    }

    public char getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(char gameStatus) {
        this.gameStatus = gameStatus;
    }

    public boolean getXTurn() {
        return xTurn;
    }

    public void setXTurn(boolean xTurn) {
        this.xTurn = xTurn;
    }

    public int getNextGrid() {
        return nextGrid;
    }

    public void setNextGrid(int nextGrid) {
        this.nextGrid = nextGrid;
    }

    public char getGridWonByPos(int position) {
        return this.gridWon[position];
    }

    public void setGridWonByPos(int position, char value) {
        this.gridWon[position] = value;
    }

    @NonNull
    @Override
    public String toString() {
        // Display the board as a 2-D matrix
        StringBuilder string = new StringBuilder("Board:\n");
        string.append("  A B C D E F G H I\n");
        for (int i = 0; i < board.size(); i++) {
            if (i % 9 == 0) {
                string.append(i / 9).append(" ");
            }
            string.append(board.get(i)).append(" ");
            if (i % 9 == 8) {
                string.append("\n");
            }
        }
        // Display each value in gridWon
        string.append("Grids Won:\n").append("[ ");
        for (char gridPoint: gridWon) {
            string.append(gridPoint).append(" ");
        }
        string.append("]\n");
        // Display the rest of Game's attributes
        string.append("Game Status: ").append(gameStatus).append("\n");
        string.append("X Turn: ").append(xTurn).append("\n");
        string.append("Next Grid: ").append(nextGrid).append("\n");
        return string.toString();
    }

    @Override
    public boolean equals(@Nullable Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        Game otherGame = (Game)other;
        return board.equals(otherGame.board)
                && Arrays.equals(gridWon, otherGame.getGridWon())
                && gameStatus == otherGame.getGameStatus()
                && xTurn == otherGame.getXTurn()
                && nextGrid == otherGame.getNextGrid();
    }
}

package edu.byui.cit.ultimatetictactoe.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

import androidx.fragment.app.FragmentManager;
import edu.byui.cit.ultimatetictactoe.R;
import edu.byui.cit.ultimatetictactoe.model.Game;
import edu.byui.cit.ultimatetictactoe.model.GameDAO;
import edu.byui.cit.ultimatetictactoe.model.AppDatabase;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import edu.byui.cit.ultimatetictactoe.model.GameFirebaseDAO;
import edu.byui.cit.ultimatetictactoe.model.OnlineGame;
import edu.byui.cit.ultimatetictactoe.model.OnlineGameDAO;


/*
    This Fragment displays frag_game.xml and populates the RecyclerView with
    a grid of data.
 */
public class GameFrag extends Fragment {
	private Game game;
	private boolean online;
	private TextView gridHeading;
	private Button playAgainBtn;
	private View view;
	private OnlineGame onlineGame;
	private RecyclerView.Adapter adapter;

	/*
		This should execute when a game is created/opened locally
	 */
	GameFrag(Game game) {
		this.game = game;
		online = false;
		onlineGame = null;
	}

	/*
		This should execute when a game is created/opened online
	 */
	GameFrag(Game game, OnlineGame onlineGame) {
		this.game = game;
		this.onlineGame = onlineGame;
		online = true;
	}


	/*
		This method creates the view for GameFrag. It does this by attaching
		GameFrag to
		frag_game.xml and then creating a RecyclerView using GridAdapter.
	 */
	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
			@Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		// Use RecyclerView to create grid
		view = inflater.inflate(R.layout.frag_game, container, false);
		// Get a reference to the grid heading
		gridHeading = view.findViewById(R.id.gridHeading);
		// Initially hide the play again button
		playAgainBtn = view.findViewById(R.id.playAgainBtn);
		playAgainBtn.setVisibility(View.GONE);
		playAgainBtn.setOnClickListener(new PlayAgainHandler());

		attachGridAdapter();

		return view;
	}

	/*
		Attach a GridAdapter to GameFrag
		Used both onCreateView and PlayAgainHandler
	 */
	private void attachGridAdapter() {
		// Create a 9x9 grid
		int numColumns = 9;
		RecyclerView recyclerView = view.findViewById(R.id.vwGrid);
		if (online) {
			adapter = new GridAdapter(game, this, onlineGame);
		}
		else {
			adapter = new GridAdapter(game, this);
		}
		RecyclerView.LayoutManager layoutManager = new GridLayoutManager(
				getContext(), numColumns);
		recyclerView.setLayoutManager(layoutManager);
		recyclerView.setAdapter(adapter);
	}

	/*
		This onPause() will save the Game's state whenever this Fragment is no
		 longer in the
		forefront.
	 */
	@Override
	public void onPause() {
		super.onPause();
		AppDatabase appDatabase = AppDatabase.getInstance(getContext());
		if (online) {
			// The only thing we need to update is the last played date
			OnlineGameDAO onlineGameDAO = appDatabase.getOnlineGameDAO();
			onlineGame.setDate(new Date());
			onlineGameDAO.update(onlineGame);
		}
		else {
			// Persist the Game's state in the Room Database
			GameDAO gridDAO = appDatabase.getGameDAO();
			gridDAO.update(game);
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		if (online) {
			((GridAdapter)adapter).stopListening();
		}
	}

	/*
			This method finds the correct ImageView based on the grid
			completed and the current player.
			It then makes the selected ImageView visible.
		 */
	public void gridComplete(final int grid, final char player) {
		int id;
		switch (grid) {
			case 0:
				id = getResources().getIdentifier("Iv" + player + "TL", "id",
						getContext().getPackageName());
				break;
			case 1:
				id = getResources().getIdentifier("Iv" + player + "TM", "id",
						getContext().getPackageName());
				break;
			case 2:
				id = getResources().getIdentifier("Iv" + player + "TR", "id",
						getContext().getPackageName());
				break;
			case 3:
				id = getResources().getIdentifier("Iv" + player + "ML", "id",
						getContext().getPackageName());
				break;
			case 4:
				id = getResources().getIdentifier("Iv" + player + "MM", "id",
						getContext().getPackageName());
				break;
			case 5:
				id = getResources().getIdentifier("Iv" + player + "MR", "id",
						getContext().getPackageName());
				break;
			case 6:
				id = getResources().getIdentifier("Iv" + player + "BL", "id",
						getContext().getPackageName());
				break;
			case 7:
				id = getResources().getIdentifier("Iv" + player + "BM", "id",
						getContext().getPackageName());
				break;
			default:
				id = getResources().getIdentifier("Iv" + player + "BR", "id",
						getContext().getPackageName());
				break;
		}
		ImageView imgView = view.findViewById(id);
		imgView.setVisibility(View.VISIBLE);
	}

	public void nextGrid(final int grid) {
		int id;
		switch (grid) {
			case 0:
				id = getResources().getIdentifier("IvBTL", "id",
						getContext().getPackageName());
				break;
			case 1:
				id = getResources().getIdentifier("IvBTM", "id",
						getContext().getPackageName());
				break;
			case 2:
				id = getResources().getIdentifier("IvBTR", "id",
						getContext().getPackageName());
				break;
			case 3:
				id = getResources().getIdentifier("IvBML", "id",
						getContext().getPackageName());
				break;
			case 4:
				id = getResources().getIdentifier("IvBMM", "id",
						getContext().getPackageName());
				break;
			case 5:
				id = getResources().getIdentifier("IvBMR", "id",
						getContext().getPackageName());
				break;
			case 6:
				id = getResources().getIdentifier("IvBBL", "id",
						getContext().getPackageName());
				break;
			case 7:
				id = getResources().getIdentifier("IvBBM", "id",
						getContext().getPackageName());
				break;
			default:
				id = getResources().getIdentifier("IvBBR", "id",
						getContext().getPackageName());
				break;
		}
		ImageView imgView = view.findViewById(id);
		imgView.setVisibility(View.VISIBLE);
	}

	public void clearNextGrid() {
		ImageView imgView;

		imgView = view.findViewById(getResources().getIdentifier("IvBTL", "id",
				getContext().getPackageName()));
		imgView.setVisibility((View.INVISIBLE));
		imgView = view.findViewById(getResources().getIdentifier("IvBTM", "id",
				getContext().getPackageName()));
		imgView.setVisibility((View.INVISIBLE));
		imgView = view.findViewById(getResources().getIdentifier("IvBTR", "id",
				getContext().getPackageName()));
		imgView.setVisibility((View.INVISIBLE));
		imgView = view.findViewById(getResources().getIdentifier("IvBML", "id",
				getContext().getPackageName()));
		imgView.setVisibility((View.INVISIBLE));
		imgView = view.findViewById(getResources().getIdentifier("IvBMM", "id",
				getContext().getPackageName()));
		imgView.setVisibility((View.INVISIBLE));
		imgView = view.findViewById(getResources().getIdentifier("IvBMR", "id",
				getContext().getPackageName()));
		imgView.setVisibility((View.INVISIBLE));
		imgView = view.findViewById(getResources().getIdentifier("IvBBL", "id",
				getContext().getPackageName()));
		imgView.setVisibility((View.INVISIBLE));
		imgView = view.findViewById(getResources().getIdentifier("IvBBM", "id",
				getContext().getPackageName()));
		imgView.setVisibility((View.INVISIBLE));
		imgView = view.findViewById(getResources().getIdentifier("IvBBR", "id",
				getContext().getPackageName()));
		imgView.setVisibility((View.INVISIBLE));
	}

	public void displayMessage(final String message) {
		Toast toast = Toast.makeText(getContext(), message,
				Toast.LENGTH_SHORT);
		toast.show();
	}

	public void displayTurn(final String turn) {
		gridHeading.setText(getString(R.string.turn, turn));
	}

	public void updateCellView(final int position, final char value) {
		adapter.notifyItemChanged(position, value);
	}

	public void displayEndGame(final char gameStatus) {
		switch (gameStatus) {
			case 'D':
				gridHeading.setText(R.string.game_draw);
				break;
			case 'X':
				gridHeading.setText(R.string.x_win);
				break;
			case 'O':
				gridHeading.setText(R.string.o_win);
				break;
		}
	}

	public void displayPlayAgain() {
		if (!online) {
			playAgainBtn.setVisibility(View.VISIBLE);
		}
		else {
			AppDatabase appDatabase = AppDatabase.getInstance(getContext());
			OnlineGameDAO onlineGameDAO = appDatabase.getOnlineGameDAO();
			onlineGameDAO.delete(onlineGame);
		}
	}

	private void openNewGame() {
		// Clear the View and re-enter GameFrag
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.popBackStack();
		MainActivity.openGameFrag(game, onlineGame, fragmentManager);
	}

	/*
		Allow the user to play another game
		Clear the game
		Remove current GameFrag from BackStack
		Create a new GameFrag with the new Game
		Start playing!
	 */
	final class PlayAgainHandler implements View.OnClickListener {
		@Override
		public void onClick(View view) {
			AppDatabase appDatabase = AppDatabase.getInstance(getContext());
			GameDAO gameDAO = appDatabase.getGameDAO();
			game = new Game();

			gameDAO.deleteAll();
			gameDAO.insert(game);

			openNewGame();
		}
	}
}

package edu.byui.cit.ultimatetictactoe.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import edu.byui.cit.ultimatetictactoe.R;
import edu.byui.cit.ultimatetictactoe.model.Game;
import edu.byui.cit.ultimatetictactoe.model.OnlineGame;

import android.os.Bundle;

/*
    TODO: Let the users play again online without having to recreate a whole new game
    TODO: Provide better and longer lasting error messages when appropriate
    TODO: Fix occasional GameFrag bug, it somehow loses Context during game play
    TODO: Allow the user to delete an online game from list view? (Do we want this?)
    TODO: Prevent app from crashing after game completion and then clicking "let's play" again from
     joinGameFrag or GameKeyFrag(these frags display on back key press)
 */
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        openFrag(new MainFrag(), getSupportFragmentManager());
    }

    /*
        Many Fragment classes require opening another Fragment, creating this function reduces
        duplicate code and the potential for mistakes.
     */
    public static void openFrag(Fragment fragment, FragmentManager fragmentManager) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragContainer, fragment, fragment.getClass().getSimpleName());
        fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
        fragmentTransaction.commit();
    }

    /*
        This is the exact same as openFrag, except removes the current Fragment from the BackStack
        This was the best solution that I could find for skipping a fragment that you don't want added
        to the BackStack (simply not include AddToBackStack created an issue of its next Fragment
        overlapping its previous Fragment)
        Thanks to: https://stackoverflow.com/questions/31198894/how-to-delete-a-specific-fragment-from-back-stack-in-android
     */
    public static void openFragNoBack(Fragment fragment, FragmentManager fragmentManager) {
        fragmentManager.popBackStack();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragContainer, fragment, fragment.getClass().getSimpleName());
        fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
        fragmentTransaction.commit();
    }

    /*
        Many Fragment classes require opening GameFrag, creating this function reduces
        duplicate code and the potential for mistakes.
        This method is slightly different than openFrag because GameFrag requires a Game and
        possibly an OnlineGame.
     */
    public static void openGameFrag(Game game, OnlineGame onlineGame, FragmentManager fragmentManager) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment;
        if (onlineGame == null) {
            fragment = new GameFrag(game);
        } else {
            fragment = new GameFrag(game, onlineGame);
        }
        fragmentTransaction.replace(R.id.fragContainer, fragment, GameFrag.class.getSimpleName());
        fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
        fragmentTransaction.commit();
    }
}

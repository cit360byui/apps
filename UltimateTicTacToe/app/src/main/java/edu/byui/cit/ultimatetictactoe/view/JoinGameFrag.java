package edu.byui.cit.ultimatetictactoe.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import edu.byui.cit.ultimatetictactoe.R;
import edu.byui.cit.ultimatetictactoe.model.AppDatabase;
import edu.byui.cit.ultimatetictactoe.model.GameFirebaseDAO;
import edu.byui.cit.ultimatetictactoe.model.OnlineGame;
import edu.byui.cit.ultimatetictactoe.model.OnlineGameDAO;

/*
    1. Link all inputs
    2. Fully implement PlayButtonHandler
 */
public class JoinGameFrag extends Fragment {
    private EditText gameJoinEdit;
    private EditText friendKeyEdit;
    private TextView joinWarningTxt;
    private String title;
    private String key;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_join_game, container, false);

        gameJoinEdit = view.findViewById(R.id.editGameJoin);
        friendKeyEdit = view.findViewById(R.id.editFriendKey);
        joinWarningTxt = view.findViewById(R.id.txtJoinWarning);
        Button joinPlayBtn = view.findViewById(R.id.btnJoinPlay);

        joinPlayBtn.setOnClickListener(new PlayButtonHandler());
        joinWarningTxt.setVisibility(View.GONE);

        return view;
    }

    private void getFriendGame() {
        GameFirebaseDAO.getFriendGame(key, this);
    }

    /*
        Inform the user that the key they entered doesn't exist in Firebase
     */
    public void displayWrongKey() {
        joinWarningTxt.setText(R.string.wrong_key);
        joinWarningTxt.setVisibility(View.VISIBLE);
    }

    /*
        Only called when the game exists
        Store the game's references and setting in OnlineGame
            If the game already exists on the device, display the @string/duplicate_game error message
        Load the game
     */
    public void loadFriendGame(final boolean isCreatorX) {
        OnlineGame onlineGame = new OnlineGame(key, title, new Date(), !isCreatorX);
        AppDatabase appDatabase = AppDatabase.getInstance(getContext());
        OnlineGameDAO onlineGameDAO = appDatabase.getOnlineGameDAO();
        try {
            onlineGameDAO.insert(onlineGame);
            GameFirebaseDAO.loadGame(onlineGame, this);
        } catch (android.database.sqlite.SQLiteConstraintException exception) {
            joinWarningTxt.setText(R.string.duplicate_game);
            joinWarningTxt.setVisibility(View.VISIBLE);
        } catch (Exception exception) {
            Toast.makeText(getContext(), "An error occurred!", Toast.LENGTH_LONG).show();
        }
    }

    /*
        1. Ensure that all data is correctly filled
        2. Attempt to connect to Firebase game
            If can't, provide message, don't move on and don't crash!
     */
    class PlayButtonHandler implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            title = gameJoinEdit.getText().toString();
            key = friendKeyEdit.getText().toString();
            if (title.length() > 0 && key.length() > 0) {
                joinWarningTxt.setVisibility(View.GONE);
                // Get if friend is 'X' or 'O'
                getFriendGame();
            } else {
                joinWarningTxt.setText(R.string.missing_field);
                joinWarningTxt.setVisibility(View.VISIBLE);
            }
        }

    }
}

package edu.byui.cit.ultimatetictactoe.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import edu.byui.cit.ultimatetictactoe.R;
import edu.byui.cit.ultimatetictactoe.model.GameFirebaseDAO;
import edu.byui.cit.ultimatetictactoe.model.OnlineGame;

/*
    1. Display all the values the user entered
    2. Display the key their friend will need to play with them
    3. Load the newly created Game in Firebase
    Fixed: If the user selects to go back, this Fragment covers the Fragment before the previous one
 */
public class GameKeyFrag extends Fragment {
    private OnlineGame onlineGame;

    /*
        Get all the values the user entered
        Get the key that their friend will need to play with them
     */
    GameKeyFrag(OnlineGame onlineGame) {
        this.onlineGame = onlineGame;
    }

    private void loadGame() {
        GameFirebaseDAO.loadGame(onlineGame, this);
    }

    /*
        Display all the values the user entered
        Display the key their friend will need to play with them
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_game_key, container, false);
        // Get reference to TextViews and Button
        TextView gameConfirmTxt = view.findViewById(R.id.txtGameConfirm);
        TextView turnTxt = view.findViewById(R.id.txtTurn);
        TextView keyTxt = view.findViewById(R.id.txtKey);
        Button playBtn = view.findViewById(R.id.btnPlay);
        // Set TextViews' text and Button's onClickListener
        String turnString = (onlineGame.isX()) ? "X" : "O";
        gameConfirmTxt.setText(onlineGame.getGameTitle());
        turnTxt.setText(turnString);
        keyTxt.setText(onlineGame.getKey());
        playBtn.setOnClickListener(new PlayButtonHandler());

        return view;
    }

    /*
        Load the newly created game in Firebase
     */
    class PlayButtonHandler implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            loadGame();
        }
    }
}

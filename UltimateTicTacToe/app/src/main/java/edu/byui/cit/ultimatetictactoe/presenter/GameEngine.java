package edu.byui.cit.ultimatetictactoe.presenter;

import java.util.List;

import edu.byui.cit.ultimatetictactoe.model.Game;
import edu.byui.cit.ultimatetictactoe.view.GameFrag;

/*
    Local Game Functionality:
    Allow the user to make a move
    Update the View based on the game's status
    Prevent any turn that goes against the rules
    When the game is over, display who won and let them play another game
 */
public class GameEngine {
    private Game game;
    private GameFrag gameFrag;

    public GameEngine(Game game, GameFrag gameFrag) {
        this.game = game;
        this.gameFrag = gameFrag;
        gameFrag.displayTurn(getTurnAsString());
        // Check that the game isn't already finished
        char gameStatusTemp = game.getGameStatus();
        if (gameStatusTemp != 'C') {
            gameFrag.displayEndGame(gameStatusTemp);
            gameFrag.displayPlayAgain();
        }
        // Display any won grid
        char[] gridWonTemp = this.game.getGridWon();
        for(int i = 0; i < gridWonTemp.length; i++) {
            if(gridWonTemp[i] != 'F') {
                gameFrag.gridComplete(i, gridWonTemp[i]);
            }
        }
        displayNextGrid(game.getNextGrid());
    }

    public List<Character> getBoard() {
        return game.getBoard();
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Character getCell(int position) {
        return game.getBoard().get(position);
    }

    /*
        Alternate if it's X's or O's turn
        Inform the user if they try to play when the game is over, in the wrong grid, or in an
            invalid cell
        Alter the Grid
        Update gridWon and gameStatus
        Enforce playing in the correct grid during first move after resuming
     */
    public boolean play(int position) {
        Character currentPlayer = game.getXTurn() ? 'X' : 'O';
        int gridNum = getGridNum(position);
        boolean success = false;

        String message;
        if (game.getGameStatus() != 'C') {
            message = "The game is already over!";
            gameFrag.displayMessage(message);
        } else if (gridNum != game.getNextGrid() && game.getNextGrid() != 10) {
            message = "Can't play in that grid!";
            gameFrag.displayMessage(message);
        } else if (game.getGridWon()[gridNum] != 'F') {
            message = "Grid is already complete!";
            gameFrag.displayMessage(message);
        } else if (game.getBoard().get(position) != ' ') {
            message = "Cell is already taken!";
            gameFrag.displayMessage(message);
        } else {
            // Only in this statement do we want the option to push to Firebase
            success = true;
            game.getBoard().set(position, currentPlayer);
            completeGrid(position, currentPlayer);
            // Check if the game's finished
            char gameStatus = gameComplete();
            game.setGameStatus(gameStatus);
            if (gameStatus == 'C') {
                // It's the next player's turn
                game.setXTurn(!game.getXTurn());
                gameFrag.displayTurn(getTurnAsString());
                getNextGrid(position);
            } else {
                gameFrag.clearNextGrid();
                gameFrag.displayEndGame(gameStatus);
                gameFrag.displayPlayAgain();
            }
        }
        return success;
    }

    /*
        Check if the smaller grid will be won vertically
     */
    private boolean verticalWin(int position, Character currentPlayer) {
        int spaceBetweenRow = 9;
        // Initialize begin to the top row
        int begin = position - position / spaceBetweenRow % 3 * spaceBetweenRow;
        // Check if each row is the current player's, if not, return false
        for (int i = begin; i <= begin + spaceBetweenRow * 2; i += spaceBetweenRow) {
            if (game.getBoard().get(i) != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    /*
        Check if a smaller grid will be won horizontally
     */
    private boolean horizontalWin(int position, Character currentPlayer) {
        int gridWidth = 3;
        // Initialize begin to the most left column
        int begin = position - (position % gridWidth);
        // Check if each column is the current player's, if not, return false
        for (int i = begin; i < begin + gridWidth; i++) {
            if (game.getBoard().get(i) != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    /*
        Check if a smaller grid will be won diagonally starting in the top-left corner
     */
    private boolean diagonalToRightWin(int position, Character currentPlayer) {
        // Initialize begin to top-left corner
        int begin = position - (position % 3) - (position / 9 % 3 * 9);
        int spaceBetweenCells = 10;
        // Check that each diagonal square is the current player's
        for (int i = begin; i <= begin + spaceBetweenCells * 2; i += spaceBetweenCells) {
            if (game.getBoard().get(i) != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    /*
        Check if a smaller grid will be won diagonally starting in the top-right corner
     */
    private boolean diagonalToLeftWin(int position, Character currentPlayer) {
        // Initialize begin to top-right corner
        int begin = position + (2 - position % 3) - (position / 9 % 3 * 9);
        int spaceBetweenCells = 8;
        // Check that each diagonal square is the current player's
        for (int i = begin; i <= begin + spaceBetweenCells * 2; i += spaceBetweenCells) {
            if (game.getBoard().get(i) != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    /*
        Get the next grid in which the user can play
     */
    private void getNextGrid(int position) {
        int column = (position % 9) % 3;
        int row = (position / 9) % 3;
        int grid = column + (row * 3);

        if (game.getGridWon()[grid] != 'F')
            grid = 10;
        game.setNextGrid(grid);

        gameFrag.clearNextGrid();
        displayNextGrid(grid);
    }

    void displayNextGrid(int nextGrid){
        gameFrag.clearNextGrid();

        if(nextGrid != 10)
            gameFrag.nextGrid(nextGrid);
        else{
            for(int i = 0; i < 9; i++){
                if(game.getGridWon()[i] == 'F')
                    gameFrag.nextGrid(i);
            }
        }
    }

    /*
        Get the grid in which position resides
     */
    private int getGridNum(int position) {
        int column = position % 9 / 3;
        int row = position / 27;
        return column + (row * 3);
    }

    /*
        Check if the current grid has been won
        If the grid is complete, change the gridWon array
        Display that the grid is taken
     */
    private void completeGrid(int position, Character currentPlayer) {
        if (verticalWin(position, currentPlayer)
                || horizontalWin(position, currentPlayer)
                || diagonalToRightWin(position, currentPlayer)
                || diagonalToLeftWin(position, currentPlayer)) {
            game.setGridWonByPos(getGridNum(position), game.getBoard().get(position));

            gameFrag.gridComplete(getGridNum(position), currentPlayer);
        }
    }

    /*
        Check if any grid is still not finished
        If this method is called and all grids are finished, it was a draw
     */
    private boolean draw() {
        for (char c : game.getGridWon()) {
            if (c == 'F') {
                return false;
            }
        }
        return true;
    }

    /*
        Check if the larger grid has been won, and thus the game
     */
    private char gameComplete() {
        char won;

        if (game.getGridWonByPos(0) != 'F'
                && game.getGridWonByPos(0) == game.getGridWonByPos(1)
                && game.getGridWonByPos(1) == game.getGridWonByPos(2)) {
            won = game.getGridWonByPos(0);
        } else if (game.getGridWonByPos(3) != 'F'
                && game.getGridWonByPos(3) == game.getGridWonByPos(4)
                && game.getGridWonByPos(4) == game.getGridWonByPos(5)) {
            won = game.getGridWonByPos(3);
        } else if (game.getGridWonByPos(6) != 'F'
                && game.getGridWonByPos(6) == game.getGridWonByPos(7)
                && game.getGridWonByPos(7) == game.getGridWonByPos(8)) {
            won = game.getGridWonByPos(6);
        } else if (game.getGridWonByPos(0) != 'F'
                && game.getGridWonByPos(0) == game.getGridWonByPos(3)
                && game.getGridWonByPos(3) == game.getGridWonByPos(6)) {
            won = game.getGridWonByPos(0);
        } else if (game.getGridWonByPos(1) != 'F'
                && game.getGridWonByPos(1) == game.getGridWonByPos(4)
                && game.getGridWonByPos(4) == game.getGridWonByPos(7)) {
            won = game.getGridWonByPos(1);
        } else if (game.getGridWonByPos(2) != 'F'
                && game.getGridWonByPos(2) == game.getGridWonByPos(5)
                && game.getGridWonByPos(5) == game.getGridWonByPos(8)) {
            won = game.getGridWonByPos(3);
        } else if (game.getGridWonByPos(0) != 'F'
                && game.getGridWonByPos(0) == game.getGridWonByPos(4)
                && game.getGridWonByPos(4) == game.getGridWonByPos(8)) {
            won = game.getGridWonByPos(0);
        } else if (game.getGridWonByPos(2) != 'F'
                && game.getGridWonByPos(2) == game.getGridWonByPos(4)
                && game.getGridWonByPos(4) == game.getGridWonByPos(6)) {
            won = game.getGridWonByPos(2);
        } else if (draw()) {
            won = 'D';
        } else {
            won = 'C';
        }

        return won;
    }

    private String getTurnAsString() {
        return (game.getXTurn() ? "X" : "O");
    }
}

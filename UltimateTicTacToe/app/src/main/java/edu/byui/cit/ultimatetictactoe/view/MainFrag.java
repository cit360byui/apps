package edu.byui.cit.ultimatetictactoe.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import edu.byui.cit.ultimatetictactoe.R;
import edu.byui.cit.ultimatetictactoe.model.GameDAO;

import edu.byui.cit.ultimatetictactoe.model.AppDatabase;
import edu.byui.cit.ultimatetictactoe.model.Game;

public class MainFrag extends Fragment {
    private Game game;
    private GameDAO gameDAO;
    private boolean storedGrid;

    /*
        This method creates the view for MainFrag. It does this by attaching MainFrag to
        frag_main.xml, attaching its buttons to the view, and setting their onClickListeners.
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Attach frag_main.xml to this Fragment
        View view;
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_main, container, false);
        // Check if there's a saved game, if so, retrieve and store it, otherwise, create a new Grid
        AppDatabase appDatabase = AppDatabase.getInstance(getContext());
        gameDAO = appDatabase.getGameDAO();
        storedGrid = gameDAO.count() > 0;

        // Only display the continue button if there's a saved game
        Button btnContinue = view.findViewById(R.id.btnContinue);
        if (!storedGrid) {
            btnContinue.setVisibility(View.GONE);
        } else {
            btnContinue.setOnClickListener(new ContinueGameHandler());
        }

        Button btnNew = view.findViewById(R.id.btnNewGame);
        btnNew.setOnClickListener(new NewGameHandler());

        Button btnOnline = view.findViewById(R.id.btnOnline);
        btnOnline.setOnClickListener(new OnlineHandler());

        return view;
    }

    /*
        Display frag_game.xml.
        Look for a saved game, take its data, and pass it to GameFrag.
     */
    final class ContinueGameHandler implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            game = gameDAO.getFirstGame();
            MainActivity.openGameFrag(game, null, getFragmentManager());
        }
    }

    /*
        Display frag_game.xml and execute GameFrag.
        If there's a game saved, display a popup.
     */
    final class NewGameHandler implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            if (storedGrid) {
                // Create the popup asking for a confirmation
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Warning")
                        .setMessage("This will overwrite your saved game. Are you sure you want to do this?")
                        .setPositiveButton("Confirm", new OverwriteListener())
                        .setNegativeButton("Cancel", null);
                builder.show();
            } else {
                game = new Game();
                gameDAO.insert(game);
                MainActivity.openGameFrag(game, null, getFragmentManager());
            }
        }
    }

    /*
        This class creates a new Grid, deletes the old Grid, inserts the new Grid, and changes
        the Fragment to GameFrag.
     */
    final class OverwriteListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            game = new Game();
            gameDAO.deleteAll();
            gameDAO.insert(game);
            MainActivity.openGameFrag(game, null, getFragmentManager());
        }
    }


    final class OnlineHandler implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            MainActivity.openFrag(new OnlineGamesFrag(), getFragmentManager());
        }
    }
}

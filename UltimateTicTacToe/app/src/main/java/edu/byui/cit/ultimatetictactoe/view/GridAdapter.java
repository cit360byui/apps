package edu.byui.cit.ultimatetictactoe.view;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import edu.byui.cit.ultimatetictactoe.R;
import edu.byui.cit.ultimatetictactoe.model.Game;
import edu.byui.cit.ultimatetictactoe.model.OnlineGame;
import edu.byui.cit.ultimatetictactoe.presenter.GameEngine;
import edu.byui.cit.ultimatetictactoe.presenter.OnlineGameEngine;

/*
    This Adapter will populate a RecyclerView to essentially become a grid.
    It takes a list of Characters and sets each cell within its grid to the Character's value.
 */
public class GridAdapter extends RecyclerView.Adapter<GridAdapter.ViewHolder> {
    private GameEngine gameEngine;

    /*
        This is the constructor method.
        It creates a GameEngine to handle the local-game logic.
        It receives and immediately passes gameFrag so that the GameEngine can update the GUI
        while maintaining some modularization.
     */
    GridAdapter(Game game, GameFrag gameFrag) {
        gameEngine = new GameEngine(game, gameFrag);
    }

    /*
        This is the constructor method.
        It's does what the local-constructor (the first one) does, except it creates an
        onlineGameEngine, which is a sub-class of GameEngine.
        Additionally, it passes the onlineGame's key, so that OnlineGameEngine can update Firebase
        appropriately.
     */
    GridAdapter(Game game, GameFrag gameFrag, OnlineGame onlineGame) {
        gameEngine = new OnlineGameEngine(game, gameFrag, onlineGame);
    }

    /*
        This method attaches the Adapter to the grid_item.xml
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        int section = viewType % 27;

        if (section <= 8) {
            switch (section % 3) {
                case 0:
                    view = layoutInflater.inflate(R.layout.top_left_cell, parent, false);
                    break;
                case 1:
                    view = layoutInflater.inflate(R.layout.top_middle_cell, parent, false);
                    break;
                case 2:
                    view = layoutInflater.inflate(R.layout.top_right_cell, parent, false);
                    break;
            }
        } else if (section <= 17) {
            switch (section % 3) {
                case 0:
                    view = layoutInflater.inflate(R.layout.middle_left_cell, parent, false);
                    break;
                case 1:
                    view = layoutInflater.inflate(R.layout.middle_middle_cell, parent, false);
                    break;
                case 2:
                    view = layoutInflater.inflate(R.layout.middle_right_cell, parent, false);
                    break;
            }
        } else {
            switch (section % 3) {
                case 0:
                    view = layoutInflater.inflate(R.layout.bottom_left_cell, parent, false);
                    break;
                case 1:
                    view = layoutInflater.inflate(R.layout.bottom_middle_cell, parent, false);
                    break;
                case 2:
                    view = layoutInflater.inflate(R.layout.bottom_right_cell, parent, false);
                    break;
            }
        }

        return new ViewHolder(view);
    }

    /*
        This method executes the ViewHolder.bind() method defined below and gives it the current
        cell's position.
        Create a Cell and have its data be retrieved and filled in
        Attach an OnClickListener to each cell
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            holder.bind(position);
            holder.itemView.setOnClickListener(new CellClickHandler(position, holder));
        } catch (Exception ex) {
            Log.e("onBindViewHolder TAG", ex.toString());
        }
    }

    /*
        We're going to change this because there is more than 1 view for a grid
     */
    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /*
        This method is used to get the number of items contained within the demoList. This is used
        to populated each cell.
     */
    @Override
    public int getItemCount() {
        return this.gameEngine.getBoard().size();
    }

    void stopListening() {
        ((OnlineGameEngine) gameEngine).stopListening();
    }

    /*
                    This ViewHolder creates each cell within the RecyclerView. All it does for now is get the
                    cell's id and set its text to what's contained within demoList at that position.
                 */
    final class ViewHolder extends RecyclerView.ViewHolder {
        TextView cell;

        /*
            This is the ViewHolder's constructor
         */
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            cell = itemView.findViewById(R.id.txtCell);
        }

        /*
            This method sets the corresponding TextView to the specified element in the demoList
         */
        void bind(int position) {
            Character cellData = gameEngine.getCell(position);
            this.cell.setText(cellData.toString());
        }
    }

    /*
        Every time a cell is clicked, attach this OnClickListener.
        Call GameEngine's play method.
        Update the grid based based on GameEngine's grid.
     */
    final private class CellClickHandler implements View.OnClickListener {
        int position;
        ViewHolder viewHolder;

        CellClickHandler(int position, ViewHolder viewHolder) {
            this.position = position;
            this.viewHolder = viewHolder;
        }

        @Override
        public void onClick(View view) {
            gameEngine.play(position);
            viewHolder.bind(position);
        }
    }
}

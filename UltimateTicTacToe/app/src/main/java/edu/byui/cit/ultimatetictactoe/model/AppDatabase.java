package edu.byui.cit.ultimatetictactoe.model;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = {Game.class, OnlineGame.class}, version = 5, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase singleton;

    public static AppDatabase getInstance(Context appCtx) {
        if (singleton == null) {
            singleton = Room.databaseBuilder(
                    appCtx, AppDatabase.class, "Ultimate Tic Tac Toe")
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return singleton;
    }

    public abstract GameDAO getGameDAO();
    public abstract OnlineGameDAO getOnlineGameDAO();
}

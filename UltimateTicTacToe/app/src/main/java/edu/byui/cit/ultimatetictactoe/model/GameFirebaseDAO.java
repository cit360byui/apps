package edu.byui.cit.ultimatetictactoe.model;

import android.util.Log;

import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import edu.byui.cit.ultimatetictactoe.presenter.OnlineGameEngine;
import edu.byui.cit.ultimatetictactoe.view.JoinGameFrag;
import edu.byui.cit.ultimatetictactoe.view.MainActivity;
import edu.byui.cit.ultimatetictactoe.view.OnlineGamesFrag;

/*
    This class's role is to handel communication between the local game and Firebase. Because web
    APIs are asynchronous, synchronous code, such as that of the OnlinGameEngine, need to be
    executed after the listeners have executed.
    This class inserts a new game to Firebase, loads a selected game, updates the selected game,
    listens to the selected game, and deletes the selected game.
    TODO: Implement delete functionality

    Good resources:
        https://firebase.google.com/docs/database/android/start
 */
public class GameFirebaseDAO {
    private DatabaseReference dbRef;
    private boolean isCreatorX;
    private ValueEventListener gameListener;

    /*
        Get a reference to Firebase
     */
    public GameFirebaseDAO(final String key) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        dbRef = firebaseDatabase.getReference("game").child(key);
    }

    /*
        Insert a new GameFirebase which contains the game state and a position
    */
    public static String insert(final boolean isCreatorX) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference insertRef = firebaseDatabase.getReference("game");
        String gameJson = toJson(new Game());
        GameFirebase gameFirebase = new GameFirebase(gameJson, -1, isCreatorX);
        String key = insertRef.push().getKey();
        insertRef.child(key).setValue(gameFirebase)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("success", "Firebase insert was successful");
                    }
                })
                .addOnCanceledListener(new OnCanceledListener() {
                    @Override
                    public void onCanceled() {
                        Log.e("ERROR", "Firebase insert was canceled");
                    }
                });
        return key;
    }

    /*
        Retrieve a game by its key
        Load the game (remember that web APIs are asynchronous)
        Open a new GameFrag to start the game
        If the game no longer exists:
            1. Display an error message to the user
            2. Keep the user in their current View
            3. Stop the App from crashing
            4. Delete the Game from the Room db, since this will only be clicked in a RecyclerView
     */
    public static void loadGame(final OnlineGame onlineGame, final Fragment fragment) {
        DatabaseReference loadRef = createGameDbRef(onlineGame.getKey());
        loadRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (!dataSnapshot.exists()) {
                        ((OnlineGamesFrag) fragment).removedGame(onlineGame);
                    } else {
                        // Get the game from Firebase
                        GameFirebase gameFirebase = dataSnapshot.getValue(GameFirebase.class);
                        Game game = toGame(gameFirebase.getJsonGame());
                        // Load the GameFrag
                        MainActivity.openGameFrag(game, onlineGame, fragment.getFragmentManager());
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.e("ERROR", "Load Game was cancelled");
                }
            });
    }

    /*
        Connect to the friend's game
        Get if the friend is X or not (last missing piece to create an OnlineGame)
        Call JoinGameFrag's loadGame method
        If the game doesn't exist, display an error message and stay in the current view
     */
    public static void getFriendGame(final String key, final JoinGameFrag joinGameFrag) {
        DatabaseReference loadRef = createGameDbRef(key);
        loadRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Display an error message to the user
                // Keep the user in their current view
                // Stopped the App from crashing
                if (!dataSnapshot.exists()) {
                    joinGameFrag.displayWrongKey();
                } else {
                    // Get the game from Firebase
                    GameFirebase gameFirebase = dataSnapshot.getValue(GameFirebase.class);
                    joinGameFrag.loadFriendGame(gameFirebase.getIsCreatorX());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("ERROR", "Load Game was cancelled");
            }
        });
    }

    /*
        Update the game stored in Firebase as it changes locally
     */
    public void update(final Game game, final int position) {
        GameFirebase gameFirebase = new GameFirebase(toJson(game), position, isCreatorX);
        dbRef.setValue(gameFirebase)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("success", "Update was successful");
                    }
                })
                .addOnCanceledListener(new OnCanceledListener() {
                    @Override
                    public void onCanceled() {
                        Log.e("ERROR", "Reset was canceled");
                    }
                });
    }

    /*
        Whenever the selected Firebase game is changed, send the changed position to the gameEngine
        for logic handling and interface updates.
     */
    public void listenToGame(final OnlineGameEngine onlineGameEngine) {
        gameListener = dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    GameFirebase gameFirebase = dataSnapshot.getValue(GameFirebase.class);
                    onlineGameEngine.updateInterface(gameFirebase.getPosition());
                } else {
                    onlineGameEngine.deleteGame();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR", "The read failed: " + databaseError.getCode());
            }
        });
    }

    public void stopListeningToGame() {
        dbRef.removeEventListener(gameListener);
        dbRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                GameFirebase gameFirebase = dataSnapshot.getValue(GameFirebase.class);
                if (dataSnapshot.exists()) {
                    Game game = toGame(gameFirebase.getJsonGame());
                    if (game.getGameStatus() != 'C') {
                        delete();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("ERROR", "The read failed: " + databaseError.getCode());
            }
        });
    }

    private void delete() {
        dbRef.removeValue();

    }

    private static DatabaseReference createGameDbRef(final String key) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        return firebaseDatabase.getReference("game").child(key);
    }

    private static String toJson(final Game game) {
        Gson gson = new Gson();
        return gson.toJson(game);
    }

    private static Game toGame(final String jsonFormat) {
        Type type = new TypeToken<Game>(){}.getType();
        return new Gson().fromJson(jsonFormat, type);
    }
}

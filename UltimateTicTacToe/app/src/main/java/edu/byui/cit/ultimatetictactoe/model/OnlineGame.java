package edu.byui.cit.ultimatetictactoe.model;

import java.util.Date;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class OnlineGame {
    @NonNull
    @PrimaryKey
    private String key;
    private String gameTitle;
    private Date date;
    private boolean isX;

    public OnlineGame(String key, String gameTitle, Date date, boolean isX) {
        this.key = key;
        this.gameTitle = gameTitle;
        this.date = date;
        this.isX = isX;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getGameTitle() {
        return gameTitle;
    }

    public void setGameTitle(String gameTitle) {
        this.gameTitle = gameTitle;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isX() {
        return isX;
    }

    public void setX(boolean x) {
        isX = x;
    }
}

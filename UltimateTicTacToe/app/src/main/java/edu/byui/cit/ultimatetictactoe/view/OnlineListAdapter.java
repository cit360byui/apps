package edu.byui.cit.ultimatetictactoe.view;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import edu.byui.cit.ultimatetictactoe.R;
import edu.byui.cit.ultimatetictactoe.model.GameFirebaseDAO;
import edu.byui.cit.ultimatetictactoe.model.OnlineGame;

final class OnlineListAdapter extends RecyclerView.Adapter<OnlineListAdapter.ViewHolder> {
    private List<OnlineGame> onlineGameList;
    private OnlineGamesFrag onlineGamesFrag;

    /*
        We need context within the parameters so that we can access the Room db
     */
    OnlineListAdapter(List<OnlineGame> onlineGameList, OnlineGamesFrag onlineGamesFrag) {
        this.onlineGameList = onlineGameList;
        this.onlineGamesFrag = onlineGamesFrag;
    }

    /*
        This method attaches the layout to each row in the RecyclerView
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.onlline_game, parent, false);
        return new ViewHolder(view);
    }

    /*
        This method attaches an OnClickListener to each row
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int index) {
        try {
            holder.bind(index);
            OnlineGame onlineGame = onlineGameList.get(index);
            holder.itemView.setOnClickListener(new GameClickedHandler(onlineGame));
        } catch (Exception ex) {
            Log.e("onBindViewHolder TAG", ex.toString());
        }
    }

    /*
        This method is used to get the number onlineGames the user has
     */
    @Override
    public int getItemCount() {
        return this.onlineGameList.size();
    }

    /*
        This ViewHolder creates each row within the RecyclerView.
     */
    final class ViewHolder extends RecyclerView.ViewHolder {
        TextView titleTxt;
        TextView dateTxt;

        /*
            This is the ViewHolder's constructor
         */
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTxt = itemView.findViewById(R.id.title);
            dateTxt = itemView.findViewById(R.id.date);
        }

        /*
            This method sets the values of the views contained in online_game.xml
         */
        void bind(int index) {
            OnlineGame onlineGame = onlineGameList.get(index);
            DateFormat dateFormat = new SimpleDateFormat("MM/dd/yy hh:mm a");
            String dateString = dateFormat.format(onlineGame.getDate());
            titleTxt.setText(onlineGame.getGameTitle());
            dateTxt.setText(dateString);
        }
    }

    /*
        When a game is clicked, it should display the game in GameFrag
     */
    private final class GameClickedHandler implements View.OnClickListener {
        private OnlineGame onlineGame;

        GameClickedHandler(OnlineGame onlineGame) {
            this.onlineGame = onlineGame;
        }

        /*
            Loads the selected game
         */
        @Override
        public void onClick(View view) {
            GameFirebaseDAO.loadGame(onlineGame, onlineGamesFrag);
        }
    }

}

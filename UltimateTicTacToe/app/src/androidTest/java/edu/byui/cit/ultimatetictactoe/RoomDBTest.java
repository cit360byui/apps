package edu.byui.cit.ultimatetictactoe;

import android.content.Context;

import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import edu.byui.cit.ultimatetictactoe.model.AppDatabase;
import edu.byui.cit.ultimatetictactoe.model.Game;
import edu.byui.cit.ultimatetictactoe.model.GameDAO;

@RunWith(AndroidJUnit4.class)
public final class RoomDBTest {
    private AppDatabase appDatabase;

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        appDatabase = AppDatabase.getInstance(context);
    }

    @After
    public void closeDb() throws IOException {
        appDatabase.close();
    }

    @Test
    public void testGame() {
        GameDAO gameDAO = appDatabase.getGameDAO();
        // Clear the Game Table
        gameDAO.deleteAll();
        assertEquals(0,gameDAO.count());
        // Insert 2 games
        Game game1 = new Game();
        Game game2 = new Game();
        game2.setGameStatus('D');
        gameDAO.insert(game1);
        assertEquals(1, gameDAO.count());
        gameDAO.insert(game2);
        assertEquals(2, gameDAO.count());
        Game storedGame1 = gameDAO.getByKey(game1.getKey());
        assertEquals(game1, storedGame1);
        Game storedGame2 = gameDAO.getByKey(game2.getKey());
        assertEquals(game2, storedGame2);
        // Update one game
        game1.setXTurn(false);
        gameDAO.update(game1);
        assertEquals(2, gameDAO.count());
        storedGame1 = gameDAO.getByKey(game1.getKey());
        assertEquals(game1, storedGame1);
        storedGame2 = gameDAO.getByKey(game2.getKey());
        assertEquals(game2, storedGame2);
        // Remove one game
        gameDAO.delete(game1);
        assertEquals(1, gameDAO.count());
        storedGame2 = gameDAO.getByKey(game2.getKey());
        assertEquals(game2, storedGame2);
        // Remove the last game
        gameDAO.delete(game2);
        assertEquals(0, gameDAO.count());
    }

}

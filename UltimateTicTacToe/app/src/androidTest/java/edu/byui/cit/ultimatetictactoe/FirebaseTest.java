package edu.byui.cit.ultimatetictactoe;

import android.content.Context;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import edu.byui.cit.ultimatetictactoe.model.AppDatabase;
import edu.byui.cit.ultimatetictactoe.model.Game;
import edu.byui.cit.ultimatetictactoe.model.GameDAO;
import edu.byui.cit.ultimatetictactoe.model.GameFirebaseDAO;

@RunWith(AndroidJUnit4.class)
public final class FirebaseTest {
    private GameFirebaseDAO gameFirebaseDAO;
    @Before
    public void connectToFirebase() {
        gameFirebaseDAO = new GameFirebaseDAO();
    }

    @After
    public void closeDb() throws IOException {

    }

    @Test
    public void testGame() {
        gameFirebaseDAO.insert(new Game());
        Game game = new Game();
        gameFirebaseDAO.insert(game);
    }

}

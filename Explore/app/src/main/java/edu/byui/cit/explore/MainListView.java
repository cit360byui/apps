package edu.byui.cit.explore;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MainListView extends Fragment {
    private RecyclerView.Adapter myAdapter;
    private RecyclerView myRecyclerView;
    private RecyclerView.LayoutManager myLayoutManager;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstState){
        View view = null;
        try {
            super.onCreateView(inflater,container,savedInstState);
            view = inflater.inflate(R.layout.main_list_view, container, false);

            MainActivity act = (MainActivity) getActivity();
            myRecyclerView = view.findViewById(R.id.recycler_view);
            myAdapter = new ListViewAdapter(act);
            myRecyclerView.setAdapter(myAdapter);
            myLayoutManager = new LinearLayoutManager(act);
            myRecyclerView.setLayoutManager(myLayoutManager);
        }
        catch(Exception ex){
            Log.e(MainActivity.TAG, ex.toString());
        }
        return view;
    }
}

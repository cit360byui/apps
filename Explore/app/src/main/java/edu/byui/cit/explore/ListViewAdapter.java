package edu.byui.cit.explore;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import edu.byui.cit.model.AppDatabase;
import edu.byui.cit.model.Pin;
import edu.byui.cit.model.PinDAO;


public class ListViewAdapter extends RecyclerView.Adapter<ListViewAdapter.ViewHolder> {
    private MainActivity activity;
    private PinDAO pDao;
    private List<Pin> dataset;

    ListViewAdapter(MainActivity activity){
        this.activity = activity;

        Context appCtx = activity.getApplicationContext();
        AppDatabase db = AppDatabase.getInstance(appCtx);

        pDao = db.getPinDAO();

        dataset = pDao.getAll();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pin_list_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            holder.bind(position);
        }
        catch(Exception ex){
            Log.e(MainActivity.TAG, ex.toString());
        }

    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    final class ViewHolder extends RecyclerView.ViewHolder {
        private Pin pin;

        ViewHolder(View view) {
            super(view);
        }

        void bind(int pos){
            pin = dataset.get(pos);
        }

    }


}

package edu.byui.cit.hangman.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import edu.byui.cit.hangman.R;
import edu.byui.cit.hangman.model.HighScore;
import edu.byui.cit.hangman.model.WordDatabase;

public class GameFrag extends Fragment {


    private TextView word;
    private String wordString;
    private int score = 0;
    private int counter = 0;
    private HighScore HS;
    private WordDatabase WD;
    private String[] listChoice;
    private Context appctx;
    private View view = null;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstState) {


        try {
            super.onCreateView(inflater, container, savedInstState);
            view = inflater.inflate(R.layout.frag_game, container, false);

            appctx = getContext();

            HS = new HighScore(appctx);
            WD = new WordDatabase(appctx);
            listChoice = getArguments().getStringArray("listChoice");
            wordString = getArguments().getString("Word");

            setWord(view);
            setButtons(view);

        } catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }
        return view;
    }

    /**
     * Method to set the word
     *
     * @param view view from onCreate to be able to reference the layouts
     *        The arguments are brought over from the MainFrag in args
     *        The word is then changed to underscores for the blanks
     */
    private void setWord(View view) {
        LinearLayout wordLay = view.findViewById(R.id.layWord);
        wordLay.removeAllViews();

//        wordString = getArguments().getString("Word");
        String[] letters = wordString.trim().split("");
        word = new TextView(view.getContext());
        String blankWord = "";
        wordString = "";
        for (int i = 0; i < letters.length; i++) {
            //letters[i] = "_";
            if (letters[i].equals(" ")) {
                blankWord += "   ";
                wordString += "   ";

            } else {//if(letters[i].equals("")) {
                blankWord += "_ ";
                wordString += letters[i].trim() + " ";


            }
        }
        //The loop adds and extra underscore and space to the end and this cuts it off.
        blankWord = blankWord.substring(0, blankWord.length() - 2);

        word.setText(blankWord.trim());

        word.setTextSize(40);
        wordLay.addView(word);
    }

    /**
     * Method that creates buttons when the fragment is opened
     *
     * @param view view from onCreate to be able to reference the layouts
     *        Letters are added to the buttons which are dynamically added to the layouts in frag_game.xml
     *        Layouts are cleared of all buttons first
     *        The onClickListeners are added within the loops because the buttons don't have IDs and can't be referenced
     *            outside of that instance of the loop.
     */
    private void setButtons(View view) {
        String[] choiceR1 = new String[]{"A", "B", "C", "D", "E", "F", "G", "H", "I"};
        String[] choiceR2 = new String[]{"J", "K", "L", "M", "N", "O", "P", "Q", "R"};
        String[] choiceR3 = new String[]{"S", "T", "U", "V", "W", "X", "Y", "Z"};
        LinearLayout.LayoutParams butParams = new LinearLayout.LayoutParams(70, 70);
        butParams.rightMargin = 10;
        butParams.topMargin = 10;
        Context viewCtx = view.getContext();

//      ToDo: Make the buttons size based on the screen resolution. They don't show up right on some screens
//      ToDo: Maybe find a more efficient way to load the buttons
        LinearLayout layout = view.findViewById(R.id.layLetters);
        layout.removeAllViews();
        for (int count = 0; count < choiceR1.length; count++) {

            final Button button1 = new Button(viewCtx);

            button1.setText(choiceR1[count]);
            button1.setTextColor(Color.parseColor("#FF0000"));
            button1.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.button_un_pressed, null));
            button1.setLayoutParams(butParams);
            button1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    button1.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.button_pressed, null));
                    String letter = (String) button1.getText();
                    checkLetter(letter);
                }
            });
            layout.addView(button1);
        }

        layout = view.findViewById(R.id.layLetters2);
        layout.removeAllViews();
        for (int count = 0; count < choiceR2.length; count++) {

            final Button button1 = new Button(viewCtx);

            button1.setText(choiceR2[count]);
            button1.setTextColor(Color.parseColor("#FF0000"));
            button1.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.button_un_pressed, null));
            button1.setLayoutParams(butParams);
            button1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    button1.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.button_pressed, null));
                    String letter = (String) button1.getText();
                    checkLetter(letter);
                }
            });
            layout.addView(button1);
        }

        layout = view.findViewById(R.id.layLetters3);
        layout.removeAllViews();
        for (int count = 0; count < choiceR3.length; count++) {

            final Button button1 = new Button(viewCtx);

            button1.setText(choiceR3[count]);
            button1.setTextColor(Color.parseColor("#FF0000"));
            button1.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.button_un_pressed, null));
            button1.setLayoutParams(butParams);
            button1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    button1.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.button_pressed, null));
                    CharSequence letter = button1.getText();
                    checkLetter(letter);
                }
            });
            layout.addView(button1);
        }
    }

    /**
     * Checks if the users letter choice is in the word
     *
     * @param letter receives the letter from the pressed button
     *        If it is not, change the image and add one to the counter.
     *        If it is, replace the underscore with the letter.
     */
    private void checkLetter(CharSequence letter) {
        boolean correct = false;
        String[] mainLetters = wordString.trim().split("");
        String userWord = word.getText().toString().trim();
        String[] userLetters = userWord.split("");
        ImageView image;
        image = view.findViewById(R.id.imgHang);

        for (int i = 0; i < mainLetters.length; i++) {
            if (mainLetters[i].equalsIgnoreCase(letter.toString())) {
                correct = true;
                userLetters[i] = letter.toString();
            }
        }

        if (!correct)
            counter += 1;
        switch (counter) {
            case 1:
                image.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.one, null));
                break;
            case 2:
                image.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.two, null));
                break;
            case 3:
                image.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.three, null));
                break;
            case 4:
                image.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.four, null));
                break;
            case 5:
                image.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.five, null));
                break;
            case 6:
                image.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.six, null));
                break;
            case 7:
                image.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.seven, null));
                break;
            case 8:
                image.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.eight, null));
                gameLose();
                image.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.hangman_start, null));
                break;

        }
        String newWord = TextUtils.join("", userLetters);

//      if the user wins
        if (!newWord.contains("_")) {
            image.setImageDrawable(null);
            image.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.hangman_start, null));
            counter = 0;
            gameWin();
        }
        word.setText(newWord);
    }

    /**
     * Method that is called when the user loses.
     * Called from checkLetter
     * Creates a popup that says they lose and asks if they want another game.
     * Uses the win_window.xml
     */
    private void gameLose() {
        Context context = getContext();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();

//      Width for popupWindow
        int width = displayMetrics.widthPixels - 20;

        LayoutInflater inflateChoice = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflateChoice.inflate(R.layout.win_window, null);
        final PopupWindow lose = new PopupWindow();

        lose.setContentView(layout);
        lose.setWidth(width);
//      Set height of popup: -2 wraps content
        lose.setHeight(-2);
        lose.setFocusable(true);
        lose.showAtLocation(layout, Gravity.CENTER, 1, 1);
        lose.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.rgb(255, 255, 255)));

        TextView loseMsg = layout.findViewById(R.id.winTitle);
        loseMsg.setText("Game Over! Want to play again?");
//      Subtract from the score if it is not 0
        if (score > 0)
            score--;

        Button nxtGame = layout.findViewById(R.id.btnNextGame);
        nxtGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                lose.dismiss();
                WD.runWordDatabase(listChoice);
                counter = 0;

                wordString = WD.getSelectedWord();

                setWord(getView());
                setButtons(getView());
            }
        });

        Button canGame = layout.findViewById(R.id.gameCancel);
        canGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lose.dismiss();


//              ToDo: Implement a way for the user to type in a name or User
                HS.scoreWriter(score, "User");

                Fragment nextfrag = new MainFrag();
                FragmentManager mgr = getFragmentManager();
                FragmentTransaction trans = mgr.beginTransaction();
                trans.replace(R.id.fragContainer, nextfrag);
                trans.addToBackStack(null);
                trans.commit();
            }
        });
    }

    /**
     * Method is called when the user wins
     * Called from checkLetter
     * Creates a popup window with two buttons
     * Adds to the score for high scores
     * Uses win_window.xml
     */
    private void gameWin() {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
//      Width for popupWindow
        int width = displayMetrics.widthPixels - 20;

        LayoutInflater inflateChoice = (LayoutInflater) appctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflateChoice.inflate(R.layout.win_window, null);
        final PopupWindow win = new PopupWindow();

        win.setContentView(layout);
        win.setWidth(width);
//      Set height of popup: -2 wraps content
        win.setHeight(-2);
        win.setFocusable(true);
        win.showAtLocation(layout, Gravity.CENTER, 1, 1);
        win.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.rgb(255, 255, 255)));

        score++;

        Button nxtGame = layout.findViewById(R.id.btnNextGame);
        nxtGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//              ToDo: Implement a way for the users to select a new category if they wish on new game
                win.dismiss();
                WD.runWordDatabase(listChoice);

                wordString = WD.getSelectedWord();

                setWord(view);
                setButtons(view);
            }
        });

        Button canGame = layout.findViewById(R.id.gameCancel);
        canGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                win.dismiss();

//              ToDo: Implement a way for the user to type in a name or User
                HS.scoreWriter(score, "User");

                Fragment nextfrag = new MainFrag();
                FragmentManager mgr = getFragmentManager();
                FragmentTransaction trans = mgr.beginTransaction();
                trans.replace(R.id.fragContainer, nextfrag);
                trans.addToBackStack(null);
                trans.commit();
            }
        });
    }
}


package edu.byui.cit.hangman.model;


import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class WordDatabase {


    private String selectedWord;
    private String selectedHint;
    private AssetManager mgr;
    private String[] listChoice;


    //basic constructor that gets the context and the asset manager from the MainFrag
    public WordDatabase(Context context) {
        mgr = context.getResources().getAssets();
    }

    //gets the listChoice from the fragment then runs the reader.
     public void runWordDatabase(String[] listChoice){

        this.listChoice = listChoice;
        WordListReader();

    }

    //The function that reads the word list files in the asset folder
    private void WordListReader() {
        String wordLine;
        String[] split;

        try {

            InputStreamReader ir = new InputStreamReader(mgr.open(listChoice[0]));
            BufferedReader br = new BufferedReader(ir);

            int lines = Integer.parseInt(listChoice[1]);

            //picks a random number between min and max
            double ranLineNum = ((Math.random() * ((lines) + 1)) + 0);


            //uses the random number to get to the line.
            for (int i = 0; i < (int) ranLineNum; i++) {
                br.readLine();
            }

//          ToDo:see if the random number actually correspondes to the file line and if the wordLine variable is the same or the next line.

            //assigns the line to wordLine
            wordLine = br.readLine();

            //splits the word and the hint.
            split = wordLine.split(":", 2);

            //assigns the word and hint
            selectedWord = split[0];
            selectedHint = split[1];

        } catch (IOException ex) {
            Log.e("IOException", ex.getMessage(), ex);
        }


    }


    public String getSelectedWord() {

        return selectedWord;
    }

    public String getSelectedHint() {

        return selectedHint;
    }


}

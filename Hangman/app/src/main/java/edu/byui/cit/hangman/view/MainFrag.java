package edu.byui.cit.hangman.view;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import edu.byui.cit.hangman.R;
import edu.byui.cit.hangman.model.WordDatabase;

// ToDo: get everything ready for translation to other languages
public class MainFrag extends Fragment {

    private Context appctx;
    private String[] listChoice = new String[2];
    private WordDatabase WD;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstState) {
        View view = null;

        try {
            super.onCreateView(inflater, container, savedInstState);
            view = inflater.inflate(R.layout.frag_main, container, false);

            appctx = getContext();
            WD = new WordDatabase(appctx);

            Button start = view.findViewById(R.id.btnStart);
            start.setOnClickListener(new startClick());

            Button exit = view.findViewById(R.id.btnExit);
            exit.setOnClickListener(new exitListen());

            Button btnRand = view.findViewById(R.id.btnRand);
            btnRand.setOnClickListener(new randListen());

            Button btnScores = view.findViewById(R.id.btnScores);
            btnScores.setOnClickListener(new scoresListen());


        } catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }
        return view;
    }

    private class startClick implements View.OnClickListener {
        private DisplayMetrics displayMetrics = getResources().getDisplayMetrics();

        //            Width for popupWindow
        private int width = displayMetrics.widthPixels - 20;

        private LayoutInflater inflateChoice = (LayoutInflater) appctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        private final View layout = inflateChoice.inflate(R.layout.frag_choice, null);
        private final PopupWindow choices = new PopupWindow();
        private final RadioGroup rg1 = layout.findViewById(R.id.leftRadioGroup);
        private final RadioGroup rg2 = layout.findViewById(R.id.rightRadioGroup);


        @Override
        public void onClick(View v) {

            choices.setContentView(layout);
            choices.setWidth(width);
//            Set height of popup: -2 wraps content
            choices.setHeight(-2);
            choices.setFocusable(true);
            Button cancel = layout.findViewById(R.id.btnCancel);
            cancel.setOnClickListener(new View.OnClickListener(

            ) {
                @Override
                public void onClick(View view) {
                    choices.dismiss();
                }
            });
            choices.showAtLocation(layout, Gravity.CENTER, 1, 1);
            choices.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.rgb(255, 255, 255)));


            // Clears any lingering checked radio buttons.
            rg1.clearCheck();
            rg2.clearCheck();

            rg1.setOnCheckedChangeListener(listener1);
            rg2.setOnCheckedChangeListener(listener2);


            Button begin = layout.findViewById(R.id.btnBegin);
            begin.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    int btnID;
                    String toastMes = null;


                    //Grabs the id of the radio buttton that was selected
                    if (rg1.getCheckedRadioButtonId() != -1) {
                        btnID = rg1.getCheckedRadioButtonId();
                    } else {
                        btnID = rg2.getCheckedRadioButtonId();
                    }

                    //Checks for what radio button was checked and assigns the array with the appropriate parameters.
                    switch (btnID) {

                        case R.id.allRadio:
                            //0 is the text file name
                            listChoice[0] = "all.txt";
                            //1 is the line count
                            listChoice[1] = "1258";
                            break;

                        case R.id.animalsRadio:
                            listChoice[0] = "animals.txt";
                            listChoice[1] = "458";
                            break;

                        case R.id.bibleRadio:
                            listChoice[0] = "bible.txt";
                            listChoice[1] = "65";
                            break;

                        case R.id.colorRadio:
                            listChoice[0] = "color.txt";
                            listChoice[1] = "167";

                            break;

                        case R.id.dictionaryRadio:
                            listChoice[0] = "dict_defs.txt";
                            listChoice[1] = "20120";
                            break;

                        case R.id.foodRadio:
                            listChoice[0] = "food.txt";
                            listChoice[1] = "123";
                            break;

                        case R.id.musicRadio:
                            listChoice[0] = "music.txt";
                            listChoice[1] = "216";
                            break;

                        case R.id.satRadio:
                            listChoice[0] = "sat_prac.txt";
                            listChoice[1] = "5010";
                            break;

                        default:
                            toastMes = "Please select an option";
                            break;
                    }

                    //Essentially checks to make sure a radio button is selected then moves to the game frag while passing the selected word and hint.
                    //Else it displays a toast to pick a choice.
                    if (toastMes == null) {
                        WD.runWordDatabase(listChoice);
                        Fragment frag = new GameFrag();
                        FragmentTransaction trans = getFragmentManager().beginTransaction();
                        Bundle args = new Bundle();
                        args.putString("Word", WD.getSelectedWord());
                        args.putString("Hint", WD.getSelectedHint());
                        args.putStringArray("listChoice", listChoice);
                        frag.setArguments(args);
                        trans.replace(R.id.fragContainer, frag);
                        trans.addToBackStack(null);
                        trans.commit();
                        choices.dismiss();
                    } else {

//                      Todo: Change theme to make toast visible again.
                        Toast.makeText(getActivity(), toastMes, Toast.LENGTH_LONG).show();
                        choices.dismiss();

                    }

                }


            });


        }

        //Listeners that checks to make sure only one radio button is selected in the two radio groups
        final RadioGroup.OnCheckedChangeListener listener1 = new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId != -1) {
                    rg2.setOnCheckedChangeListener(null);
                    rg2.clearCheck();
                    rg2.setOnCheckedChangeListener(listener2);
                }
            }
        };

        final RadioGroup.OnCheckedChangeListener listener2 = new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId != -1) {
                    rg1.setOnCheckedChangeListener(null);
                    rg1.clearCheck();
                    rg1.setOnCheckedChangeListener(listener1);
                }
            }
        };
    }

    private class exitListen implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            System.exit(0);
        }
    }

    private class randListen implements View.OnClickListener {


        @Override
        public void onClick(View view) {

            //Picks a random number
            double ranLineNum = ((Math.random() * ((8 - 1) + 1)) + 1);

            //uses the random number to choose a word list
            switch ((int) ranLineNum) {

                case 1:
                    //0 is the text file name
                    listChoice[0] = "all.txt";
                    //1 is the maximum line number
                    listChoice[1] = "1258";
                    break;

                case 2:
                    listChoice[0] = "animals.txt";
                    listChoice[1] = "458";
                    break;

                case 3:
                    listChoice[0] = "bible.txt";
                    listChoice[1] = "65";
                    break;

                case 4:
                    listChoice[0] = "color.txt";
                    listChoice[1] = "167";

                    break;

                case 5:
                    listChoice[0] = "dict_defs.txt";
                    listChoice[1] = "20120";
                    break;

                case 6:
                    listChoice[0] = "food.txt";
                    listChoice[1] = "123";
                    break;

                case 7:
                    listChoice[0] = "music.txt";
                    listChoice[1] = "216";
                    break;

                case 8:
                    listChoice[0] = "sat_prac.txt";
                    listChoice[1] = "5010";
                    break;
            }


            WD.runWordDatabase(listChoice);

            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();

//            Width for popupWindow based on screen size
            int width = displayMetrics.widthPixels - 20;

            LayoutInflater inflateRand = (LayoutInflater) appctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflateRand.inflate(R.layout.frag_rand, null);
            final PopupWindow randWord = new PopupWindow();
            randWord.setContentView(layout);
            randWord.setWidth(width);
//            Set height of popup: -2 wraps content
            randWord.setHeight(-2);
            randWord.setFocusable(true);
//            Okay button listener
            Button okay = layout.findViewById(R.id.btnOk);
            okay.setOnClickListener(new View.OnClickListener(

            ) {
                @Override
                public void onClick(View view) {
                    randWord.dismiss();
                }
            });
            randWord.showAtLocation(layout, Gravity.CENTER, 1, 1);
            TextView word = layout.findViewById(R.id.txtWord);
            TextView hint = layout.findViewById(R.id.txtHint);
            word.setText(WD.getSelectedWord());
            hint.setText(WD.getSelectedHint());
            randWord.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.rgb(255, 255, 255)));


        }
    }

    private class scoresListen implements View.OnClickListener {

        @Override
        public void onClick(View v) {

            Fragment nextfrag = new ScoreBoardFrag();
            FragmentManager mgr = getFragmentManager();
            FragmentTransaction trans = mgr.beginTransaction();
            trans.replace(R.id.fragContainer, nextfrag);
            trans.addToBackStack(null);
            trans.commit();

        }

    }
}






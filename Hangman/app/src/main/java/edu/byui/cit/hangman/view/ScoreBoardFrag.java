package edu.byui.cit.hangman.view;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import edu.byui.cit.hangman.R;
import edu.byui.cit.hangman.model.HighScore;
import edu.byui.cit.hangman.model.ScoreAdapter;

public class ScoreBoardFrag extends Fragment {
    private HighScore HS;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstState) {
        View view = null;

        try {
            super.onCreateView(inflater, container, savedInstState);
            view = inflater.inflate(R.layout.frag_high_score_list, container, false);

            Context appctx = getContext();
            HS = new HighScore(appctx);

            RecyclerView scoreList = view.findViewById(R.id.scoreList);
            RecyclerView.Adapter ScoreListAdapter = new ScoreAdapter(HS.getUserList(), HS.getScoreList());
            scoreList.setAdapter(ScoreListAdapter);

            scoreList.setHasFixedSize(true);

            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(appctx);
            scoreList.setLayoutManager(layoutManager);


            Button cancel = view.findViewById(R.id.scoreBtnCancel);
            cancel.setOnClickListener(new cancelListen());

            Button clear = view.findViewById(R.id.scoreBtnClear);
            clear.setOnClickListener(new clearListen());


        } catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }
        return view;
    }

    private class cancelListen implements View.OnClickListener {

        @Override
        public void onClick(View v) {

            Fragment nextfrag = new MainFrag();
            FragmentManager mgr = getFragmentManager();
            FragmentTransaction trans = mgr.beginTransaction();
            trans.replace(R.id.fragContainer, nextfrag);
            trans.addToBackStack(null);
            trans.commit();

        }

    }

    private class clearListen implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            HS.scoreWiper();
            HS.scoreReader();

        }
    }
}

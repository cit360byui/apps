package edu.byui.cit.hangman.model;

import android.content.Context;
import android.content.SharedPreferences;

public class HighScore {

    private int[] scoreList = new int[10];
    private String[] userList = new String[10];
    private Context appctx;
    private SharedPreferences sharedPref;


    //Constructor that gets the context and assigns a sharedPreference and editor
    public HighScore(Context context) {
        appctx = context;
        sharedPref = appctx.getSharedPreferences("Score", Context.MODE_PRIVATE);
        scoreReader();
    }

    //Function that writes the score to the sharedPreference
    public void scoreWriter(int score, String user) {
        SharedPreferences.Editor editor = sharedPref.edit();
        String scoreFile;
        String userFile;

        //Scans for where to place the new score, if it's not higher than any in the board then it does nothing.
        for (int i = 0; i < scoreList.length; i++) {
            if (score > scoreList[i]) {
                scoreFile = "High_Score" + i;
                userFile = "Users" + i;
                editor.putInt(scoreFile, score);
                editor.putString(userFile, user);
                editor.apply();
                break;
            }
        }
    }


    //function that wipes the scores
    public void scoreWiper() {
        appctx.getSharedPreferences("Score", 0).edit().clear().apply();

    }

    //function the reads the score and user from the preferences
    public void scoreReader() {
        int tmpS;
        String tmpU;

        //Reads from the shared preferences
        for (int i = 0; i < scoreList.length; i++) {
            userList[i] = sharedPref.getString("Users" + i, "User");
            scoreList[i] = sharedPref.getInt("High_Score" + i, 0);
        }

        //sorts the scores and the corresponding user from greatest to least
        for (int i = 0; i < scoreList.length; i++) {

            for (int j = i; j > 0; j--) {
                if (scoreList[j] > scoreList[j - 1]) {

                    tmpS = scoreList[j];
                    scoreList[j] = scoreList[j - 1];
                    scoreList[j - 1] = tmpS;

                    tmpU = userList[j];
                    userList[j] = userList[j - 1];
                    userList[j - 1] = tmpU;
                }
            }

        }

    }


    public String[] getUserList() {
        return userList;
    }

    public int[] getScoreList() {
        return scoreList;
    }
}

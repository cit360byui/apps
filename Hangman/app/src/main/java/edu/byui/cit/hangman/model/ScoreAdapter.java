package edu.byui.cit.hangman.model;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import edu.byui.cit.hangman.R;



public class ScoreAdapter extends RecyclerView.Adapter<ScoreAdapter.ViewHolder> {
    private String[] userList;
    private int[] scoreList;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView score, user;
        public ViewHolder(View view) {
            super(view);
            user = view.findViewById(R.id.user);
            score = view.findViewById(R.id.score);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ScoreAdapter(String[] userList, int[] scoreList) {
        this.userList = userList;
        this.scoreList = scoreList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ScoreAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.score_list, parent, false);

        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        String scoreText = "" + scoreList[position];

        holder.user.setText(userList[position]);
        holder.score.setText(scoreText);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return userList.length;
    }
}
package edu.byui.cit.hangman.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import edu.byui.cit.hangman.R;
import edu.byui.cit.hangman.model.WordDatabase;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "Hangman";

    @Override
    protected void onCreate(Bundle savedInstState) {
        try {
            super.onCreate(savedInstState);
            setContentView(R.layout.activity_main);

            if (savedInstState == null) {
                // Create the main fragment and place it
                // as the first fragment in this activity.
                Fragment frag = new MainFrag();
                FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
                trans.add(R.id.fragContainer, frag, TAG);
                trans.commit();
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.toString());
        }
    }
}

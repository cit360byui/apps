package edu.byui.cit.jokeapp.model;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import edu.byui.cit.jokeapp.R;
import edu.byui.cit.jokeapp.view.MainActivity;

public class JokeFrag extends Fragment {
    String TAG = "";
    //Buttons
    Button prev;
    Button next;
    FloatingActionButton fab;
    TextView mainJoke;
    //Index for keeping track of which joke is currently being displayed. Starts at index 0 (first joke)
    int index = 0;
    //Snackbar for alerting the user that there are no more previous/more jokes.
    Snackbar snackbar;
    //arrayList for the jokes pulled from Firebase
    ArrayList<String> jokeList = new ArrayList<>(5);


    //Default Constructor
    public JokeFrag() {
    }

    //Method for every fragment
    public JokeFrag(View view) {
        //Buttons
        prev = view.findViewById(R.id.previousJoke);
        prev.setOnClickListener(new previousButton());
        next = view.findViewById(R.id.newRandomJoke);
        next.setOnClickListener(new nextButton());
        fab = view.findViewById(R.id.Fab);
        fab.setOnClickListener(new addJoke());
        //MainJoke TextView
        mainJoke = view.findViewById(R.id.MainJokeTextView);

        jokeList.add("Lol");
        jokeList.add("Number 2");
        jokeList.add("number 3");

        //https://console.firebase.google.com/u/0/project/jokes-e7b5d/overview
        //Get the jokes from the database using the TAG.
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference(TAG);
        //Todo Pull from database and make it into a list.

        //Get joke from getJokeIndex() method and set the displaying joke.
        mainJoke.setText(jokeList.get(index));
        //Since the joke list is starting on the first joke (index 0), there are no more previous jokes.
        prev.setText(R.string.noPreviousJokesTxt);


    }

    //Previous joke
    public class previousButton implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            try {
                //No matter what, previous button goes decrements the index
                index -= 1;
                //Make sure there are no other previous jokes to go back to at the start.
                if (index < 0) {
                    snackbar = Snackbar.make(view, "There is no previous joke!! Read the button! >:l", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                    //If there isn't, add 1.
                    index += 1;
                } else if (index == 0) {
                    //If the index is 0, set the previous button text to "No previous Jokes"
                    prev.setText(R.string.noPreviousJokesTxt);
                } else if (index == jokeList.size() - 2) {
                    //If the current joke is the last joke in the list, set the Next button BACK to "Next Joke"
                    //(Since the nextButton() method changes it to "No more jokes"
                    next.setText(R.string.nextJokeBtn);

                }

                mainJoke.setText(jokeList.get(index));

            } catch (Exception ex) {
                Log.e(MainActivity.TAG, ex.toString());
            }
        }

    }

    //Next joke to display in the TextView
    public class nextButton implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            //Always add 1 to the index
            index += 1;
            //Set the next button to say "no more jokes" when the user has read all the jokes.
            if (index == jokeList.size() - 1) {
                next.setText(R.string.noMoreJokesTxt);
                //Yell at the user that there are no more jokes if they press the next button.
            } else if (index == jokeList.size()) {
                snackbar = Snackbar.make(view, "There are no more jokes!! Read the button! >:l", Snackbar.LENGTH_SHORT);
                snackbar.show();
                //Decrement 1 since the user tried to go past the last joke in the list.
                index -= 1;
                //After reaching the beginnning (not the first time), set the previous joke button to normal
            } else if (index == 1) {
                prev.setText(R.string.previousJokeBtn);
            }

            mainJoke.setText(jokeList.get(index));
        }
    }

    //Add a joke - Button
    public class addJoke extends MainActivity implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            index = 0;
            FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
            trans.replace(R.id.fragContainer, new edu.byui.cit.jokeapp.view.addJoke(), "addJoke");
            trans.addToBackStack(null);
            trans.commit();

        }
    }

}

package edu.byui.cit.jokeapp.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import edu.byui.cit.jokeapp.R;
import edu.byui.cit.jokeapp.model.JokeFrag;

//Extend JokeFrag for Fragment parent and buttons functions
public class koreanFrag extends JokeFrag {
    static final String TAG = "Korean Jokes";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstState) {

        View view = null;
        try {
            super.onCreateView(inflater, container, savedInstState);
            // Load the xml file that corresponds to this Java file.
            view = inflater.inflate(R.layout.korean_jokes, container, false);


            /**
             | The "hunk" of the fragment. Refers to JokeFrag. That has all the code each other fragment has.
             |
             **/
            new JokeFrag(view);

        } catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }

        return view;

    }

}
package edu.byui.cit.jokeapp.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import edu.byui.cit.jokeapp.R;

public class MainFrag extends Fragment {
    static final String TAG = "MainFrag";

    //Mainactivity for all the fragments
    //Just some kicks and giggles. Ignore these.
    int giggles;
    Button null2;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstState) {
        View view = null;
        try {

            // TODO: 11/20/2019 - List of little tasks that need to be done:
            // TODO: - Copy next/prev button code from Knock Knock to the other fragments.
            // TODO: - Link the Fabs in all of the fragments to the addJoke fragment. (Use the "SwitchToFragment" method, used in the OnClickListeners below.)
            // TODO: - Make the Joke of the day (Jotd) look more like a button and not a pretty title in MainFrag.
            // TODO: - Come up with code that picks a random joke from the Firebase database for the Joke of the day (Jotd).
            //               I was thinking of using two variables of random numbers, one for the category, and one for the index of the joke in that category.
            // TODO: - ^- Associated with the previous TODO, think of how to make the "Reveal Punchline"
            //              button come up on the Jotd if the joke is a pun.
            // TODO: - Make the "submit" button on addJoke do something. (1. Firebase 2. Maybe make a snackbar saying "Success!" or something. (copy the code from Knock Knock)
            //              3. Then go back to MainFrag
            // TODO: - Make the punch line button
            // TODO: - Add a little "settings" page to view and edit all the jokes.


            super.onCreateView(inflater, container, savedInstState);

            // Load the xml file that corresponds to this Java file.
            view = inflater.inflate(R.layout.frag_main, container, false);

            //All the buttons for the different fragments and their OnClickListeners

            //Joke of The Day button is not being used.
//            Button newJokeBtn = view.findViewById(R.id.newJokeButton);
//            newJokeBtn.setOnClickListener(new jodt());
            Button blonde_jokes = view.findViewById(R.id.blonde_jokesBtn);
            blonde_jokes.setOnClickListener(new HandleBlonde());
            Button jokes_story = view.findViewById(R.id.jokes_storyBtn);
            jokes_story.setOnClickListener(new HandleStory());
            Button knock_knock = view.findViewById(R.id.knock_knockBtn);
            knock_knock.setOnClickListener(new HandleKnock());
            Button korean_jokes = view.findViewById(R.id.korean_jokesBtn);
            korean_jokes.setOnClickListener(new HandleKorean());
            Button pun_layout = view.findViewById(R.id.pun_layoutBtn);
            pun_layout.setOnClickListener(new HandlePun());
            Button chuck = view.findViewById(R.id.chuck_norrisBtn);
            chuck.setOnClickListener(new HandleChuck());
            Button misc = view.findViewById(R.id.Misc_JokeBtn);
            misc.setOnClickListener(new HandleMisc());

            //Null buttons, for kicks and giggles
            null2 = view.findViewById(R.id.null2);
            null2.setOnClickListener(new HandleNulls());

            //Fab for adding a joke.
            FloatingActionButton fab = view.findViewById(R.id.MainFragFab);
            fab.setOnClickListener(new HandleAddJoke());

        } catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }
        return view;


    }



    // -------------- On Click Listeners ------------- //

    //Blonde Jokes
    private final class HandleBlonde implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            MainActivity mainActivity = (MainActivity) getActivity();
            mainActivity.switchToFragment(new blondeJokes(), "Blonde Jokes");

        }
    }

    //Story Jokes
    private final class HandleStory implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            MainActivity mainActivity = (MainActivity) getActivity();
            mainActivity.switchToFragment(new storyJokes(), "Story Jokes");

        }
    }

    //Knock knock jokes
    private final class HandleKnock implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            MainActivity mainActivity = (MainActivity) getActivity();
            mainActivity.switchToFragment(new KnockKnockFrag(), "Knock Knock Jokes");

        }
    }

    //Korean Jokes
    private final class HandleKorean implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            MainActivity mainActivity = (MainActivity) getActivity();
            mainActivity.switchToFragment(new koreanFrag(), "Korean Jokes");

        }
    }

    //Puns
    private final class HandlePun implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            MainActivity mainActivity = (MainActivity) getActivity();
            mainActivity.switchToFragment(new punJokes(), "Puns");

        }
    }

    //Chuck norris jokes
    private class HandleChuck implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            MainActivity act = (MainActivity) getActivity();
            act.switchToFragment(new ChuckNorris(), "Chuck Norris Jokes");
        }

    }

    //MiscJokes
    private final class HandleMisc implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            MainActivity mainActivity = (MainActivity) getActivity();
            mainActivity.switchToFragment(new MiscJoke(), "Misc Jokes");

        }
    }

    //Add a joke
    private class HandleAddJoke implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            MainActivity act = (MainActivity) getActivity();
            act.switchToFragment(new addJoke(), "addJoke");
        }

    }


    //For the null buttons when pressed.
    private class HandleNulls implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            //Useless Stuff don't mind this. Not important.
            //Snackbar for kicks and giggles.
            Snackbar snackbar;
            //If statements for when the null buttons get clicked.
            if (giggles == 2) {
                snackbar = Snackbar.make(view, "I said nothing appears to be here...", Snackbar.LENGTH_LONG);
                snackbar.show();
            } else if (giggles == 3) {
                snackbar = Snackbar.make(view, "I SAID nothing appears to be here...", Snackbar.LENGTH_LONG);
                snackbar.show();
            } else if (giggles == 4) {
                snackbar = Snackbar.make(view, "I SAID NOTHING APPEARS TO BE HERE...!!", Snackbar.LENGTH_LONG);
                snackbar.show();
            } else if (giggles == 5) {
                snackbar = Snackbar.make(view, "NOTHING IS HERE!!", Snackbar.LENGTH_LONG);
                snackbar.show();
            } else if (giggles >= 6 && giggles < 10) {
                snackbar = Snackbar.make(view, "NOTHING IS HERE!!!!!! DEAL WITH IT!!! >:(", Snackbar.LENGTH_LONG);
                snackbar.show();
            } else if (giggles == 10) {
                null2.setEnabled(false);
                snackbar = Snackbar.make(view, "Ha.... Now you can't do anything. >:P", Snackbar.LENGTH_LONG);
                snackbar.show();
            } else if (giggles > 10) {
                giggles = 0;
            } else {
                snackbar = Snackbar.make(view, "Nothings appears to be here...", Snackbar.LENGTH_LONG);
                snackbar.show();
            }

            giggles += 1;
        }

    }
}
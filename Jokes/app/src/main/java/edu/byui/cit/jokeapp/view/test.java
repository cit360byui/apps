package edu.byui.cit.jokeapp.view;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import edu.byui.cit.jokeapp.R;

public class test extends AppCompatActivity {


    private static final String TAG = "MainActivity";


    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.test);

    }


    public void basicReadWrite() {

        // [START write_message]

        // Write a message to the database

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("StoryJokes");

        myRef.setValue("Hello, World!");

        // [END write_message]
        // [START read_message]
        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {

            @Override

            public void onDataChange(DataSnapshot dataSnapshot) {

                // This method is called once with the initial value and again

                // whenever data at this location is updated.

                String value = dataSnapshot.getValue(String.class);

                Log.d(TAG, "Value is: " + value);

            }


            @Override

            public void onCancelled(DatabaseError error) {

                // Failed to read value

                Log.w(TAG, "Failed to read value.", error.toException());

            }

        });

        // [END read_message]

    }

}
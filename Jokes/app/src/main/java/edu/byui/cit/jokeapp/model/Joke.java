package edu.byui.cit.jokeapp.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.ServerValue;

import edu.byui.cit.jokeapp.view.MainActivity;

public class Joke extends MainActivity {
    @Exclude
    private String key;

    private String jokeType;
    private String jokeText;

    private Object whenCreated;
    private String creator;

    // Used by firebase and get time automatically.
    @SuppressWarnings("unused")
    public Joke() {this.whenCreated = ServerValue.TIMESTAMP;}

    // Used for searching the list of jokes.
    public Joke(String key) {this.key = key;}

    // Used for creating a joke that will be sent to firebase.
    public Joke(String jokeText, String jokeType, String creator) {
        this.creator = creator;
        this.jokeText = jokeText;
        this.jokeType = jokeType;
        this.whenCreated = ServerValue.TIMESTAMP;
    }

    // Getters and Setters
    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }

    public String getjokeText() {
        return jokeText;
    }
    public void setjokeText(String jokeText) {
        this.jokeText = jokeText;
    }

    public String getjokeType() {
        return jokeType;
    }
    public void setjokeType(String jokeType) {
        this.jokeType = jokeType;
    }

    public Object getWhenCreated() {
        return whenCreated;
    }
    public void setWhenCreated(Object whenCreated) {this.whenCreated = whenCreated;}

    public String getCreator() {return creator;}
    public void setCreator(String creator) {this.creator = creator;}

    @Override
    public boolean equals(Object obj) {
        boolean isEqual = false;
        if (obj instanceof Joke) {
            Joke other = (Joke)obj;
            isEqual = this.key.equals(other.key);
        }
        return isEqual;
    }
}

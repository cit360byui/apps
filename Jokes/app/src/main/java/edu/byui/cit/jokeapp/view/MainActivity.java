package edu.byui.cit.jokeapp.view;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import edu.byui.cit.jokeapp.R;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "MainActivity";
    Fragment mainFrag;

    @Override
    protected void onCreate(Bundle savedInstState) {
        super.onCreate(savedInstState);
        setContentView(R.layout.activity_main);

        // Change the home icon on the action bar to
        // the menu (hamburger) icon and make it visible.
        ActionBar actBar = getSupportActionBar();
        if (actBar != null) {
            actBar.setDisplayHomeAsUpEnabled(true);
        }


        if (savedInstState == null) {
            // Create the main fragment and place it
            // as the first fragment in this activity.
            mainFrag = new MainFrag();
            FragmentTransaction trans =
                    getSupportFragmentManager().beginTransaction();
            trans.add(R.id.fragContainer, mainFrag);
            trans.commit();
        }
    }

    /**
     * Method for displaying a different fragment in the fragment container.
     */
    public void switchToFragment(Fragment toShow, String tag) {
        // Replace whatever is in the fragContainer view with
        // toShow, and add the transaction to the back stack so
        // that the user can navigate back.
        FragmentTransaction trans =
                getSupportFragmentManager().beginTransaction();
        trans.replace(R.id.fragContainer, toShow, tag);
        trans.addToBackStack(null);
        trans.commit();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public String fragGetTag() {
        return TAG;
    }
}


package edu.byui.cit.jokeapp.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import edu.byui.cit.jokeapp.R;

//This class will be used for when the user clicks the "Add" fab on the mainFrag.
public class addJoke extends Fragment {
    static final String TAG = "addJoke";
    Spinner spinner;
    EditText editText;
    Button submit;
    String spinnerTxt;
    String jokeTxt;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstState) {
        View view = null;
        try {
            super.onCreateView(inflater, container, savedInstState);
            view = inflater.inflate(R.layout.add_joke, container, false);

            //joke input, and submit button OnClickListeners
            editText = view.findViewById(R.id.txtJoke);
            submit = view.findViewById(R.id.btnSubmit);
            submit.setOnClickListener(new SubmitClicky());

            //Spinner stuff
            spinner = view.findViewById(R.id.categorySpinner);
            // Create an ArrayAdapter using the string array and a default spinner layout
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.categorySpinner, android.R.layout.simple_spinner_item);
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // Apply the adapter to the spinner
            spinner.setAdapter(adapter);

        } catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }
        return view;

    }

    //OnClickListener for the Submit button. Submits the category and inputted joke.
    public class SubmitClicky implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            try {
                //Push to Firebase database the joke the user inputted and the category.
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                //Get the string in the spinner and use that as the Database reference
                spinnerTxt = String.valueOf(spinner.getSelectedItem());
                DatabaseReference ref = database.getReference(spinnerTxt);

                //Inputted Joke text
                jokeTxt = editText.getText().toString().trim();

                if (spinnerTxt.equals("Choose a Category:")) {
                    Snackbar wrongAnswer = Snackbar.make(v, "No! >:l That's not a category!!", Snackbar.LENGTH_LONG);
                    wrongAnswer.show();
                } else if (jokeTxt.equals("") ) {
                    Snackbar wrongAnswer = Snackbar.make(v, "No! >:l That's not a joke!!", Snackbar.LENGTH_LONG);
                    wrongAnswer.show();
                } else {

                    ref.push().setValue(jokeTxt);
                    MainActivity mainActivity = (MainActivity) getActivity();
                    mainActivity.onBackPressed();
                }
            } catch (Exception ex) {
                Log.e(MainActivity.TAG, ex.toString());
            }
        }
    }
}


package edu.byui.cit.jokeapp.view;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.List;

import edu.byui.cit.jokeapp.model.Joke;

public class firebaseFrag extends MainFrag {

    private List<Joke> listJokes;
    private DatabaseReference dbJokes;
    private enum State {Browsing, Inputting, PendingInsert}
    private State state;
    private String pendingKey;
    private int index = -1;

//    if (dbJokes == null) {
//        listJokes = new ArrayList<>();
//        dbJokes.addChildEventListener(new JokeEventHandler());
//    }

    private final class JokeEventHandler implements ChildEventListener {
        /**
         * Handles the event that firebase generates
         * when a new creature is added to the database.
         */
        @Override
        public void onChildAdded(
                @NonNull DataSnapshot dataSnapshot, String prevChildKey) {
            try {
                // Get the joke that was added.
                Joke added = dataSnapshot.getValue(Joke.class);
                if (added != null) {
                    String key = dataSnapshot.getKey();
                    added.setKey(key);
                    listJokes.add(added);
                    if (state == State.Browsing) {
                        if (index == -1) {
                            index = listJokes.size() - 1;
                            showJoke();
                        }
                    }
                    else if (state == State.PendingInsert) {
                        if (key != null && key.equals(pendingKey)) {
                            index = listJokes.size() - 1;
                            showJoke();
                            // enableButtons(true);
                            pendingKey = null;
                            state = State.Browsing;
                        }
                    }
                }
            }
            catch (Exception ex) {
                Log.e(MainActivity.TAG, "1: " + ex.getMessage());
            }
        }

        /**
         * Handles the event that firebase
         * generates when a joke is updated.
         */
        @Override
        public void onChildChanged(
                @NonNull DataSnapshot dataSnapshot, String s) {
            try {
                Joke changed = dataSnapshot.getValue(Joke.class);
                if (changed != null) {
                    String key = dataSnapshot.getKey();
                    changed.setKey(key);
                    int i = listJokes.indexOf(changed);
                    listJokes.set(i, changed);
                    if (i == index) {
                        showJoke();
                    }
                }
            }
            catch (Exception ex) {
                Log.e(MainActivity.TAG, "2: " + ex.getMessage());
            }
        }

        /**
         * Handles the event that firebase
         * generates when a creature is deleted.
         */
        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            try {
                String key = dataSnapshot.getKey();
                int i = listJokes.indexOf(new Joke(key));
                if (i != -1) {
                    listJokes.remove(i);
                    if (i == index) {
                        showJoke();
                    }
                }
            }
            catch (Exception ex) {
                Log.e(MainActivity.TAG, "3: " + ex.getMessage());
            }
        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, String
                s) {
        }

        @Override
        public void onCancelled(DatabaseError error) {
            // Failed to read value
            Log.e(MainActivity.TAG, "4: DB error: " + error.toString());
        }


    }
    // We have a couple of buttons to add new jokes. And we do not know what button we are supposed to use.
    private void showJoke() {
        int size = listJokes.size();
        if (index >= size) {
            index = size - 1;
        }
//        if (0 <= index && index < listJokes.size()) {
//            Joke joke = listJokes.get(index);
//            txtCreatureKey.setText(Joke.getKey());
//            txtName.setText(Joke.getjokeName());
//            txtType.setText(Joke.getjokeType());
//        }
//        else {
//            // ??? I think we don't need to clear edit text.
//            clearFields();
//        }
    }
}

package edu.byui.cit.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import androidx.appcompat.widget.AppCompatSpinner;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

import edu.byui.cit.model.Assert;
import edu.byui.cit.model.Keys;


public final class MultiSpinner extends AppCompatSpinner
		implements OnMultiChoiceClickListener, OnDismissListener {

	public interface MultiSpinnerListener {
		void onCheckedChanged(@NotNull boolean[] checked);
	}


	private static final int
			ALL = 0,    // Index of the "Select All" checkbox
			FIRST = 1;  // Index of the first item after "Select All"

	private String defaultText;
	private String[] items;
	private boolean[] checked;
	private AlertDialog dialog;
	private MultiSpinnerListener listener;


	public MultiSpinner(Context ctx) {
		super(ctx);
	}

	public MultiSpinner(Context ctx, AttributeSet attrs) {
		super(ctx, attrs);
	}

	public MultiSpinner(Context ctx, AttributeSet attrs, int defStyleAttr) {
		super(ctx, attrs, defStyleAttr);
	}


	public void setItems(String[] items, boolean[] checked,
			MultiSpinnerListener listener) {
		Assert.notNull(items);
		Assert.notNull(checked);
		Assert.is(items.length == checked.length);

		this.defaultText = items[ALL];
		this.items = Arrays.copyOf(items, items.length);
		this.checked = Arrays.copyOf(checked, checked.length);
		this.listener = listener;

		// Set the text on the spinner.
		normalize();
		String text = getSpinnerText();
		ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),
				android.R.layout.simple_spinner_item, new String[]{ text });
		setAdapter(adapter);
	}


	public boolean[] getChecked() {
		return Arrays.copyOf(checked, checked.length);
	}


	@Override
	public boolean performClick() {
		boolean handled = false;
		try {
			super.performClick();
			if (dialog == null) {
				AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
				builder.setMultiChoiceItems(items, checked, this);
				builder.setOnDismissListener(this);
				dialog = builder.create();
				Window window = dialog.getWindow();
				Assert.notNull(window);
				window.setGravity(Gravity.END);
				window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			}
			dialog.show();
			handled = true;
		}
		catch (Exception ex) {
			Log.e(Keys.TAG, "MultiSpinner.performClick", ex);
		}
		return handled;
	}


	@Override
	public void onClick(DialogInterface dialog, int which, boolean isChecked) {
		try {
			ListView listView = ((AlertDialog)dialog).getListView();
			checked[which] = isChecked;
			if (which == ALL) {
				for (int i = FIRST; i < items.length;  ++i) {
					checked[i] = isChecked;
					listView.setItemChecked(i, isChecked);
				}
			}

			// If the the user checked a checkbox other than selectAll, we
			// verify the state of all checkboxes. If all checkboxes are
			// checked, we check the selectAll checkbox.
			else if (isChecked) {
				normalize();
				listView.setItemChecked(ALL, checked[ALL]);
			}

			// If the the user unchecked a checkbox other
			// than selectAll, we uncheck selectAll.
			else /*if (! isChecked)*/ {
				checked[ALL] = false;
				listView.setItemChecked(ALL, false);
			}
		}
		catch (Exception ex) {
			Log.e(Keys.TAG, "MultiSpinner.onClick", ex);
		}
	}


	@Override
	public void onDismiss(DialogInterface dialog) {
		try {
			// Refresh the text on the spinner.
			String text = getSpinnerText();
			ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),
					android.R.layout.simple_spinner_item, new String[]{ text });
			setAdapter(adapter);

			// Attempt to get this MultiSpinner to give
			// up focus. None of these are working.
			performItemClick(adapter.getView(0, null, this), 0, adapter.getItemId(0));
			setSelection(0, false);
			clearFocus();

			if (listener != null) {
				// Inform the listener that the checked items have changed.
				listener.onCheckedChanged(checked);
			}
		}
		catch (Exception ex) {
			Log.e(Keys.TAG, "MultiSpinner.onDismiss", ex);
		}
	}


	private void normalize() {
		boolean all = true;
		for (int i = FIRST; i < items.length; i++) {
			if (! checked[i]) {
				all = false;
				break;
			}
		}
		checked[ALL] = all;
	}

	private String getSpinnerText() {
		String text;
		if (checked[ALL]) {
			text = defaultText;
		}
		else {
			// Build the text that will display in the spinner.
			StringBuilder sb = new StringBuilder();
			String separator = "";
			for (int i = FIRST; i < items.length; i++) {
				if (checked[i]) {
					sb.append(separator).append(items[i]);
					separator = ", ";
				}
			}
			text = sb.toString();
		}
		return text;
	}
}

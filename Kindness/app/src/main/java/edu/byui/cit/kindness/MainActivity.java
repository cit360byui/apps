package edu.byui.cit.kindness;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import com.google.android.material.navigation.NavigationView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.os.Bundle;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.FirebaseApp;

import edu.byui.cit.exception.LocationException;
import edu.byui.cit.exception.PermissionException;
import edu.byui.cit.exception.ProviderException;
import edu.byui.cit.exception.ServiceException;
import edu.byui.cit.model.Assert;
import edu.byui.cit.model.Keys;
import edu.byui.cit.widget.ErrorFrag;


/* TODO
 * 1. Get people to test out the app and give feedback.
 * 2. Create Junit tests for the database
 * 3. Add "Kindness List" item to the navigation drawer. Add a new fragment
 *    similar to the ReportsFrag that includes filtering and sorting. Improve
 *    the ReportAdapter to handle click and long click. Click shows the
 *    report on the map. Long click opens a popup menu with what items?
 * 4. Improve the ThanksFrag to look better and include a nice picture or
 *    clip art.
 * 5. Add "Settings" item to the navigation drawer. The settings should include:
 * 		On startup:
 * 			(radio button) view your current location
 * 			(radio button) return to last place on the map
 * 		After I submit a report of kindness
 * 			(checkbox) move the map to show my report
 * 		After someone else submits a report of kindness
 * 			(radio button) ask me if I want to move the map to show the report
 * 			(radio button) move the map to show the report
 * 6. Redo the translations because some of the English strings have changed.
 * 7. Add a fragment that views kindness reports as a chart or set of charts.
 * 8. On the submit page, make the center button active to show various
 *    "Easter Eggs", such as a smiley face, a waving person, positive quotes,
 *    a joke.
 * 9. Switch from firebase to firestore.
 * 10. Possibly add a data label above each icon
 * 11. Possible: have firebase generate a unique key for each user. Store the
 *     key in preferences and submit the key with a report. Add pin icons that
 *     have a border. Display the bordered icons for a user's own reports.
 */
public final class MainActivity extends AppCompatActivity {
	private DrawerLayout drawerLayout;
	private ActionBarDrawerToggle drawerToggle;
	private ErrorFrag fragError;


	@Override
	public void onCreate(Bundle savedInstState) {
		try {
			super.onCreate(savedInstState);
			setContentView(R.layout.activity_main);

			Context appCtx = getApplicationContext();

			// Try to start the LocationTracker early so that the
			// current location will be ready for the MapFragment
			// to move the camera to the current location.
			ActivityCompat.requestPermissions(this,
					new String[]{ Manifest.permission.ACCESS_COARSE_LOCATION,
					Manifest.permission.ACCESS_FINE_LOCATION },
					1);
			try {
				LocationTracker.getInstance().start(appCtx);
			}
			catch (PermissionException | ServiceException | ProviderException | LocationException ex) {
				Log.e(Keys.TAG, "MainActivity.onCreate", ex);
			}
			catch (Exception ex) {
				Log.e(Keys.TAG, "MainActivity.onCreate", ex);
			}

			// Initialize the firebase realtime database.
			FirebaseApp.initializeApp(appCtx);

			// Set the toolbar that is in activity_main.xml
			// as the action bar for this app.
			Toolbar toolbar = findViewById(R.id.toolbar);
			setSupportActionBar(toolbar);
			ActionBar actBar = getSupportActionBar();
			Assert.notNull(actBar);
			actBar.setDisplayHomeAsUpEnabled(true);

			drawerLayout = findViewById(R.id.drawerLayout);
			NavigationView navView = findViewById(R.id.navView);
			Menu menu = navView.getMenu();
			menu.findItem(R.id.howTo).setOnMenuItemClickListener(handleHowTo);
			menu.findItem(R.id.privacy).setOnMenuItemClickListener(handlePrivacy);
			menu.findItem(R.id.about).setOnMenuItemClickListener(handleAbout);

			drawerToggle = new ActionBarDrawerToggle(
					this, drawerLayout,
					R.string.open, R.string.close);
			drawerToggle.syncState();

			if (savedInstState == null) {
				// Create the main fragment and place it
				// as the first fragment in this activity.
				Fragment frag = new MainFrag();
				FragmentTransaction trans =
						getSupportFragmentManager().beginTransaction();
				trans.add(R.id.fragContainer, frag);
				trans.commit();
			}
		}
		catch (Exception ex) {
			Log.e(Keys.TAG, "MainActivity.onCreate", ex);
		}
	}


	@Override
	public void onStart() {
		try {
			super.onStart();

			// In case the LocationTracker was stopped in onStop, try to
			// start it again. If the LocationTracker was successfully
			// started in onCreate, this call will have not effect.
			LocationTracker tracker = LocationTracker.getInstance();
			tracker.start(getApplicationContext());
		}
		catch (PermissionException | ServiceException | ProviderException | LocationException ex) {
			Log.e(Keys.TAG, "MainActivity.onStart", ex);
		}
		catch (Exception ex) {
			Log.e(Keys.TAG, "MainActivity.onStart", ex);
		}
	}


	public void setDrawerIndicatorEnabled(boolean enable) {
		drawerToggle.setDrawerIndicatorEnabled(enable);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean handled = false;
		try {
			// If the menu icon is displayed in the toolbar, the drawToggle
			// will handle the menu being pressed by opening the drawer.
			handled = drawerToggle.onOptionsItemSelected(item);

			if (!handled) {
				switch (item.getItemId()) {
					case android.R.id.home:
						// Respond to the user pressing the "back/up" button
						// on the action bar in the same way as if the user
						// pressed the left-facing triangle icon on the main
						// android toolbar.
						onBackPressed();
						handled = true;
						break;
				}

				if (!handled) {
					handled = super.onOptionsItemSelected(item);
				}
			}
		}
		catch (Exception ex) {
			Log.e(Keys.TAG,"MainActivity.onOptionsItemSelected", ex);
		}
		return handled;
	}


	private final MenuItem.OnMenuItemClickListener handleHowTo =
			new MenuItem.OnMenuItemClickListener() {
				private Fragment fragment;

				@Override
				public boolean onMenuItemClick(MenuItem item) {
					boolean handled = false;
					try {
						drawerLayout.closeDrawers();

						// Replace whatever is in the fragment container
						// view with the HowTo fragment.
						if (fragment == null || fragment.isDetached()) {
							fragment = new HowToFrag();
						}
						switchToFrag(fragment);

						// Return true to indicate that this
						// method handled the item selected event.
						handled = true;
					}
					catch (Exception ex) {
						Log.e(Keys.TAG, "MainActivity.handleHowTo", ex);
					}
					return handled;
				}
			};

	private final MenuItem.OnMenuItemClickListener handlePrivacy =
			new MenuItem.OnMenuItemClickListener() {
				private Fragment fragment;

				@Override
				public boolean onMenuItemClick(MenuItem item) {
					boolean handled = false;
					try {
						drawerLayout.closeDrawers();

						// Replace whatever is in the fragment container
						// view with the HowTo fragment.
						if (fragment == null || fragment.isDetached()) {
							fragment = new PrivacyFrag();
						}
						switchToFrag(fragment);

						// Return true to indicate that this
						// method handled the item selected event.
						handled = true;
					}
					catch (Exception ex) {
						Log.e(Keys.TAG, "MainActivity.handlePrivacy", ex);
					}
					return handled;
				}
			};

	private final MenuItem.OnMenuItemClickListener handleAbout =
			new MenuItem.OnMenuItemClickListener() {
				private Fragment fragment;

				@Override
				public boolean onMenuItemClick(MenuItem item) {
					boolean handled = false;
					try {
						drawerLayout.closeDrawers();

						// Replace whatever is in the fragment container
						// view with the HowTo fragment.
						if (fragment == null || fragment.isDetached()) {
							fragment = new AboutFrag();
						}
						switchToFrag(fragment);

						// Return true to indicate that this
						// method handled the item selected event.
						handled = true;
					}
					catch (Exception ex) {
						Log.e(Keys.TAG, "MainActivity.handlePrivacy", ex);
					}
					return handled;
				}
			};


	@Override
	public void onStop() {
		try {
			LocationTracker.getInstance().stop();
		}
		catch (Exception ex) {
			Log.e(Keys.TAG, "MainActivity.onStop", ex);
		}
		finally {
			super.onStop();
		}
	}


	final void switchToFrag(Fragment fragment) {
		// Replace whatever is in the fragContainer view with
		// fragment, and add the transaction to the back stack
		// so that the user can navigate back.
		FragmentTransaction trans =
				getSupportFragmentManager().beginTransaction();
		trans.replace(R.id.fragContainer, fragment).addToBackStack(
				null).commit();
	}


	final void showErrorDialog(int messageID,
			DialogInterface.OnClickListener handler) {
		showErrorDialog(getString(messageID), handler);
	}

	final void showErrorDialog(String message,
			DialogInterface.OnClickListener handler) {
		ErrorFrag frag = fragError;
		if (frag == null) {
			frag = fragError = new ErrorFrag();
		}
		Bundle args = new Bundle();
		args.putString(Keys.MESSAGE, message);
		frag.setArguments(args);
		frag.setHandler(handler);
		frag.show(getSupportFragmentManager(), "dlgError");
	}
}

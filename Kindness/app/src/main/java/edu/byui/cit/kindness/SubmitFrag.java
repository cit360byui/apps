package edu.byui.cit.kindness;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import org.jetbrains.annotations.NotNull;

import edu.byui.cit.model.Category;
import edu.byui.cit.model.Keys;
import edu.byui.cit.model.Report;
import edu.byui.cit.widget.ChildFragment;


public final class SubmitFrag extends ChildFragment {
	private static final int[] buttons = {
			R.id.gifts,
			R.id.service,
			R.id.time,
			R.id.touch,
			R.id.words
	};
	private static final int[] animations = {
			R.anim.gifts_animate,
			R.anim.service_animate,
			R.anim.time_animate,
			R.anim.touch_animate,
			R.anim.words_animate
	};
	private static final Category[] categories = {
			Category.Gifts,
			Category.Service,
			Category.Time,
			Category.Touch,
			Category.Words
	};

	private ThanksFrag fragThanks;


	@Override
	protected String getTitle() {
		return getString(R.string.reportKindness);
	}


	@Override
	public View onCreateView(@NotNull LayoutInflater inflater,
			ViewGroup container, Bundle savedInstState) {
		View view;
		try {
			view = inflater.inflate(R.layout.frag_submit, container, false);

			MainActivity act = getMainActivity();
			for (int i = 0; i < buttons.length; ++i) {
				Button button = view.findViewById(buttons[i]);
				button.setOnClickListener(
						new CategoryClickHandler(categories[i]));
				button.getBackground().setAlpha(100);
				Animation anim = AnimationUtils.loadAnimation(act,
						animations[i]);
				button.startAnimation(anim);
			}
		}
		catch (Exception ex) {
			Log.e(Keys.TAG, "SubmitFrag.onCreateView", ex);
			view = inflater.inflate(R.layout.frag_mistake, container, false);
		}
		return view;
	}


	// Handles a click on one of the category buttons.
	private final class CategoryClickHandler
			implements View.OnClickListener, Runnable {
		private final Category category;

		CategoryClickHandler(Category category) {
			this.category = category;
		}

		@Override
		public void onClick(View button) {
			try {
				Animation buttonAnimate = AnimationUtils.loadAnimation(
						getActivity(), R.anim.button_click);
				button.startAnimation(buttonAnimate);
				button.postDelayed(this, buttonAnimate.getDuration());
			}
			catch (Exception ex) {
				Log.e(Keys.TAG, "SubmitFrag.onClick", ex);
				getMainActivity().onBackPressed();
			}
		}


		@Override
		public void run() {
			try {
				MainActivity act = getMainActivity();
				Context ctx = act.getApplicationContext();
				Location loc = LocationTracker.getInstance().getLocation(ctx);
				Report report = new Report(category, loc);
				report.submit();

				if (fragThanks == null || fragThanks.isDetached()) {
					fragThanks = new ThanksFrag();
				}
				act.getSupportFragmentManager().popBackStack();
				act.switchToFrag(fragThanks);
			}
			catch (Exception ex) {
				Log.e(Keys.TAG, "SubmitFrag.run", ex);
				getMainActivity().onBackPressed();
			}
		}
	}
}

package edu.byui.cit.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;

import org.jetbrains.annotations.NotNull;

import edu.byui.cit.kindness.R;
import edu.byui.cit.model.Assert;
import edu.byui.cit.model.Keys;


public final class ErrorFrag extends DialogFragment {
	private AlertDialog dialog;
	private DialogInterface.OnClickListener handler;

	public void setHandler(DialogInterface.OnClickListener handler) {
		this.handler = handler;
	}

	@NotNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstState) {
		Activity act = getActivity();
		Bundle args = getArguments();
		Assert.notNull(args);
		dialog = new AlertDialog.Builder(act)
				.setTitle(getString(R.string.errorTitle))
				.setMessage(args.getString(Keys.MESSAGE))
				.setPositiveButton(R.string.btnOK, handler)
				.create();
		return dialog;
	}

	@Override
	public void onResume() {
		super.onResume();
		Bundle args = getArguments();
		Assert.notNull(args);
		dialog.setMessage(args.getString(Keys.MESSAGE));
	}
}

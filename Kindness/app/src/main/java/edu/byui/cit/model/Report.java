package edu.byui.cit.model;

import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.maps.android.clustering.ClusterItem;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Comparator;
import java.util.Date;


public final class Report implements ClusterItem, Serializable {
	private final Object timestamp;
	private Category category;
	private double latitude;
	private double longitude;

	@Exclude
	private LatLng position;


	@SuppressWarnings("unused")  // Used by firebase
	public Report() {
		this.timestamp = ServerValue.TIMESTAMP;
		this.category = null;
		this.latitude = 0;
		this.longitude = 0;
	}

	// Used for searching a list of reports.
	public Report(long timestamp) {
		this.timestamp = timestamp;
		this.category = null;
		this.latitude = 0;
		this.longitude = 0;
	}

	// Used for creating a report that will be sent to firebase.
	public Report(Category category, Location loc) {
		this.timestamp = ServerValue.TIMESTAMP;
		this.category = category;
		this.latitude = loc.getLatitude();
		this.longitude = loc.getLongitude();
	}

	public void setCategory(int cat) {
		this.category = Category.valueOf(cat);
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@SuppressWarnings("unused")  // Used by firebase
	public Object getTimestamp() {
		return timestamp;
	}

	public int getCategory() {
		return this.category.ordinal();
	}

	public double getLatitude() {
		return this.latitude;
	}

	public double getLongitude() {
		return this.longitude;
	}


	@Exclude
	public void submit() {
		FirebaseDatabase database = FirebaseDatabase.getInstance();
		DatabaseReference reports = database.getReference(Keys.REPORTS);
		reports.push().setValue(this);
//		String key = reports.push().getKey();
//		HashMap<String, Object> updates = new HashMap<>();
//		updates.put("/" + Keys.REPORTS + "/" + key, this);
//		updates.put("/" + Keys.CATEGORIES + "/" + getCategory() + "/"
//						+ Keys.REPORTS + "/" + key, true);
//		database.getReference().updateChildren(updates);
		Log.i(Keys.TAG, toString());
	}

	@Exclude
	public long timestamp() {
		return timestamp instanceof Long ? ((Long)timestamp) : Long.MIN_VALUE;
	}

	@Exclude
	public Category category() {
		return category;
	}

	@Exclude @Override
	public LatLng getPosition() {
		if (position == null) {
			position = new LatLng(latitude, longitude);
		}
		return position;
	}

	@Exclude @Override
	public String getTitle() {
		return category.name();
	}

	@Exclude @Override
	public String getSnippet() {
		return "snippet";
	}

	@Exclude
	public BitmapDescriptor getIcon() {
		return category.getIcon();
	}

	@Exclude
	private static final double
			beginZ = 0,
			endZ = Float.MAX_VALUE / 2,
			rangeZ = endZ - beginZ,
			beginHistory = (30 * 365.25 * 24 * 60 * 60 * 1000), // 2000 AD
			endHistory = (100 * 365.25 * 24 * 60 * 60 * 1000),  // 2070 AD
			rangeHistory = endHistory - beginHistory;

	/** Computes and returns a z-index based on the timestamp. If the markers
	 * for two kindness reports overlap on the map, this z-index will force
	 * the marker for the newer report to be rendered in front of the marker
	 * for the older report. */
	@Exclude
	public float getZIndex() {
		return (float)(beginZ +
				(timestamp() - beginHistory) / rangeHistory * rangeZ);
	}


	@Exclude
	private static final DateFormat fmtr =
			DateFormat.getDateInstance(DateFormat.MEDIUM);

	@Exclude
	@Override
	public String toString() {
		return "Report: " + fmtr.format(new Date(timestamp())) +
				" " + (category == null ? "null" : category.name()) +
				" " + latitude + " " + longitude;
	}


	@Exclude
	public static final Comparator<Report> byTimestamp =
			new Comparator<Report>() {
				@Override
				public int compare(Report r1, Report r2) {
					return Long.signum(r1.timestamp() - r2.timestamp());
				}
			};

	@Exclude
	public static final Comparator<Report> byLocation =
			new Comparator<Report>() {
				@Override
				public int compare(Report r1, Report r2) {
					// 0.00000898316 of a degree of longitude at the equator is
					// approximately 1 meter, so we use that constant as the
					// threshold to determine if two latitudes or longitudes
					// are the same.
					int r;
					double epsilon = 0.00000898316;
					double delta = r1.latitude - r2.latitude;
					if (Math.abs(delta) < epsilon) {
						epsilon /= Math.cos(Math.max(r1.latitude, r2.latitude));
						delta = r1.longitude - r2.longitude;
						if (Math.abs(delta) < epsilon) {
							r = Long.signum(r1.timestamp() - r2.timestamp());
						}
						else {
							r = delta < 0 ? -1 : 1;
						}
					}
					else {
						r = delta < 0 ? -1 : 1;
					}
					return r;
				}
			};

	@Exclude
	public static final Comparator<Report> byCategory =
			new Comparator<Report>() {
				@Override
				public int compare(Report r1, Report r2) {
					int r = r1.getCategory() - r2.getCategory();
					if (r == 0) {
						r = Long.signum(r1.timestamp() - r2.timestamp());
					}
					return r;
				}
			};
}

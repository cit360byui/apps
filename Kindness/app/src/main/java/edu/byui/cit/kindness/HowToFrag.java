package edu.byui.cit.kindness;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import edu.byui.cit.model.Keys;
import edu.byui.cit.widget.ChildFragment;


public final class HowToFrag extends ChildFragment {
	@Override
	public View onCreateView(@NotNull LayoutInflater inflater,
			ViewGroup container, Bundle savedInstState) {
		View view;
		try {
			view = inflater.inflate(R.layout.frag_how_to, container, false);
		}
		catch (Exception ex) {
			Log.e(Keys.TAG, "HowToFrag.onCreateView", ex);
			view = inflater.inflate(R.layout.frag_mistake, container, false);
		}
		return view;
	}

	@Override
	protected String getTitle() {
		return getString(R.string.howTo);
	}
}

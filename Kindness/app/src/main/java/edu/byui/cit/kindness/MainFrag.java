package edu.byui.cit.kindness;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Point;
import android.location.Location;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import edu.byui.cit.exception.PermissionException;
import edu.byui.cit.model.Assert;
import edu.byui.cit.model.Category;
import edu.byui.cit.model.Duration;
import edu.byui.cit.model.Keys;
import edu.byui.cit.model.Report;
import edu.byui.cit.widget.Fragment;
import edu.byui.cit.widget.MultiSpinner;
import edu.byui.cit.widget.MultiSpinner.MultiSpinnerListener;


// This is a fragment within the main activity (main_activity.xml).
// We had to do it this way because the spinners were showing up behind
// the map, and now they have their own spot and can be used. This
// fragment is different than the other fragments because it uses the
// same toolbar as main_activity.xml and the floating action button (FAB).
public final class MainFrag extends Fragment
		implements ChildEventListener, OnMapReadyCallback,
		OnClickListener, OnItemSelectedListener, MultiSpinnerListener,
		ClusterManager.OnClusterClickListener<Report>,
		ClusterManager.OnClusterItemClickListener<Report> {

	private SubmitFrag fragReport;

	private Spinner spinPeriod;
	private MultiSpinner spinCategory;
	private FloatingActionButton fabAdd;

	private final HashMap<Category, ArrayList<Report>> indexes;
	private GoogleMap mMap;
	private ClusterManager<Report> clusterManager;


	public MainFrag() {
		super();

		// Create the category indexes that
		// will hold references to the reports.
		indexes = new HashMap<>();
		for (Category cat : Category.values()) {
			indexes.put(cat, new ArrayList<Report>());
		}

		// Get a reference to the /reports node in the database.
		FirebaseDatabase database = FirebaseDatabase.getInstance();
		DatabaseReference dbReports = database.getReference("/reports");

		// Register a listener object that will handle new
		// kindness reports that are added to the database.
		dbReports.addChildEventListener(this);
	}


	@Override
	protected String getTitle() {
		return getString(R.string.appName);
	}


	@Override
	public void onChildAdded(
			@NotNull DataSnapshot dataSnapshot, String prevChildKey) {
		try {
			// Get the report that was added.
			Report report = dataSnapshot.getValue(Report.class);
			if (report != null) {
				// Add this report to the index that
				// corresponds to this report's category.
				ArrayList<Report> index = indexes.get(report.category());
				Assert.notNull(index);
				index.add(report);

				if (mMap != null) {
					// Get the user selected categories to filter the reports.
					boolean[] checked = spinCategory.getChecked();
					if (checked[report.getCategory()]) {

						// Get the user selected duration to filter the
						// reports.
						int which = spinPeriod.getSelectedItemPosition();
						Duration durat = Duration.valueOf(which);
						long now = System.currentTimeMillis();
						if (durat.beginning(now) <= report.timestamp()) {
							// Add an item for this report
							// to the cluster manager.
							clusterManager.addItem(report);

							// Force the cluster manager to re-cluster
							// because we added another item.
							clusterManager.cluster();
						}
					}
				}
			}
		}
		catch (Exception ex) {
			Log.e(Keys.TAG, "MainFrag.onChildAdded", ex);
		}
	}

	@Override
	public void onChildChanged(@NotNull DataSnapshot dataSnapshot, String s) {
	}

	@Override
	public void onChildRemoved(@NotNull DataSnapshot dataSnapshot) {
	}

	@Override
	public void onChildMoved(@NotNull DataSnapshot dataSnapshot, String s) {
	}

	@Override
	public void onCancelled(DatabaseError error) {
		// Failed to read value
		Log.e(Keys.TAG, "MainFrag.onCancelled: database error: "
				+ error.toString());
	}


	@Override
	public View onCreateView(@NotNull LayoutInflater inflater,
			ViewGroup container, Bundle savedInstState) {
		View view = null;
		try {
			view = inflater.inflate(R.layout.frag_main, container, false);

			spinPeriod = view.findViewById(R.id.spinPeriod);
			spinCategory = view.findViewById(R.id.spinCategory);

			Resources res = getResources();
			String[] categories = res.getStringArray(R.array.categories);
			String durat;
			boolean[] checked;
			if (savedInstState != null) {
				durat = savedInstState.getString(Keys.FILTER_DURATION,
						Duration.AllTime.name());
				checked = savedInstState.getBooleanArray(Keys.FILTER_CATEGORY);
			}
			else {
				SharedPreferences prefs =
						getMainActivity().getPreferences(Context.MODE_PRIVATE);
				durat = prefs.getString(Keys.FILTER_DURATION,
						Duration.AllTime.name());
				checked = new boolean[categories.length];
				for (int i = 1; i < checked.length; ++i) {
					// Set all checked by default.
					checked[i] = prefs.getBoolean(Keys.FILTER_CATEGORIES[i],
							true);
				}
			}

			int which = Duration.valueOf(durat).ordinal();
			spinPeriod.setSelection(which);
			spinPeriod.setOnItemSelectedListener(this);
			spinCategory.setItems(categories, checked, this);

			SupportMapFragment fragMap = (SupportMapFragment)
					getChildFragmentManager().findFragmentById(R.id.fragMap);
			Assert.notNull(fragMap);
			if (mMap == null) {
				fragMap.getMapAsync(this);
			}

			fabAdd = view.findViewById(R.id.fabAdd);
			fabAdd.setOnClickListener(this);
		}
		catch (Exception ex) {
			Log.e(Keys.TAG, "MainFrag.onCreateView", ex);
		}
		return view;
	}


	@Override
	public void onStart() {
		try {
			super.onStart();
			getMainActivity().setDrawerIndicatorEnabled(true);
			showMarkers();
		}
		catch (Exception ex) {
			Log.e(Keys.TAG, "MainFrag.onStart", ex);
		}
	}


	/**
	 * Manipulates the map once it is available. This callback is triggered
	 * when the map is ready to be used. This is where we can add markers or
	 * lines, add listeners or move the camera. If Google Play services is not
	 * installed on the device, the user will be prompted to install it inside
	 * the SupportMapFragment. This method will only be triggered once the
	 * user has installed Google Play services and returned to the app.
	 */
	@Override
	public void onMapReady(GoogleMap googleMap) {
		try {
			// Load the icons to put on the map.
			Category.loadIcons(getResources());

			MainActivity act = getMainActivity();
			Context ctx = act.getApplicationContext();
			Assert.notNull(ctx);
			int granted = PackageManager.PERMISSION_GRANTED;
			int fine = ActivityCompat.checkSelfPermission(ctx,
					Manifest.permission.ACCESS_FINE_LOCATION);
			int coarse = ActivityCompat.checkSelfPermission(ctx,
					Manifest.permission.ACCESS_COARSE_LOCATION);

			mMap = googleMap;
			UiSettings settings = mMap.getUiSettings();
			settings.setCompassEnabled(true);
			settings.setZoomControlsEnabled(true);
			if (fine == granted || coarse == granted) {
				settings.setMyLocationButtonEnabled(true);
				mMap.setMyLocationEnabled(true);
			}

			LatLng rexburg = new LatLng(43.8260227, -111.7896876);
			SharedPreferences prefs = act.getPreferences(Context.MODE_PRIVATE);
			if (prefs.contains(Keys.CAMERA_LATITUDE)) {
				double lat = prefs.getFloat(Keys.CAMERA_LATITUDE, (float)rexburg.latitude);
				double lng = prefs.getFloat(Keys.CAMERA_LONGITUDE, (float)rexburg.longitude);
				float zoom = prefs.getFloat(Keys.CAMERA_ZOOM, 3);
				LatLng latlng = new LatLng(lat, lng);
				mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, zoom));
			}
			else if (fine == granted || coarse == granted) {
				// Move the camera to the user's location.
				Location loc = LocationTracker.getInstance().getLocation(ctx);
				LatLng latlng = new LatLng(loc.getLatitude(), loc.getLongitude());
				mMap.moveCamera(CameraUpdateFactory.newLatLng(latlng));
			}
			else {
				mMap.moveCamera(CameraUpdateFactory.newLatLng(rexburg));
			}

			// Initialize the cluster manager with the context and the map.
			// Activity extends context, so we can pass the main activity
			// as the context.
			clusterManager = new ClusterManager<>(act, mMap);
			clusterManager.setRenderer(
					new ClusterRenderer(act, mMap, clusterManager));

			// Point the map's listeners at the listeners
			// implemented by the cluster manager.
			mMap.setOnCameraIdleListener(clusterManager);
			mMap.setOnMarkerClickListener(clusterManager);
			clusterManager.setOnClusterClickListener(this);
			clusterManager.setOnClusterItemClickListener(this);

			// Show the pins for the reports that we have already received.
			showMarkers();
		}
		catch (Exception ex) {
			Log.e(Keys.TAG, "MainFrag.onMapReady", ex);
		}
	}


	@Override
	public void onItemSelected(
			AdapterView<?> parent, View view, int which, long id) {
		try {
			showMarkers();
		}
		catch (Exception ex) {
			Log.e(Keys.TAG, "MainFrag.onItemSelected", ex);
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		try {
			showMarkers();
		}
		catch (Exception ex) {
			Log.e(Keys.TAG, "MainFrag.onNothingSelected", ex);
		}

	}

	@Override
	public void onCheckedChanged(@NotNull boolean[] checked) {
		try {
			showMarkers();

			// Additional attempt to get the MultiSpinner
			// to give up focus. Doesn't work.
			fabAdd.requestFocus();
			fabAdd.clearFocus();
		}
		catch (Exception ex) {
			Log.e(Keys.TAG, "MainFrag.onCheckedChanged", ex);
		}

	}


	private void showMarkers() {
		if (mMap != null) {
			// Get the user selected duration to filter the reports.
			int which = spinPeriod.getSelectedItemPosition();
			Duration durat = Duration.valueOf(which);

			// Create a sample report that has as its timestamp
			// the beginning of the user selected duration.
			long now = System.currentTimeMillis();
			Report key = new Report(durat.beginning(now));

			// Get the user selected categories to filter the reports.
			boolean[] checked = spinCategory.getChecked();

			// Create an array list to hold all the filtered reports.
			ArrayList<Report> filtered = new ArrayList<>();

			// We start counting from 1 because the
			// first item in the list is "All Categories".
			for (int i = 1; i < checked.length; ++i) {
				if (checked[i]) {
					Category cat = Category.valueOf(i);
					ArrayList<Report> index = indexes.get(cat);
					Assert.notNull(index);

					// The list of reports from firebase is always stored in
					// chronological order, so find the position within the
					// list where the sample report should be located. This
					// position is where the reports that are within the user
					// selected duration begin.
					int pos = Collections.binarySearch(
							index, key, Report.byTimestamp);
					if (pos < 0) {
						pos = -(pos + 1);
					}

					// Add the reports to the filtered list of reports.
					for (int len = index.size(); pos < len; ++pos) {
						filtered.add(index.get(pos));
					}
				}
			}

			// Sort the filtered list of filtered reports from oldest to
			// newest.
			Collections.sort(filtered, Report.byTimestamp);

			// Clear the map and cluster manager of all markers.
			clusterManager.clearItems();

			// For each report in the filtered list, add a marker to the map.
			// For each report in the filtered list,
			// add an item to the cluster manager.
			for (int i = 0, size = filtered.size(); i < size; ++i) {
				Report report = filtered.get(i);
				clusterManager.addItem(report);
			}

			// Force the cluster manager to re-cluster because we added items.
			clusterManager.cluster();
		}
	}


	/** Handles a click on a cluster on the map. */
	@Override
	public boolean onClusterClick(Cluster<Report> cluster) {
		try {
			Bundle args = new Bundle();
			Report[] list = cluster.getItems().toArray(new Report[0]);
			args.putSerializable(Keys.REPORTS, list);
			ReportsFrag fragReports = new ReportsFrag();
			fragReports.setArguments(args);
			getMainActivity().switchToFrag(fragReports);
		}
		catch (Exception ex) {
			Log.e(Keys.TAG, "MainFrag.onClusterClick", ex);
		}
		return true;
	}


	/** Handles a click on a marker that currently is not in a cluster. */
	@Override
	public boolean onClusterItemClick(Report clicked) {
		try {
			// Get the user selected duration to filter the reports.
			int which = spinPeriod.getSelectedItemPosition();
			Duration durat = Duration.valueOf(which);

			// Create a sample report that has as its timestamp
			// the beginning of the user selected duration.
			long now = System.currentTimeMillis();
			Report key = new Report(durat.beginning(now));

			// Get the user selected categories to filter the reports.
			boolean[] checked = spinCategory.getChecked();

			Projection proj = mMap.getProjection();
			Point pc = proj.toScreenLocation(clicked.getPosition());

			// Create an array list to hold all the filtered reports.
			ArrayList<Report> filtered = new ArrayList<>();

			// We start counting from 1 because the
			// first item in the list is "All Categories".
			for (int i = 1; i < checked.length; ++i) {
				if (checked[i]) {
					Category cat = Category.valueOf(i);
					ArrayList<Report> index = indexes.get(cat);
					Assert.notNull(index);

					// The list of reports from firebase is always stored in
					// chronological order, so find the position within the
					// list where the sample report should be located. This
					// position is where the reports that are within the user
					// selected duration begin.
					int pos = Collections.binarySearch(
							index, key, Report.byTimestamp);
					if (pos < 0) {
						pos = -(pos + 1);
					}

					// Add the reports to the filtered list of reports.
					for (int len = index.size(); pos < len; ++pos) {
						Report other = index.get(pos);
						Point po = proj.toScreenLocation(other.getPosition());
						if (Math.abs(pc.x - po.x) < 50 &&
								Math.abs(pc.y - po.y) < 78) {
							filtered.add(index.get(pos));
						}
					}
				}
			}

			// Sort the filtered list of filtered reports from oldest to
			// newest.
			Collections.sort(filtered, Report.byTimestamp);

			Bundle args = new Bundle();
			Report[] list = filtered.toArray(new Report[0]);
			args.putSerializable(Keys.REPORTS, list);
			ReportsFrag fragReports = new ReportsFrag();
			fragReports.setArguments(args);
			getMainActivity().switchToFrag(fragReports);
		}
		catch (Exception ex) {
			Log.e(Keys.TAG, "MainFrag.onClusterItemClick", ex);
		}
		return true;
	}


	/** Handles a click on the add floating action button. */
	@Override
	public void onClick(View view) {
		try {
			// Try to start the LocationTracker again. If it was successfully
			// started in onCreate, this call will have no effect. If it
			// wasn't successfully started in onCreate and can't be started
			// here, show an error dialog and don't switch to the SubmitFrag.
			Context appCtx = getMainActivity().getApplicationContext();
			LocationTracker.getInstance().start(appCtx);

			if (fragReport == null || fragReport.isDetached()) {
				fragReport = new SubmitFrag();
			}
			getMainActivity().switchToFrag(fragReport);
		}
		catch (PermissionException ex) {
			Log.e(Keys.TAG, "MainFrag.onClick", ex);
			getMainActivity().showErrorDialog(R.string.locationErrMsg, requestPermissions);
		}
		catch (Exception ex) {
			Log.e(Keys.TAG, "MainFrag.onClick", ex);
			String msg = ex.toString() + " " + getString(R.string.unknownErrMsg);
			getMainActivity().showErrorDialog(msg, null);
		}
	}

	private final DialogInterface.OnClickListener requestPermissions =
			new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					MainActivity act = getMainActivity();
					ActivityCompat.requestPermissions(act, new String[]{
									Manifest.permission.ACCESS_COARSE_LOCATION,
									Manifest.permission.ACCESS_FINE_LOCATION },
							1);
				}
			};


	@Override
	public void onSaveInstanceState(@NotNull Bundle outState) {
		try {
			savePreferences(outState);
			super.onSaveInstanceState(outState);
		}
		catch (Exception ex) {
			Log.e(Keys.TAG, "MainFrag.onSaveInstanceState", ex);
		}
	}

	@Override
	public void onStop() {
		try {
			savePreferences(null);
			super.onStop();
		}
		catch (Exception ex) {
			Log.e(Keys.TAG, "MainFrag.onStop", ex);
		}
	}

	private void savePreferences(Bundle outState) {
		Duration durat = Duration.valueOf(
				spinPeriod.getSelectedItemPosition());
		boolean[] checked = spinCategory.getChecked();
		CameraPosition camera = mMap.getCameraPosition();

		if (outState != null) {
			// Save into the save instance state bundle.
			outState.putString(Keys.FILTER_DURATION, durat.name());
			outState.putBooleanArray(Keys.FILTER_CATEGORY, checked);
			outState.putDouble(Keys.CAMERA_LATITUDE, camera.target.latitude);
			outState.putDouble(Keys.CAMERA_LONGITUDE, camera.target.longitude);
			outState.putFloat(Keys.CAMERA_ZOOM, camera.zoom);
		}

		// Save into the user preferences.
		SharedPreferences prefs =
				getMainActivity().getPreferences(Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(Keys.FILTER_DURATION, durat.name());
		for (int i = 1; i < Keys.FILTER_CATEGORIES.length; ++i) {
			editor.putBoolean(Keys.FILTER_CATEGORIES[i], checked[i]);
		}
		editor.putFloat(Keys.CAMERA_LATITUDE, (float)camera.target.latitude);
		editor.putFloat(Keys.CAMERA_LONGITUDE, (float)camera.target.longitude);
		editor.putFloat(Keys.CAMERA_ZOOM, camera.zoom);
		editor.apply();
	}
}

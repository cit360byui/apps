package edu.byui.cit.model;

import org.jetbrains.annotations.Contract;


public abstract class Assert {
	@Contract("!null -> fail")
	public static void isNull(Object obj) {
		if (obj != null) {
			throw new IllegalArgumentException();
		}
	}

	@Contract("null -> fail")
	public static void notNull(Object obj) {
		if (obj == null) {
			throw new NullPointerException();
		}
	}

	@Contract("false -> fail")
	public static void is(boolean cmp) {
		if (! cmp) {
			throw new IllegalStateException();
		}
	}

	@Contract("true -> fail")
	public static void not(boolean cmp) {
		if (cmp) {
			throw new IllegalStateException();
		}
	}
}

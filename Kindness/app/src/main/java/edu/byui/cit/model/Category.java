package edu.byui.cit.model;

import android.content.res.Resources;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import edu.byui.cit.kindness.R;


public enum Category {
	None(0),  // or All
	Gifts(R.drawable.gifts_pin),
	Service(R.drawable.service_pin),
	Time(R.drawable.time_pin),
	Touch(R.drawable.touch_pin),
	Words(R.drawable.words_pin);

	private final int iconID;
	private BitmapDescriptor icon;
	private String localName;

	Category(int iconID) {
		this.iconID = iconID;
	}

	private void loadIcon() {
		if (icon == null && iconID != 0) {
			icon = BitmapDescriptorFactory.fromResource(iconID);
		}
	}

	public BitmapDescriptor getIcon() {
		return icon;
	}

	private void setLocalName(String localName) {
		this.localName = localName;
	}

	public String getLocalName() {
		return localName;
	}


	public static Category valueOf(int ordinal) {
		return values()[ordinal];
	}

	// Loads the icons that are placed on the map.
	public static void loadIcons(Resources res) {
		String[] localNames = res.getStringArray(R.array.categories);
		Category[] categories = values();
		for (int i = 0;  i < categories.length;  ++i) {
			Category cat = categories[i];
			cat.loadIcon();
			cat.setLocalName(localNames[i]);
		}
	}
}

package edu.byui.cit.widget;

import org.jetbrains.annotations.NotNull;

import edu.byui.cit.kindness.MainActivity;
import edu.byui.cit.model.Assert;


public abstract class Fragment extends androidx.fragment.app.Fragment {

	protected abstract String getTitle();


	@NotNull
	protected final MainActivity getMainActivity() {
		MainActivity act = (MainActivity)getActivity();
		Assert.notNull(act);
		return act;
	}


	@Override
	public void onStart() {
		super.onStart();
		getMainActivity().setTitle(getTitle());
	}
}

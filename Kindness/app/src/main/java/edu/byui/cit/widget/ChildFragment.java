package edu.byui.cit.widget;

/** The parent class for all fragments that are "children" of the
 * MainFrag, or in other words come after the MainFrag. */
public abstract class ChildFragment extends Fragment {
	@Override
	public void onStart() {
		super.onStart();
		getMainActivity().setDrawerIndicatorEnabled(false);
	}
}

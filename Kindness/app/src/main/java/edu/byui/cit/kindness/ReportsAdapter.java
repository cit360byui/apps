package edu.byui.cit.kindness;

import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Date;

import edu.byui.cit.model.Keys;
import edu.byui.cit.model.Report;


final class ReportsAdapter
		extends RecyclerView.Adapter<ReportsAdapter.ViewHolder> {
	private final Report[] dataset;
	private final String[] categoryNames;
	private final DateFormat fmtrDate, fmtrTime;
	private final NumberFormat fmtrDouble;


	ReportsAdapter(MainActivity activity, Report[] dataset) {
		Arrays.sort(dataset, Report.byTimestamp);
		this.dataset = dataset;
		categoryNames = activity.getResources().getStringArray(R.array.categories);
		fmtrDate = DateFormat.getDateInstance(DateFormat.MEDIUM);
		fmtrTime = DateFormat.getTimeInstance(DateFormat.MEDIUM);
		fmtrDouble = NumberFormat.getNumberInstance();
		fmtrDouble.setMaximumFractionDigits(20);

		// Tell the RecyclerView that the project keys are stable.
		setHasStableIds(true);
	}


	@Override
	public int getItemCount() {
		// Return the number of elements that are stored in the dataset.
		return dataset.length;
	}

	@Override
	public long getItemId(int index) {
		// Return the key of the report that is
		// stored in the dataset at index.
		return index + 1;
	}


	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.item_report, parent, false);
		return new ViewHolder(view);
	}

	// This onBindViewHolder method will be called each time that the
	// WorkTime app displays a project in a row of the RecyclerView.
	@Override
	public void onBindViewHolder(@NotNull ViewHolder holder, int index) {
		try {
			holder.bind(index);
		}
		catch (Exception ex) {
			Log.e(Keys.TAG, "ReportsAdapter.onBindViewHolder", ex);
		}
	}


	// Each ViewHolder object corresponds to one row in the RecyclerView.
	// Each ViewHolder object will hold three TextViews that display a
	// project to the user. The TextViews are defined in item_project.xml.
	final class ViewHolder extends RecyclerView.ViewHolder {
		// References to the four TextViews in this row.
		private final TextView txtWhen, txtLocation;

		ViewHolder(View view) {
			super(view);

			// Get a reference to each of the four
			// TextViews that are in the corresponding row.
			txtWhen = view.findViewById(R.id.txtWhen);
			txtLocation = view.findViewById(R.id.txtLocation);
		}

		// Bind this ViewHolder to the report that
		// is stored at index in the dataset.
		void bind(int index) {
			// A reference to the project object that
			// is displayed (held) in this ViewHolder.
			Report heldReport = dataset[index];

			// Display the data that is in the heldReport.
			Date date = new Date(heldReport.timestamp());
			String when = fmtrDate.format(date) + "  "
					+ fmtrTime.format(date) + "   "
					+ categoryNames[heldReport.getCategory()];
			String location = fmtrDouble.format(heldReport.getLatitude())
					+ "   " + fmtrDouble.format(heldReport.getLongitude());
			txtWhen.setText(when);
			txtLocation.setText(location);
		}
	}
}

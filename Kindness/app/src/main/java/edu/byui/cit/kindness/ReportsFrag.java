package edu.byui.cit.kindness;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import edu.byui.cit.model.Assert;
import edu.byui.cit.model.Keys;
import edu.byui.cit.model.Report;
import edu.byui.cit.widget.ChildFragment;


public final class ReportsFrag extends ChildFragment {
	@Override
	public View onCreateView(@NotNull LayoutInflater inflater,
			ViewGroup container, Bundle savedInstState) {
		View view = null;
		try {
			view = inflater.inflate(R.layout.frag_reports, container, false);

			Bundle args = getArguments();
			Assert.notNull(args);
			Report[] dataset = (Report[])args.getSerializable(Keys.REPORTS);

			MainActivity act = getMainActivity();
			RecyclerView recycler = view.findViewById(R.id.recycler);
			recycler.setLayoutManager(new LinearLayoutManager(act));
			recycler.setAdapter(new ReportsAdapter(act, dataset));
		}
		catch (Exception ex) {
			Log.e(Keys.TAG, "ReportsFrag.onCreateView", ex);
		}
		return view;
	}


	@Override
	protected String getTitle() {
		return getString(R.string.selected);
	}
}

package edu.byui.cit.model;

public abstract class Keys {
	public static final String TAG = "Kindness";

	public static final String
			REPORTS = "reports",
			CATEGORIES = "categories";

	public static final String
			MESSAGE = "message";

	public static final String
			FILTER_DURATION = "filterDuration",
			FILTER_CATEGORY = "filterCategory";
	public static final String[] FILTER_CATEGORIES = {
			null,
			"filterGifts",
			"filterService",
			"filterTime",
			"filterTouch",
			"filterWords"
	};

	public static final String
			CAMERA_LATITUDE = "cameraLatitude",
			CAMERA_LONGITUDE = "cameraLongitude",
			CAMERA_ZOOM = "cameraZoom";
}

package edu.byui.cit.kindness;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

import edu.byui.cit.model.Report;


final class ClusterRenderer extends DefaultClusterRenderer<Report> {
	ClusterRenderer(Context context,
			GoogleMap map, ClusterManager<Report> manager) {
		super(context, map, manager);
	}

	@Override
	public void onBeforeClusterItemRendered(
			Report item, MarkerOptions options) {
		options.icon(item.getIcon());
		options.zIndex(item.getZIndex());
		options.visible(true);
	}
}

package edu.byui.cit.maintenance;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;

import edu.byui.cit.model.AppDatabase;
import edu.byui.cit.model.Item;
import edu.byui.cit.model.ItemDAO;

public class AddItemFrag extends Fragment {

    private View view;
    private TextView editText;
    private DatePickerDialog.OnDateSetListener onDateSetListener;
    //For the calendar
    int year; int month; int day;
    private Calendar calendar;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstState) {
        view = null;
        try {
            super.onCreateView(inflater, container, savedInstState);
            view = inflater.inflate(R.layout.frag_add_item, container, false);
            editText = view.findViewById(R.id.chooseDate);
            //When onclick
            editText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    calendar = Calendar.getInstance();
                     year = calendar.get(Calendar.YEAR);
                     month = calendar.get(Calendar.MONTH);
                     day = calendar.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), android.R.style.Theme_Holo_Dialog_MinWidth,
                            onDateSetListener,year,month,day);
                    datePickerDialog.show();
                }
            });
              onDateSetListener = new DatePickerDialog.OnDateSetListener(){
                  @Override
                  public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                      // Because January is 0
                      month = month + 1;
                      String date = year + "-" + month + "-" + day;
                      calendar.set(Calendar.YEAR, year);
                      calendar.set(Calendar.MONTH, month);
                      calendar.set(Calendar.DAY_OF_MONTH, day);
                      editText.setText(date);
                  }
              };

            // Get the save button and save an item calling the saveItem function
            Button saveButton = view.findViewById(R.id.buttonSave);
            saveButton.setOnClickListener(new saveItem());

        }
        catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }
        return view;
    }


    private class saveItem implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            EditText txtCategory = view.findViewById(R.id.txtCategory);
            String category = txtCategory.getText().toString();
            EditText txtTask = view.findViewById(R.id.txtTask);
            String task = txtTask.getText().toString();
            EditText txtDate = view.findViewById(R.id.chooseDate);

            Date date = calendar.getTime();

            EditText txtDescription= view.findViewById(R.id.txtDescription);
            String description= txtDescription.getText().toString();

            Context ctx = getContext();
            AppDatabase db = AppDatabase.getInstance(ctx);
            ItemDAO itemDao = db.getItemDAO();
            Item item = new Item(category, task,
                    date,description);
            itemDao.insert(item);

            Intent intent = new Intent(ctx, MainActivity.class);
            startActivity(intent);
        }
    }
}

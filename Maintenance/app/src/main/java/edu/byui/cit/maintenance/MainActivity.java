package edu.byui.cit.maintenance;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.google.android.material.navigation.NavigationView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import edu.byui.cit.model.AppDatabase;
import edu.byui.cit.model.Item;
import edu.byui.cit.model.ItemDAO;


public class MainActivity extends AppCompatActivity
		implements NavigationView.OnNavigationItemSelectedListener {
	private DrawerLayout drawer;
	public static final String TAG = "Maintenance";

	private HelpFrag fragHelp;
	private AboutFrag fragAbout;


	@Override
	protected void onCreate(Bundle savedInstState) {

		try {
			super.onCreate(savedInstState);
			setContentView(R.layout.activity_main);
			//Adding MainFrag
			if (savedInstState == null) {
				// Create the main fragment and place it
				// as the first fragment in this activity.
				Fragment frag = new MainFrag();
				FragmentTransaction trans =
						getSupportFragmentManager().beginTransaction();
				trans.add(R.id.fragment_container, frag);
				trans.commit();
			}
			Toolbar toolbar = findViewById(R.id.toolbar);
			setSupportActionBar(toolbar);

			drawer = findViewById(R.id.drawer_layout);
			NavigationView navigationView = findViewById(R.id.nav_view);
			navigationView.setNavigationItemSelectedListener(this);

			ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,
					drawer, toolbar,
					R.string.navigation_drawer_open,
					R.string.navigation_drawer_close);
			drawer.addDrawerListener(toggle);
			toggle.syncState();

			ActionBar actBar = getSupportActionBar();


		}
		catch (Exception ex) {
			Log.e(TAG, ex.toString());
		}
		//actBar.setDisplayHomeAsUpEnabled(true);

		/*
		if (savedInstState == null) {
			// Create a fragment that contains all the items
			// and place it as the first fragment in this activity.
			FragmentTransaction trans =
					getSupportFragmentManager().beginTransaction();
			trans.add(R.id.fragContainer, getChooseFrag());
			trans.commit();
		}*/
	}

	@Override
	public boolean onNavigationItemSelected(@NonNull MenuItem item) {
		switch (item.getItemId()) {
			case R.id.nav_home:
				getSupportFragmentManager().beginTransaction().replace(
						R.id.fragment_container,
						new MainFrag()).commit();
				drawer.closeDrawers();
				break;
			case R.id.nav_add_item:
				getSupportFragmentManager().beginTransaction().replace(
						R.id.fragment_container,
						new AddItemFrag()).commit();
				drawer.closeDrawers();
				break;
			case R.id.nav_all_items:
				getSupportFragmentManager().beginTransaction().replace(
						R.id.fragment_container,
						new AllItemsFrag()).commit();
				drawer.closeDrawers();
				break;
			case R.id.nav_help:
				getSupportFragmentManager().beginTransaction().replace(
						R.id.fragment_container,
						new HelpFrag()).commit();
				drawer.closeDrawers();
				break;
			case R.id.nav_about:
				getSupportFragmentManager().beginTransaction().replace(
						R.id.fragment_container,
						new AboutFrag()).commit();
				drawer.closeDrawers();
				break;
		}
		return true;
	}

	@Override
	public void onBackPressed() {
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		}
		else {
			super.onBackPressed();
		}
		super.onBackPressed();
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items
		// to the action bar if it is present.
		getMenuInflater().inflate(R.menu.action_menu, menu);
		return true;
	}

//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		// Handle action bar item clicks here. The action bar will
//		// automatically handle clicks on the Home/Up button, so long
//		// as you specify a parent maintenance_activity in AndroidManifest.xml.
//		switch (item.getItemId()) {
//			case R.id.actHelp:
//				switchToFragment(getHelpFrag());
//				return true;
//
//			case R.id.actAbout:
//				switchToFragment(getAboutFrag());
//				return true;
//		}
//
//		return super.onOptionsItemSelected(item);
//	}

	HelpFrag getHelpFrag() {
		if (fragHelp == null) {
			fragHelp = new HelpFrag();
		}
		return fragHelp;
	}

	AboutFrag getAboutFrag() {
		if (fragAbout == null) {
			fragAbout = new AboutFrag();
		}
		return fragAbout;
	}


//	/** Displays a different fragment in the fragment container. */
//	void switchToFragment(Fragment toShow) {
//		// Replace whatever is in the fragContainer view with
//		// toShow, and add the transaction to the back stack so
//		// that the user can navigate back.
//		FragmentTransaction trans =
//				getSupportFragmentManager().beginTransaction();
//		trans.replace(R.id.fragContainer, toShow);
//		trans.addToBackStack(null);
//		trans.commit();
//	}

	@Override
	protected void onStart() {
		super.onStart();
		main();
	}

	private void main() {
		// Connecting the database
		Context appCtx = getApplicationContext();
		AppDatabase db = AppDatabase.getInstance(appCtx);
		ItemDAO itemDAO = db.getItemDAO();
		List<Item> items = itemDAO.getAll();
		/*Date date = new Date();
		Item item1 = new Item("Car", "Change Oil", date,
				"We need to change the oil.");
		TextView text = findViewById(R.id.testing);
		System.out.println(item1.toString());
		text.append(item1.toString());
		*/
		//Loop to check all the items
		// Loop to check each item on the database
		//itemDAO.deleteAll();
		/*for (Item i : items) {
			TextView textView = new TextView(this);
			textView.setText(i.toString());
			//LinearLayout layout = findViewById(R.id.testLayout);
			FrameLayout frameLayout = findViewById(R.id.fragment_container);
			frameLayout.addView(textView);
			//layout.addView(textView);

		}*/
	}
}
package edu.byui.cit.maintenance;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import edu.byui.cit.model.Item;


public class MainFragAdapter
		extends RecyclerView.Adapter<MainFragAdapter.ViewHolderItems> {

	// Type of data that I am going to return
	private static String TAG = "dateTest";
	private final List<Item> itemList;
	// A reference to the main activity
	private final MainActivity activity;

	//    private Item itemSelected;
	// Constructor
	public MainFragAdapter(List<Item> itemList, MainActivity activity) {
		setHasStableIds(true);
		this.itemList = itemList;

		this.activity = activity;
	}

	//Connect to my items_by_time.xml
	@Override
	public MainFragAdapter.ViewHolderItems onCreateViewHolder(
			@NonNull ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(
				R.layout.items_by_time, parent, false);
		ViewHolderItems holder = new ViewHolderItems(view);
		view.setOnClickListener(holder);
		return holder; // Call to last method
	}


	@Override
	public void onBindViewHolder(
			@NonNull MainFragAdapter.ViewHolderItems holder, int position) {
//        itemSelected = itemList.get(position);
		holder.bind(itemList.get(position));
	}

	@Override
	public int getItemCount() {
		//Returns How many elements I have in my list
		return itemList.size();
	}

	public class ViewHolderItems extends RecyclerView.ViewHolder
			implements View.OnClickListener {
		//Declare a textView variable
		TextView itemV;
		TextView itemDate;
		Item item;

		public ViewHolderItems(View itemView) {
			super(itemView);
			//Look for the items in the items_by_time.xlm and save it into our
			// textView variable
			itemV = itemView.findViewById(R.id.weekly_item);
			itemDate = itemView.findViewById(R.id.weekly_item_date);
		}

		public void bind(Item item) {
			this.item = item;

			//make our Text view to show the item
			itemV.setText(item.getTask());

			DateFormat formatter = new SimpleDateFormat("dd/mm/yyyy");

			Date today;
			today = item.getDate();
			Date noZero = new Date();
			String newDate = "";
			try {
				noZero = formatter.parse(formatter.format(today));
				newDate = formatter.format(noZero);
			}
			catch (ParseException e) {
				e.printStackTrace();
			}
			//Log.e(TAG, noZero.toString());
			itemDate.setText(newDate);
		}

		@Override
		public void onClick(View v) {
			SpecificItemFrag openItem = new SpecificItemFrag();
			FragmentTransaction trans =
					activity.getSupportFragmentManager().beginTransaction();

			Bundle bundle = new Bundle();

			bundle.putSerializable("itemSelect", this.item);
			openItem.setArguments(bundle);


			trans.replace(R.id.fragment_container, openItem);
			trans.addToBackStack(null);
			trans.commit();
		}
	}
}

package edu.byui.cit.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;


@Dao
public abstract class ItemDAO {
	// I'm still need to work on this
	/*@Query("SELECT task, date FROM Item WHERE date BETWEEN date(:today) AND
	date(:week)")
	public abstract List<Item>getWeeklyItems(long today, long week);*/

	@Query("SELECT COUNT(*) FROM Item")
	public abstract long count();

	@Query("SELECT * FROM Item")
	public abstract List<Item> getAll();

	@Query("SELECT * FROM Item WHERE itemKey = :key")
	public abstract Item getByKey(long key);

	public long insert(Item item) {
		long id = insertH(item);
		item.setItemKey(id);
		return id;
	}

	@Insert
	public abstract long insertH(Item item);

	@Update
	public abstract void update(Item item);

	@Delete
	public abstract void delete(Item item);

	@Query("DELETE FROM Item")
	public abstract void deleteAll();
}
package edu.byui.cit.maintenance;

import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import edu.byui.cit.model.Item;

public class AllItemsFrag extends Fragment {

    private List<Item> itemList;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstState) {
        View view = null;
        try {
            super.onCreateView(inflater, container, savedInstState);
            view = inflater.inflate(R.layout.frag_all_items, container, false);

            MainActivity act = (MainActivity)getActivity();
            RecyclerView viewProject = view.findViewById(R.id.all_items_rv);
            viewProject.setLayoutManager(new LinearLayoutManager(act));
            viewProject.setAdapter(new AllItemsAdapter(act));

            // Get the FAB and add a function to it
            FloatingActionButton fab = view.findViewById(R.id.addItem);
            fab.setOnClickListener(new HandleAddProject());

        }
        catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }

        return view;
    }

    private final class HandleAddProject implements View.OnClickListener {


        @Override
        public void onClick(View v) {

            AddItemFrag addFrag = new AddItemFrag();
            AppCompatActivity act = (AppCompatActivity)getActivity();
            FragmentTransaction trans =
                    act.getSupportFragmentManager().beginTransaction();
            trans.replace(R.id.fragment_container, addFrag);
            trans.addToBackStack(null);
            trans.commit();

        }
    }
}

// !isalpha(text[i])
// num of words
//Upper case the first letter and everything is lower
//ask question, all go their but just the one that fulfill the criteria will be prompt.
//when you pass one phrase  is 32 the second one not the 256
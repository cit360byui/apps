package edu.byui.cit.maintenance;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;

import java.util.List;

import edu.byui.cit.model.AppDatabase;
import edu.byui.cit.model.Item;
import edu.byui.cit.model.ItemDAO;


final class AllItemsAdapter
        extends RecyclerView.Adapter<AllItemsAdapter.ViewHolder> {
    // A reference to the main activity
    private final MainActivity activity;

    // A reference to the project DAO to read and write to the Project table.
    private final ItemDAO itemDAO;
    private Long itemKey;

    // A list of all projects stored in the Project table. This is an in
    // memory copy of the projects that are stored in non-volatile memory in
    // the Project table of the Room database.
    private final List<Item> dataset;

    AllItemsAdapter(MainActivity activity) {
        // Tell the RecyclerView that the project keys are stable.
        setHasStableIds(true);

        this.activity = activity;

        // Todo: Get a reference to the project DAO.

        // Connecting the database
        Context appCtx = activity.getApplicationContext();
        AppDatabase db = AppDatabase.getInstance(appCtx);
        itemDAO = db.getItemDAO();

        // Todo: Get a list of all the projects in the Project table.
        dataset = itemDAO.getAll();

    }

    @Override
    public int getItemCount() {
        // Todo: Return the number of elements that
        // are stored in the list of projects.

        return dataset.size();
    }

    @Override
    public long getItemId(int pos) {
        // Todo: Return the key of the project that is
        // stored in the list of projects at index pos.\

        return dataset.get(pos).getItemKey();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.items_by_name, parent, false);
        return new ViewHolder(view);
    }

    // This onBindViewHolder method will be called each time that the
    // WorkTime app displays a project in a row of the RecyclerView.
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int pos) {
        try {
            holder.bind(pos);
        }
        catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }
    }


    // Each ViewHolder object corresponds to one row in the RecyclerView.
    // Each ViewHolder object will hold three TextViews that display a project
    // to the user.
    final class ViewHolder extends RecyclerView.ViewHolder
            implements OnLongClickListener, OnMenuItemClickListener {

        // References to the three TextViews in this row.
        private final TextView itemTitle;
        private TextView itemId;


        // A reference to the project that is displayed in this ViewHolder.
        private Item item;

        ViewHolder(View view) {
            super(view);

            // Reference to each textView
            itemTitle = view.findViewById(R.id.item_name);


            // view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        // Bind this ViewHolder to the project that is
        // stored at index pos in the list of all projects.
        void bind(int pos) {
            item = dataset.get(pos);

            // Todo: Display the data in project in the three TextViews.
            itemTitle.setText(item.getTask());
            String key = item.getItemKey() + "";
            int key1 = Integer.parseInt(key);
            itemTitle.setId(key1);
            itemKey = item.getItemKey();
        }

        // This onLongClick method will be called when the
        // user long presses one row in the RecyclerView.
        @Override
        public boolean onLongClick(View view) {
            try {
                // Create a popup menu and show it to the user.
                PopupMenu menu = new PopupMenu(activity, view);
                menu.getMenuInflater().inflate(R.menu.popup, menu.getMenu());
                menu.setOnMenuItemClickListener(this);
                menu.show();
            }
            catch (Exception ex) {
                Log.e(MainActivity.TAG, ex.toString());
            }
            return true;
        }

        // This onMenuItemClick method will be called when
        // the user presses an item on the popup menu.
        @Override
        public boolean onMenuItemClick(MenuItem listItem) {
            boolean handled = false;
            try {
                switch (listItem.getItemId()) {
                    case R.id.itmDelete:

                        int index = dataset.indexOf(item);
                        itemDAO.delete(item);
                        dataset.remove(index);

                        notifyItemRemoved(index);
                        handled = true;
                        break;
                    case R.id.itmOpen:

                        // Create bundle and store data in bundle
                       // Bundle bun = new Bundle();
                        //bun.putLong("itemKey", itemKey);

                        SpecificItemFrag openItem = new SpecificItemFrag();
                        openItem.getSpecificKey(itemKey);

                        FragmentTransaction trans =
                                activity.getSupportFragmentManager().beginTransaction();
                        trans.replace(R.id.fragment_container, openItem);
                        trans.addToBackStack(null);
                        trans.commit();
                        break;
                }
            }
            catch (Exception ex) {
                Log.e(MainActivity.TAG, "Index problem");
                Log.e(MainActivity.TAG, ex.toString());
            }
            return handled;
        }
    }
}
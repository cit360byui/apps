package edu.byui.cit.model;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.Date;


@Entity
public class Item implements Serializable {
	@PrimaryKey(autoGenerate = true)
	private long itemKey;

	private String category;
	private String task;
	private Date date;
	private String description;

	public Item() {
	}

	@Ignore
	public Item(long itemKey) {
		this.itemKey = itemKey;
	}

	@Ignore
	public Item(String category, String task,
			Date date, String description) {
		this.category = category;
		this.task = task;
		this.date = date;
		this.description = description;
	}

	public long getItemKey() {
		return itemKey;
	}

	void setItemKey(long key) {
		this.itemKey = key;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	@Override
	public boolean equals(Object obj) {
		boolean eq = (this == obj);
		if (!eq && obj != null && getClass() == obj.getClass()) {
			Item other = (Item)obj;
			eq = this.itemKey == other.itemKey &&
					this.category.equals(other.category) &&
					this.task.equals(other.task) &&
					this.date.equals(other.date) &&
					this.description.equals(other.description);
		}
		return eq;
	}

	public String toString() {
		return "Item " + itemKey + ": " + category + "\n"
				+ task + "\n"
				+ date + "\n"
				+ description;
	}
}

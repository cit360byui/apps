package edu.byui.cit.maintenance;

import android.content.Context;

import androidx.appcompat.widget.AppCompatButton;

import android.util.AttributeSet;


public class ItemButton extends AppCompatButton {
	public ItemButton(Context context) {
		super(context);
	}

	public ItemButton(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ItemButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		int width = MeasureSpec.getSize(widthMeasureSpec);
		int height = (int)Math.round(width * 1.05);
		setMeasuredDimension(width, height);
	}
}


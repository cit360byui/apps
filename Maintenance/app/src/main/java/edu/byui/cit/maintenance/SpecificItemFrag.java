package edu.byui.cit.maintenance;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import edu.byui.cit.model.AppDatabase;
import edu.byui.cit.model.Item;
import edu.byui.cit.model.ItemDAO;


public class SpecificItemFrag extends Fragment {

	private List<Item> itemList;
	private RecyclerView recyclerView;
	private RecyclerView.LayoutManager layoutManager;
	private TextView title;
	private long key;

	public interface ItemClickListener {
		void onItemClick(View view, int position);
	}

	public void getSpecificKey(long key) {
		this.key = key;
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
			ViewGroup container,
			Bundle savedInstState) {
		View view = null;
		try {


			super.onCreateView(inflater, container, savedInstState);
			view = inflater.inflate(R.layout.frag_specific_item, container,
					false);

			MainActivity act = (MainActivity)getActivity();
			Context ctx = getContext();
			AppDatabase db = AppDatabase.getInstance(ctx);
			ItemDAO itamdao = db.getItemDAO();
			Item item = itamdao.getByKey(key);
			title = view.findViewById(R.id.itemTitle);
			title.setText(item.getTask());

		}
		catch (Exception ex) {
			Log.e(MainActivity.TAG, ex.toString());
		}

		return view;
	}

}
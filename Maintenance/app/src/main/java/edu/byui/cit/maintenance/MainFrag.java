package edu.byui.cit.maintenance;


import android.content.Context;

import androidx.fragment.app.Fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import edu.byui.cit.model.AppDatabase;
import edu.byui.cit.model.Item;
import edu.byui.cit.model.ItemDAO;


public class MainFrag extends Fragment {
	private List<Item> itemList;
	private RecyclerView recyclerView;
	private RecyclerView.LayoutManager layoutManager;
	private RecyclerView.Adapter mainFragAdapter;

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
			ViewGroup container,
			Bundle savedInstState) {
		View view = null;
		try {
			super.onCreateView(inflater, container, savedInstState);
			view = inflater.inflate(R.layout.frag_main, container, false);

			//find recycle view
			MainActivity act = (MainActivity)getActivity();
			RecyclerView viewProject = view.findViewById(R.id.remindersList);
			viewProject.setLayoutManager(new LinearLayoutManager(act));
			//viewProject.setAdapter(new AllItemsAdapter(act));

			//Get my list of Items for the next week
			// Connecting the database
			Context appCtx = getActivity();
			AppDatabase db = AppDatabase.getInstance(appCtx);
			ItemDAO itemDAO = db.getItemDAO();

			// itemList = itemDAO.getWeeklyItems(new Date());
			itemList = itemDAO.getAll();
			mainFragAdapter = new MainFragAdapter(itemList, act);
			viewProject.setAdapter(mainFragAdapter);

		}
		catch (Exception ex) {
			Log.e(MainActivity.TAG, ex.toString());
		}
		return view;
	}
}

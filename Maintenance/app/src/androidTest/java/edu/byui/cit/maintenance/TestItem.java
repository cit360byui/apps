package edu.byui.cit.maintenance;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Test;

import java.util.Date;

import edu.byui.cit.model.AppDatabase;
import edu.byui.cit.model.Item;
import edu.byui.cit.model.ItemDAO;

import static org.junit.Assert.assertEquals;


public class TestItem {

	// Get the database and the data access objects.
	private Context ctx =
			InstrumentationRegistry.getInstrumentation().getTargetContext();
	private AppDatabase db = AppDatabase.getInstance(ctx);
	private ItemDAO itemDao = db.getItemDAO();

	@Test
	public void useAppContext() {
		// Context of the app under test.
		Context appContext =
				InstrumentationRegistry.getInstrumentation().getTargetContext();
		assertEquals("edu.byui.cit.maintenance", appContext.getPackageName());
	}

	// Date variables
	Date date1 = new Date(1991, 1, 1);
	Date date2 = new Date(1991, 1, 1);

	@Test
	public void testProjectTable() {
		// Delete all projects and verify that
		// there are no rows in the Project table.
		itemDao.deleteAll();
		assertEquals(0, itemDao.count());

		// Insert two project rows
		Item item1 = new Item("Car 1", "Change Oil", date1,
				"We need to change the oil.");
		Item item2 = new Item("Car 2", "Change Tires", date2,
				"We need to change the tires.");
		itemDao.insert(item1);
		itemDao.insert(item2);
		assertEquals(2, itemDao.count());

		// Retrieve rows and verify data
		Item firstItemSaved = itemDao.getByKey(item1.getItemKey());
		Item secondItemSaved = itemDao.getByKey(item2.getItemKey());
		assertEquals(item1, firstItemSaved);
		assertEquals(item2, secondItemSaved);

		// Update the row
		item1.setCategory("Updated Category");
		item1.setDescription("Updated Description");

		// Update the row and test that 1st row is updated and 2nd row is not
		itemDao.update(item1);
		assertEquals("Updated Category", item1.getCategory());
		assertEquals("Updated Description", item1.getDescription());
		assertEquals("Car 2", item2.getCategory());
		assertEquals("We need to change the tires.", item2.getDescription());

		// Verify there are still two rows
		assertEquals(2, itemDao.count());

		// Delete the second row
		itemDao.delete(secondItemSaved);
		assertEquals(1, itemDao.count());

		// Delete all
		itemDao.deleteAll();
		assertEquals(0, itemDao.count());
	}
}
